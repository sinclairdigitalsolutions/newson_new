FROM node:14.4.0


RUN id -u node

# ENV LIBRARY_PATH=/lib:/usr/lib
# ENV PORT 3000
# ENV NEXT_TELEMETRY_DISABLED 1

# Install JQ and aws cli so we can get secrets pulled in
RUN apt-get update && apt-get install -y \
    jq \
    python \
    python-dev \
    python-pip \
    curl

# Install aws cli
RUN pip install awscli --upgrade --user

# Create app directory
RUN mkdir -p /usr/src/app

# Copying source files
# COPY package*.json /usr/src/app/
# COPY . /usr/src/app

#add path to aws cli to path variable
ENV PATH="/root/.local/bin:${PATH}"

# Copy entrypoint script and set permissions
ADD entrypoint.sh /usr/src/entrypoint.sh
RUN chown node: /usr/src/entrypoint.sh
RUN chmod +x /usr/src/entrypoint.sh


COPY . /usr/src/app
RUN chown -R node: /usr/src/app

# Installing dependencies
WORKDIR /usr/src/app

RUN yarn install --prod


# Building app
# COPY __mocks__ __mocks__
# COPY components components
# COPY globalStyles globalStyles
# COPY hooks hooks
# COPY lib lib
# COPY pages pages
# COPY public public

RUN yarn build

EXPOSE 3000
USER node

ENTRYPOINT [ "/bin/sh", "/usr/src/entrypoint.sh" ]
import GlobalStyles from "../globalStyles/globalStyles";
import { useState, useEffect } from "react";
import { useRouter } from "next/router"
import App from "next/app";
import { ThemeProvider } from "styled-components";
import theme from "../globalStyles/theme";
import GlobalNavigation from "../components/GlobalNavigation/GlobalNavigation";
import "react-multi-carousel/lib/styles.css";
import BreakingNewsBanner from "../components/BreakingNewsBanner/BreakingNewsBanner";
import Router from "next/router";
import NProgress from "nprogress";
import useCoordinatesFetch from "../hooks/useCoordinatesFetch";
import LogoHeader from "../components/LogoHeader/LogoHeader";
import { analyticsPageView } from "../lib/analytics";
import { sendPageView } from "../lib/utils";
import { createVinsonSignature } from "../lib/vinsonRequestSignature";
import { getCurrentDate } from "../lib/utils";
import { getCollection } from "../lib/collectionRequest";
import { loadFirebaseDB } from "../lib/firebaseDB";

Router.events.on("routeChangeStart", (url) => {
  NProgress.start();
});

Router.events.on("routeChangeComplete", (url) => {

  NProgress.done();
});

Router.events.on("routeChangeError", () => {
  NProgress.done();
});

const getBreakingNews = async () => {
  const configDevUrl = "https://redspace-newson.akamaized.net/config/staging/config.json";
  const configProdUrl = "https://redspace-newson.akamaized.net/config/config.json";

  const configFetch = await fetch(configProdUrl);
  // TODO: use env variable here to add prod url switch
  let { appConfig, breakingNews, featured, sponsorship } = await configFetch.json();

  let firebaseInstance = await loadFirebaseDB();

  const breaking_news = await firebaseInstance
    .auth()
    .signInAnonymously()
    .then(async () => {
      return await firebaseInstance.database()
      .ref("breakingNews")
      .once("value")
      .then((snapshot) => {
        return snapshot.val();
      });
    });

  breakingNews = { ...breakingNews, polling: breaking_news.polling, enabled: breaking_news.enabled};
  
  return { breakingNews };
};

/**
 * Fetches the available states with channels to populute the navigation
 * by location dropdown with
 *
 * @returns { Object } an object containing an array of states
 */
const getMenuContent = async (currentDate) => {
  const params = ["latitude=41.8483", "longitude=-87.6517"];

  const signature = createVinsonSignature(
    currentDate,
    params,
    "Channel/ByState"
  );

  const locations = await getCollection(
    "Channel/ByState",
    params,
    signature,
    currentDate
  );

  return { locations };
};

function NewsonApp({
  Component,
  pageProps,
  locations,
  breakingNews,
}) {
  const { enabled } = breakingNews;
  const { text } = breakingNews;
  const [breakingNewsEnabled] = useState(enabled);
  const [userLoc, setUserLoc] = useState({});
  const { userCoordinates, isLoading, isError } = useCoordinatesFetch();
  const router = useRouter();

  useEffect(() => {
    let userLocation;
    if (localStorage.getItem("userLocation")) {
      const cachedUserCoordinates = JSON.parse(
        localStorage.getItem("userLocation")
      );
      userLocation = {
        latitude: cachedUserCoordinates[0],
        longitude: cachedUserCoordinates[1],
      };
      setUserLoc(userLocation);
    }
  }, []);

  useEffect(() => {
    sendPageView(router.pathname) ? analyticsPageView(router.pathname) : null;
  }, [router.pathname])

  useEffect(() => {
    if (!localStorage.getItem("userLocation")) {
      setUserLoc(userCoordinates);
      if (!isLoading) {
        setTimeout(() => {
          localStorage.setItem(
            "userLocation",
            JSON.stringify(Object.values(userCoordinates))
          );
        });
      }
    }
  }, [isLoading]);

  return (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        {breakingNewsEnabled && (
          <BreakingNewsBanner storyTitle={text} storyLink={"/breakingNews"} />
        )}
        <LogoHeader breakingNewsEnabled={breakingNewsEnabled}>NEWSON</LogoHeader>
        <GlobalNavigation
          locations={locations}
          breakingNewsEnabled={breakingNewsEnabled}
        />
        <Component
          {...pageProps}
          userCoordinates={userLoc}
          breakingNewsEnabled={breakingNewsEnabled}
          breakingNewsData={breakingNews}
        />
      </ThemeProvider>
    </>
  );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
NewsonApp.getInitialProps = async (appContext) => {
  const currentDate = getCurrentDate();
  const states = await getMenuContent(currentDate);
  let breakingNews = await getBreakingNews();
  let appProps = await App.getInitialProps(appContext);

  appProps = { ...appProps, ...states, ...breakingNews };

  return { ...appProps };
};

export default NewsonApp;

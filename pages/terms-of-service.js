import Meta from "../components/Meta";
import { useEffect } from "react";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function TermsOfService({ data }) {
  useEffect(() => {
    window.location.replace("http://corporate.newson.us/terms-of-service");
  }, []);

  return null;
}

import Meta from "../components/Meta";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import LayoutGrid from "../components/PageLayouts/LayoutGrid";
import Header from "../components/Header/Header";
import useFavorites from "../hooks/useFavorites";
import useStatesFetch from "../hooks/useStatesFetch";
import useLocalItemsFetch from "../hooks/useLocalItemsFetch";
import { filterDuplicates, sortByDma } from "../lib/utils";
import { analyticsLocationPageView } from "../lib/analytics";
import { set } from "nprogress";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

const ByLocation = ({ data, userCoordinates, breakingNewsEnabled }) => {
  const { latitude, longitude } = userCoordinates;
  const { states, isLoading, isError } = useStatesFetch();
  const [stateChannels, setStateChannels] = useState([]);
  const [isRendered, setIsRendered] = useState(false);
  const [favorites] = useFavorites();
  const {
    data: localItemsContent = [],
    isLoading: localItemsLoading,
    isError: localItemsError,
  } = useLocalItemsFetch({
    latitude,
    longitude,
  });
  const {
    pathname,
    asPath,
    query: { term },
  } = useRouter();

  const pathArray = pathname.split("/");
  const path = pathArray[pathArray.length - 1];

  const getStateChannels = (stateQuery) => {
    const currentStates = states?.filter((item) => item.name === stateQuery)[0];
    let channels;

    if (currentStates?.cities.length) {
      channels = currentStates?.cities
        .map((city) => city.channels)
        .reduce((a, c) => a.concat(c), []);
    } else {
      channels = [];
    }

    return channels;
  };

  useEffect(() => {
    analyticsLocationPageView(asPath, term)
  }, [asPath, term])

  useEffect(() => {
    const stateChannels = Boolean(term) ? getStateChannels(term) : localItemsContent;
    const favoriteStateChannelIds = stateChannels
    ?.filter((item) => 
      favorites?.includes(item.id)
    )
    ?.map(channel => 
      channel.id
    );

    const sortedByDma = sortByDma(stateChannels, favoriteStateChannelIds);

    setStateChannels(sortedByDma);
  }, [term, isLoading, localItemsLoading, favorites])

  //TODO: Have since removed need for page title. Can remove this?
  const pageTitle =
    term !== undefined ? (
      <span>
        Stations Serving <div className="strong">{term}</div>
      </span>
    ) : (
      <span>Stations Closest to Me</span>
    );

  if (isLoading && localItemsLoading) return <div></div>;

  return (
    <div className="container">
      <Meta />
      <Header
        type="generic"
        size="small"
        pageTitle={pageTitle}
        pathString={path}
        byLocationData={data}
        breakingNewsEnabled={breakingNewsEnabled}
      />
      {Object.keys(stateChannels).map(dmaName => {
        return (
          <LayoutGrid
            stationGroupItems={stateChannels[dmaName]}
            favorites={favorites}
            pageTitle={dmaName}
          ></LayoutGrid>
        )
      })}
    </div>
  );
};

export default ByLocation;

import React, { useEffect } from "react";
import Meta from "../components/Meta";
import LayoutGrid from "../components/PageLayouts/LayoutGrid";
import useFavorites from "../hooks/useFavorites";
import useFavoriteFetch from "../hooks/useFavoriteFetch";
import Header from "../components/Header/Header";
import {
  FavoritesEmptyIcon,
  FavoritesEmptyTitle,
  FavoritesEmptyMessage,
  FavoritesEmptyMessageMobile,
  FavoritesEmptySection,
} from "../components/PageLayouts/FavoritesPageStyles";
import FavoritesIconSvg from "../public/images/icons/favorites-icon.svg";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function Favorites({breakingNewsEnabled}) {
  const [favorites] = useFavorites();
  const { channels, isLoading, isError } = useFavoriteFetch(favorites);

  // TODO: Add Loading state and error handling
  if (isLoading && favorites?.length > 0) return <div></div>;

  return (
    <div className="container">
      <Meta />
      <Header 
      pageTitle="My Favorites" 
      size="small" 
      type="generic" 
      breakingNewsEnabled={breakingNewsEnabled}
      />
      {favorites && favorites.length > 0 && !isError ? (
        <LayoutGrid
          favorites={favorites}
          stationGroupItems={channels}
        ></LayoutGrid>
      ) : (
        <FavoritesEmptySection>
          <FavoritesEmptyIcon>
            <FavoritesIconSvg className="svg" />
          </FavoritesEmptyIcon>
          <FavoritesEmptyTitle>
            You haven't added any stations to your favorites list!
          </FavoritesEmptyTitle>
          <FavoritesEmptyMessage>
            Try selecting the "Add to Favorites" button on a station page.
          </FavoritesEmptyMessage>
          <FavoritesEmptyMessageMobile>
            Try tapping the "Add to Favorites" button on a station page or on the heart icon to instantly add a station to your Favorites list
          </FavoritesEmptyMessageMobile>
        </FavoritesEmptySection>
      )}
    </div>
  );
}

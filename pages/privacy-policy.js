import Meta from "../components/Meta";
import {useEffect} from "react";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function PrivacyPolicy({ data }) {
  useEffect(() => {
    window.location.replace("http://corporate.newson.us/privacy-policy");
  }, []);

  return (
    <div className="container">
    </div>
  );
}

import Meta from "../components/Meta";
import HeroCarousel from "../components/HeroCarousel/HeroCarousel";
import {
  DB_FAVORITES_KEY,
  DB_LOCAL_STATIONS_KEY,
  DB_LIVE_STATIONS_KEY,
} from "../lib/constants";
import { HeroCarouselWrapper } from "../components/HeroCarousel/HeroCarouselStyles";

export const getServerSideProps = async () => {
  let carouselItems = [];
  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/firebaseConfig`
  );
  let {
    featured: {
      carousel: { syntax: carouselTemplates },
    },
  } = await firebase.json();

  //Remove duplicate items for iteration so we don't make mulitple calls to the api
  //for the same thing
  const templateRemoveDuplicates = Array.from(new Set([...carouselTemplates]));

  //Get the item type and amount of slides per type out of the RTD response
  const templateConfigs = [...carouselTemplates].reduce(
    (a, c) => a.set(c, (a.get(c) || 0) + 1),
    new Map()
  );

  //Now get the item count to set the api call limit
  const itemCount = [...templateConfigs].map(([template, items]) => ({
    template,
    items,
  }));

  // We have to construct the hero carousel from the template by concatenating
  // the various api calls into one array and returning that array to the HeroCarousel
  // component
  for (const item of templateRemoveDuplicates) {
    switch (item) {
      case DB_FAVORITES_KEY:
        console.log("get favourite stations will go here");
        break;
      case DB_LOCAL_STATIONS_KEY:
        const nearMeFrequency = itemCount.filter(
          (item) => item.template === DB_LOCAL_STATIONS_KEY
        )[0].items;

        let localItems = await fetch(
          `${process.env.ENVIRONMENT_URL}/api/localStations/${nearMeFrequency}`
        );

        let localItemsContent = await localItems.json();

        carouselItems = carouselItems.concat(localItemsContent);
        break;
      case DB_LIVE_STATIONS_KEY:
        const liveNowFrequency = itemCount.filter(
          (item) => item.template === DB_LIVE_STATIONS_KEY
        )[0].items;

        let liveNowItems = await fetch(
          `${process.env.ENVIRONMENT_URL}/api/liveNow/${liveNowFrequency}`
        );

        let liveNowContent = await liveNowItems.json();

        carouselItems = carouselItems.concat(liveNowContent);
        break;
    }
  }

  return { props: { data: carouselItems } };
};

const Home = ({ data }) => {
  return (
    <div className="container">
      <main>
        <HeroCarouselWrapper>
          <HeroCarousel items={data} autoPlayInterval={0} />
        </HeroCarouselWrapper>
      </main>
    </div>
  );
};

export default Home;

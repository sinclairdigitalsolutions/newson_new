import { useEffect, useState } from "react";
import Meta from "../components/Meta";
import { useRouter } from "next/router";
import Header from "../components/Header/Header";
import useFavorites from "../hooks/useFavorites";
import LayoutGrid from "../components/PageLayouts/LayoutGrid";
import {
  EmptySearchResults,
  EmptySearchResultsTitle,
  EmptySearchResultsMessage,
} from "../components/PageLayouts/SearchPageStyles";
import Error from "./_error";
import { filterDuplicates } from "../lib/utils";
import { analyticsSearchEvent, analyticsSearchPageView } from '../lib/analytics';

export const getServerSideProps = async ({ query: { term } }) => {
  let searchResults = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/search/${term}`
  );

  const errorCode = searchResults.ok ? false : searchResults.status;

  if (errorCode) {
    return { props: { errorCode, data: {} } };
  } else {
    let searchResultsContent = await searchResults.json();
    return {
      props: { data: searchResultsContent },
    };
  }
};

const Search = ({ data, errorCode, breakingNewsEnabled }) => {
  const [searchItems, setSearchItems] = useState([]);
  if (errorCode) {
    return <Error />;
  }

  const {
    pathname,
    query: { term },
  } = useRouter();
  const [favorites] = useFavorites();

  const pathArray = pathname.split("/");
  const path = pathArray[pathArray.length - 1];

  useEffect(() => {
    analyticsSearchPageView(pathname, term)
  }, [pathname])

  useEffect(() => {
    analyticsSearchEvent(term);
  }, [term])

  useEffect(() => {
    const searchFavorites = data.filter((item) => favorites?.includes(item.id));
    const searchItems = filterDuplicates([...searchFavorites, ...data]);

    setSearchItems(searchItems);
  }, [favorites, term]);

  const searchTitle = (
    <span>
      <span className="desktop-only">
        Results for <div className="strong">{term}</div>
      </span>
      <span className="mobile">Search</span>
    </span>
  );

  return (
    <div className="container">
      <Meta />
      <Header
        pathString={path}
        pageTitle={searchTitle}
        type="generic"
        size="small"
        breakingNewsEnabled={breakingNewsEnabled}
      />
      {data && data.length > 0 ? (
        <LayoutGrid
          gridSectionType="large"
          stationGroupItems={searchItems}
          pathString={path}
          favorites={favorites}
        ></LayoutGrid>
      ) : (
        <EmptySearchResults>
          <EmptySearchResultsTitle>
            There are no results for your search.
          </EmptySearchResultsTitle>
          <EmptySearchResultsMessage>
            Trying selecting “Search Again” to try something new.
          </EmptySearchResultsMessage>
        </EmptySearchResults>
      )}
    </div>
  );
};

export default Search;

import { useState, useEffect } from "react";
import Meta from "../components/Meta";
import HeroCarousel from "../components/HeroCarousel/HeroCarousel";
import { HeroCarouselWrapper } from "../components/HeroCarousel/HeroCarouselStyles";
import {
  DB_BREAKING_NEWS_STATIONS_KEY,
  DB_FAVORITES_KEY,
  DB_LIVE_STATIONS_KEY,
  DB_LOCAL_STATIONS_KEY,
  STATION_GROUP_TITLES
} from "../lib/constants";
import LayoutCarousel from "../components/PageLayouts/LayoutCarousel";
import useLocalItemsFetch from "../hooks/useLocalItemsFetch";
import useLiveFetch from "../hooks/useLiveItemsFetch";
import useFavoriteFetch from "../hooks/useFavoriteFetch";
import useFavorites from "../hooks/useFavorites";
import useBreakingNewsFetch from "../hooks/useBreakingNewsFetch";
import { filterDuplicates, getRandomInt, removeStationDuplicates } from "../lib/utils";
import Error from "./_error";

export const getServerSideProps = async (context) => {
  let localStationCarouselFrequency;
  let liveNowFrequency;
  let breakingNewsFrequency;
  let favoritesFrequency;
  let config = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/config`
  );
  let {
    featured: {
      carousel: { syntax: carouselTemplates },
    },
  } = await config.json();

  //Remove duplicate items for iteration so we don't make mulitple calls to the api
  //for the same thing
  const templateRemoveDuplicates = Array.from(new Set([...carouselTemplates]));

  //Get the item type and amount of slides per type out of the RTD response
  const templateConfigs = [...carouselTemplates].reduce(
    (a, c) => a.set(c, (a.get(c) || 0) + 1),
    new Map()
  );

  //Now get the item count to set the api call limit
  const itemCount = [...templateConfigs].map(([template, items]) => ({
    template,
    items,
  }));

  // We have to construct the hero carousel from the template by concatenating
  // the various api calls into one array and returning that array to the HeroCarousel
  // component
  for (const item of templateRemoveDuplicates) {
    switch (item) {
      case DB_LOCAL_STATIONS_KEY:
        localStationCarouselFrequency = itemCount.filter(
          (item) => item.template === DB_LOCAL_STATIONS_KEY
        )[0].items;

        break;
      case DB_LIVE_STATIONS_KEY:
        liveNowFrequency = itemCount.filter(
          (item) => item.template === DB_LIVE_STATIONS_KEY
        )[0].items;
        break;
      case DB_BREAKING_NEWS_STATIONS_KEY:
        breakingNewsFrequency = itemCount.filter(
          (item) => item.template === DB_BREAKING_NEWS_STATIONS_KEY
        )[0].items;
        break;
      case DB_FAVORITES_KEY:
        favoritesFrequency = itemCount.filter(
          (item) => item.template === DB_FAVORITES_KEY
        )[0].items;
      default:
        break;
    }
  }

  // If any of these values are 'undefined' they will not serialize to json. This is to catch errrors.
  breakingNewsFrequency = breakingNewsFrequency ?? null;
  favoritesFrequency = favoritesFrequency ?? null;
  liveNowFrequency = liveNowFrequency ?? null;

  return {
    props: {
      data: {
        breakingNewsFrequency,
        favoritesFrequency,
        liveNowFrequency,
        localStationCarouselFrequency,
      },
    },
  };
};

const Home = ({ data, userCoordinates, breakingNewsEnabled }) => {
  const [heroCarouselContent, setHeroCarouselContent] = useState([]);
  const {
    breakingNewsFrequency,
    favoritesFrequency,
    liveNowFrequency,
    localStationCarouselFrequency,
  } = data;

  const breakingNewsQuantity = breakingNewsEnabled ? breakingNewsFrequency : 0;

  //Then get our local stations, geolocated
  const { latitude, longitude } = userCoordinates;

  const {
    data: breakingNewsContent = [],
    isLoading: breakingNewsLoading,
    isError: breakingNewsError,
  } = useBreakingNewsFetch({
    latitude,
    longitude,
  });

  const [favorites] = useFavorites();
  const {
    channels: favoritesContent = [],
    isLoading: favoriteLoading,
    isError: isFavoriteError,
  } = useFavoriteFetch(favorites);
  // Filter for duplicates so none show in the carousel
  const favoritesContentFiltered = removeStationDuplicates(
    favoritesContent, 
    [breakingNewsContent]
  );

  const {
    data: liveNowCarouselContent = [],
    isLoading: liveIsLoading,
    isError: liveIsError,
  } = useLiveFetch({
    latitude, 
    longitude 
  })
  // Filter for duplicates so none show in the carousel
  const liveNowCarouselContentFiltered = removeStationDuplicates(
    liveNowCarouselContent,
    [breakingNewsContent, favoritesContentFiltered]
  );

  const {
    data: localStationCarouselContent = [],
    isLoading,
    isError,
  } = useLocalItemsFetch({ 
    latitude, 
    longitude 
  });
  // Filter for duplicates so none show in the carousel
  const localStationCarouselContentFiltered = removeStationDuplicates(
    localStationCarouselContent,
    [breakingNewsContent, favoritesContentFiltered, liveNowCarouselContentFiltered]
  );

  useEffect(() => {
    if (
      !isLoading &&
      !liveIsLoading &&
      !breakingNewsLoading &&
      (!favoriteLoading || (favoriteLoading && favoritesContent.length === 0))
    ) {

      // Extract two of each category for hero carousel content
      const breakingNewsItems = breakingNewsContent
      .flat()
      .slice(0, breakingNewsQuantity);
      const favoritesItems = favoritesContentFiltered.slice(0, favoritesFrequency);
      const liveNowItems = liveNowCarouselContentFiltered.slice(0, liveNowFrequency);
      const localStationItems = localStationCarouselContentFiltered.slice(
        0, localStationCarouselFrequency
      );

      setHeroCarouselContent([
        ...breakingNewsItems,
        ...favoritesItems,
        ...liveNowItems,
        ...localStationItems,
      ]); 
    }
  }, [isLoading, liveIsLoading, breakingNewsLoading, favoriteLoading]);

  if (
    liveIsError ||
    isError ||
    isFavoriteError ||
    (localStationCarouselContent &&
      localStationCarouselContent.hasOwnProperty("errorCode")) ||
    (liveNowCarouselContent && liveNowCarouselContent.hasOwnProperty("errorCode"))
  ) {
    return <Error />;
  }

  if (isLoading || liveIsLoading) return <div></div>;

  // Add local items to 'Stations near me' swimlane
  const localItems = [...localStationCarouselContent];

  // Check for if any 'live now' stations are also favourites
  const favoriteLiveItems = liveNowCarouselContent.filter((item) =>
    favorites?.includes(item.id)
  );

  // Prepend all the favorites to the front of the 'Live Now' swim lane
  const liveNowItems = filterDuplicates([
    ...favoriteLiveItems,
    ...liveNowCarouselContent,
  ]);

  const stationGroupData = [
    {
      title: STATION_GROUP_TITLES.LOCAL_STATIONS,
      data: localItems,
    },
    {
      title: STATION_GROUP_TITLES.LIVE_NOW,
      data: liveNowItems,
    },
  ];

  return (
    <div className="container">
      <Meta />
      <main>
        <HeroCarouselWrapper>
          <HeroCarousel items={heroCarouselContent} autoPlayInterval={0} breakingNewsEnabled={breakingNewsEnabled} />
        </HeroCarouselWrapper>
        <LayoutCarousel
          stationGroups={stationGroupData}
          favorites={favorites}
        />
      </main>
    </div>
  );
};

export default Home;

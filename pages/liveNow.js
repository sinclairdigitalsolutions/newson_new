import LayoutGrid from "../components/PageLayouts/LayoutGrid";
import Meta from "../components/Meta";
import Header from "../components/Header/Header";
import Error from "./_error";
import useFavorites from "../hooks/useFavorites";
import { filterDuplicates } from "../lib/utils";

import {
  LiveNowEmptySection,
  LiveNowEmptyTitle,
  LiveNowEmptyMessage,
  LiveNowEmptyIcon,
} from "../components/PageLayouts/LiveNowPageStyles";
import { STATION_GROUP_TITLES } from "../lib/constants";
import useLiveFetch from "../hooks/useLiveItemsFetch";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function LiveNow({ userCoordinates, breakingNewsEnabled }) {
  const { latitude, longitude } = userCoordinates;
  // This number refelects the number of cards that appear on the 
  const numOfStations = "999";
  const { data: liveNowContent, isLoading, isError } = useLiveFetch({
    frequency: numOfStations,
    latitude,
    longitude,
  });

  const [favorites] = useFavorites();

  if (isLoading) return <div></div>;

  //check if favorites are in the live now list
  const liveFavorites = liveNowContent.filter((item) =>
    favorites?.includes(item.id)
  );

  //If so prepend it to the list and remove any duplicates
  const liveNowItems = filterDuplicates([...liveFavorites, ...liveNowContent]);

  //TODO add is loading and is Error

  return (
    <div className="container">
      <Meta />
      <Header 
      size="small" 
      pageTitle={STATION_GROUP_TITLES.LIVE_STATIONS} 
      type="generic" 
      breakingNewsEnabled={breakingNewsEnabled}
      />
      {liveNowItems && liveNowItems.length > 0 ? (
        <LayoutGrid
          pageTitle={STATION_GROUP_TITLES.LIVE_STATIONS}
          stationGroupItems={liveNowItems}
          favorites={favorites}
        ></LayoutGrid>
      ) : (
        <LiveNowEmptySection>
          <LiveNowEmptyIcon>Live Now</LiveNowEmptyIcon>
          <LiveNowEmptyTitle>
            There are no live stations right now!
          </LiveNowEmptyTitle>
          <LiveNowEmptyMessage>
            Please check back later to see who is broadcasting live.
          </LiveNowEmptyMessage>
        </LiveNowEmptySection>
      )}
    </div>
  );
}

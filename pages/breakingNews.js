import Meta from "../components/Meta";
import LayoutCarousel from "../components/PageLayouts/LayoutCarousel";
import Header from "../components/Header/Header";
import useBreakingNewsFetch from "../hooks/useBreakingNewsFetch";
import { filterDuplicates } from "../lib/utils";
import useFavorites from "../hooks/useFavorites";

import { STATION_GROUP_TITLES } from "../lib/constants";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function BreakingNews({ userCoordinates, breakingNewsEnabled }) {
  const { latitude, longitude } = userCoordinates;
  const {
    data: breakingNewsContent = [],
    isLoading,
    isError,
  } = useBreakingNewsFetch({
    latitude,
    longitude,
  });
  const [favorites] = useFavorites();

  if (isLoading || !breakingNewsContent) return <div></div>;

  const stationGroupData = breakingNewsContent.map((item) => {
    const breakingNewsFavorites = item.filter((breaking) =>
      favorites?.includes(breaking.id)
    );

    const breakingNewsItems = filterDuplicates([
      ...breakingNewsFavorites,
      ...item,
    ]);

    return {
      title: STATION_GROUP_TITLES.BREAKING_NEWS,
      data: breakingNewsItems,
    };
  });

  return (
    <div className="container">
      <Meta />
      <Header
        type="generic"
        pageTitle={STATION_GROUP_TITLES.BREAKING_NEWS}
        size="small"
        breakingNewsEnabled={breakingNewsEnabled}
      />
      <LayoutCarousel
        pageTitle={STATION_GROUP_TITLES.BREAKING_NEWS}
        stationGroups={stationGroupData}
      />
    </div>
  );
}

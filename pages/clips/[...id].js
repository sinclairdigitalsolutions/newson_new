import Meta from "../../components/Meta";
import LayoutCarousel from "../../components/PageLayouts/LayoutCarousel";
import useFavorites from "../../hooks/useFavorites";
import {
  BANNER_IMAGE_ROOT_URL,
  STATION_GROUP_TITLES,
} from "../../lib/constants";
import { useRouter } from "next/router";
import VideoHeader from "../../components/VideoHeader/VideoHeader";
import { useEffect, useState } from "react";
import OpenGraphMeta from "../../components/OpenGraphMeta/OpenGraphMeta";
import NProgress from "nprogress";

export const getServerSideProps = async (context) => {
  const [id] = context.params.id;
  const [, clipId] = context.params.id;

  const stationItemResponse = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/station/${id}`
  );
  const stationItemsContent = await stationItemResponse.json();

  const stationClipsReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/videoListing/${stationItemsContent.channel.configValue.localvodcategories}`
  );
  const fullDetailsReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/fullDetails/${clipId}`
  );
  const configValuesReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/configValues/${clipId}`
  );

  const configValues = await configValuesReq.json();
  const stationClips = await stationClipsReq.json();
  const fullDetails = await fullDetailsReq.json();
  const host = context.req.headers.host;

  return {
    props: {
      data: {
        stationItemsContent,
        stationClips,
        id,
        clipId,
        host,
        fullDetails,
        configValues,
      },
    },
  };
};

export default function Clips({ data, userCoordinates, breakingNewsEnabled }) {
  const router = useRouter();
  const clipId = parseInt(router.query.id[1], 10);
  const [videoExpired, setVideoExpired] = useState(false);
  const [clip, setClip] = useState(null);
  const {
    stationItemsContent,
    stationItemsContent: {
      latest,
      channel,
      channel: {
        icon,
        name,
        id: channelId,
        configValue: { affiliation },
      },
      programs,
    },
    stationClips: { categories },
    id: stationId,
    host,
    fullDetails: { vodAds },
    configValues: {
      mobile_config: { prerolltimelimit, midrolltimelimit_live },
    },
  } = data;
  const pageUrl = `${host}${router.asPath}`;
  const adHolidays = {
    preroll: prerolltimelimit * 1000,
    midroll: midrolltimelimit_live * 1000,
  }
  const [favorites, FavoritesButton] = useFavorites(parseInt(stationId));
  const getVideoAssetInfo = () => {
    if (latest.id === clipId) {
      return latest;
    }

    const { programs } = stationItemsContent;
    let [program] = programs.filter((program) => {
      return program.id === clipId;
    });

    // filter the one with the correct id
    const vods = categories[0].videos;
    const [asset] = vods.filter((vod) => {
      return vod.id === clipId;
    });

    return program ? program : asset;
  };

  useEffect(() => {
    NProgress.start();

    const asset = getVideoAssetInfo();
    setClip(asset);

    if (!asset && !videoExpired) {
      // TODO: This should mean the video is no longer available or bad id
      setVideoExpired(true);
      NProgress.done();
    }
  }, []);

  const liveNow = latest.live === true ? latest : {};

  const additionalBroadcasts = programs.map((item) => ({
    portraitImage: item.thumbnailUrl,
    ...channel,
    ...item,
    live: false,
    hideIcon: true,
  }));

  const stationGroupData = [
    {
      title: STATION_GROUP_TITLES.LIVE,
      data: [{ liveNow, ...channel, hideIcon: true }, ...additionalBroadcasts],
    },
    { title: STATION_GROUP_TITLES.CLIPS, data: categories[0].videos },
  ];

  return (
    <div className="container">
      <Meta enablePlayer={true}>
        <OpenGraphMeta
          url={pageUrl}
          title={latest?.name}
          description={latest?.description}
          thumbnail={latest?.thumbnailUrl}
        />
      </Meta>
      <VideoHeader
        favoritesButton={<FavoritesButton heartButton={true} />}
        isFavorite={favorites?.includes(channelId)}
        stationItemData={data}
        channel={channel}
        clip={clip}
        videoExpired={videoExpired}
        pageUrl={pageUrl}
        userCoordinates={userCoordinates}
        vodAds={vodAds}
        adHolidays={adHolidays}
        breakingNewsEnabled={breakingNewsEnabled}
      />
      <LayoutCarousel
        pageTitle={name}
        stationGroups={stationGroupData}
      ></LayoutCarousel>
    </div>
  );
}

import {
  ErrorWrapper,
  ErrorTitle,
  ErrorMessage,
  ErrorIcon,
  ErrorButton,
} from "../components/PageLayouts/ErrorPageStyles";
import Router from "next/router";
import { analyticsErrorEvent } from '../lib/analytics';

import ErrorIconSvg from "../public/images/icons/NO-error-exclamation.svg";

const CustomError = ({ statusCode, errorMessage }) => {

  analyticsErrorEvent(errorMessage);

  return (
    <ErrorWrapper>
      <ErrorIcon>
        <ErrorIconSvg />
      </ErrorIcon>
      <ErrorTitle>Something has gone wrong!</ErrorTitle>
      <ErrorMessage>
        We encountered an error while fetching your video.
      </ErrorMessage>
      <ErrorButton onClick={() => Router.back()}>Go Back</ErrorButton>
    </ErrorWrapper>
  );
};

const getInitialProps = ({ res, err }) => {
  let statusCode;
  let errorMessage;

  // If the res variable is defined it means nextjs
  // is in server side
  if (res) {
    statusCode = res.statusCode;
    // errorMessage = res.statusMessage;
  } else if (err) {
    // if there is any error in the app it should
    // return the status code from here
    statusCode = err.statusCode;
    errorMessage = err.msg;
  } else {
    // Something really bad/weird happen and status code
    // cannot be determined.
    statusCode = null;
  }
  return { statusCode, errorMessage };
};

CustomError.getInitialProps = getInitialProps;

export default CustomError;

import Meta from "../../components/Meta";
import { useEffect, useState } from "react";
import LayoutCarousel from "../../components/PageLayouts/LayoutCarousel";
import useFavorites from "../../hooks/useFavorites";
import { STATION_GROUP_TITLES } from "../../lib/constants";
import { useRouter } from "next/router";
import VideoHeader from "../../components/VideoHeader/VideoHeader";
import OpenGraphMeta from "../../components/OpenGraphMeta/OpenGraphMeta";
import NProgress from "nprogress";

export const getServerSideProps = async (context) => {
  const [id] = context.params.id;
  const [, clipId] = context.params.id;

  const stationItemResponse = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/station/${id}`
  );
  const stationItemsContent = await stationItemResponse.json();

  const stationClipsReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/videoListing/${stationItemsContent.channel.configValue.localvodcategories}`
  );
  const fullDetailsReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/fullDetails/${stationItemsContent.latest.id}`
  );

  const fullDetails = await fullDetailsReq?.json();
  const stationClips = await stationClipsReq.json();
  const pageUrl = `${context.req.headers.host}${context.req.url}`;

  return {
    props: { data: { stationItemsContent, stationClips, id, clipId, pageUrl, fullDetails } },
  };
};

export default function Live({ data, userCoordinates, breakingNewsEnabled }) {
  const router = useRouter();
  const liveId = parseInt(router.query.id[1], 10);
  const [videoExpired, setVideoExpired] = useState(false);
  const [live, setLive] = useState(null);
  const {
    stationItemsContent: {
      latest,
      channel,
      channel: {
        icon,
        name,
        id: channelId,
        distance,
        configValue: { affiliation },
      },
    },
    stationClips: { categories },
    id: stationId,
    pageUrl,
  } = data;

  useEffect(() => {
    NProgress.start();

    const asset = getVideoAssetInfo();
    setLive(asset);

    if (!asset && !videoExpired) {
      // TODO: This should mean the video is no longer available or bad id
      setVideoExpired(true);
      NProgress.done();
    }
  }, []);

  const getVideoAssetInfo = () => {
    // Check the url assetId against the latest ID
    if (liveId === latest.id) {
      return latest;
    }

    return null;
  };

  const [favorites, FavoritesButton] = useFavorites(parseInt(stationId));

  const liveNow = latest.live === true ? latest : {};
  const stationGroupData = [
    {
      title: STATION_GROUP_TITLES.LIVE,
      data: [{ liveNow, ...channel }],
    },
    { title: STATION_GROUP_TITLES.CLIPS, data: categories[0].videos },
  ];

  return (
    <div className="container">
      <Meta enablePlayer={true}>
        <OpenGraphMeta
          url={pageUrl}
          title={latest?.name}
          description={latest?.description}
          thumbnail={latest?.thumbnailUrl}
        />
      </Meta>
      <VideoHeader
        favoritesButton={<FavoritesButton heartButton={true} />}
        isFavorite={favorites?.includes(channelId)}
        channel={channel}
        live={live}
        pageUrl={pageUrl}
        videoExpired={videoExpired}
        userCoordinates={userCoordinates}
        breakingNewsEnabled={breakingNewsEnabled}
      />
      <LayoutCarousel
        pageTitle={name}
        stationGroups={stationGroupData}
      ></LayoutCarousel>
    </div>
  );
}

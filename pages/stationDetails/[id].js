import Meta from "../../components/Meta";
import { useRouter } from 'next/router'
import LayoutCarousel from "../../components/PageLayouts/LayoutCarousel";
import Header from "../../components/Header/Header";
import useFavorites from "../../hooks/useFavorites";
import useStationFetch from "../../hooks/useStationFetch";
import useLocalItemsFetch from "../../hooks/useLocalItemsFetch"
import {
  BANNER_IMAGE_ROOT_URL,
  STATION_GROUP_TITLES,
} from "../../lib/constants";
import OpenGraphMeta from "../../components/OpenGraphMeta/OpenGraphMeta";
import { useEffect } from "react";
import { analyticsStationPageView } from '../../lib/analytics'
import { getCallSignFromStationName } from "../../lib/utils";

export const getServerSideProps = async (context) => {
  const {
    params: { id },
  } = context;

  const stationItemResponse = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/station/${id}`
  );
  const stationItemsContent = await stationItemResponse.json();

  const stationClipsReq = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/videoListing/${stationItemsContent.channel.configValue.localvodcategories}`
  );

  const stationClips = await stationClipsReq.json();
  const host = context.req.headers.host;

  return {
    props: { data: { stationItemsContent, stationClips, id, host } },
  };
};

export default function StationDetails({ data, userCoordinates }) {
  const { latitude, longitude } = userCoordinates;
  const {
    stationItemsContent: {
      latest,
      channel,
      channel: {
        icon,
        name,
        id: channelId,
        configValue: { affiliation },
      },
      programs,
    },
    stationClips: { categories },
    id: stationId,
    host,
  } = data;
  const router = useRouter();
  const pageUrl = `${host}${router.asPath}`;

  useEffect(() => {
    analyticsStationPageView(router.pathname, getCallSignFromStationName(name), { dimension5: name })
  }, [router.pathname])

  const { data: stationData, isLoading, isError } = useStationFetch({
    id: stationId,
    latitude,
    longitude,
  });

  const { data: localStationContent, isLoading: localIsLoading, isError: localIsError } = useLocalItemsFetch({
    latitude,
    longitude
  });

  if (isLoading || localIsLoading) return <div></div>;

  const {
    channel: { distance },
  } = stationData;

  const liveNow = latest.live === true ? latest : {};

  const additionalBroadcasts = programs.map((item) => ({
    portraitImage: item.thumbnailUrl,
    ...channel,
    ...item,
    live: false,
    hideIcon: true,
  }));

  const localStationContentFiltered = localStationContent.filter(station => station.id !== channelId )

  const stationGroupData = [
    {
      title: STATION_GROUP_TITLES.LIVE,
      data: [{ liveNow, ...channel, hideIcon: true }, ...additionalBroadcasts],
    },
    { title: `${name} ${STATION_GROUP_TITLES.CLIPS}`, data: categories[0].videos },
    { title: STATION_GROUP_TITLES.LOCAL_STATIONS, data: localStationContentFiltered},
  ];

  const StationSubTitle = () => (
    <h3>
      {affiliation} <span className="separator" /> {distance} Miles Away
    </h3>
  );
  
  const StationIcon = () => (
    <img src={icon} alt={name} className="station-icon" />
  );


  return (
    <div className="container">
      <Meta>
        <OpenGraphMeta
          url={pageUrl}
          title={latest?.name}
          description={latest?.description}
          thumbnail={latest?.thumbnailUrl}
        />
      </Meta>
      <Header
        pageTitle={name}
        size="large"
        type="image"
        bgImgUrl={`${BANNER_IMAGE_ROOT_URL}/channel_background_${channelId}.jpg`}
        subtitle={<StationSubTitle />}
        stationIcon={<StationIcon />}
        liveNow={latest?.live}
        shareButtons={true}
        pageUrl={pageUrl}
        stationId={stationId}
        latestVideoId={latest.id}
      />
      <LayoutCarousel
        pageTitle={name}
        stationGroups={stationGroupData}
      ></LayoutCarousel>
    </div>
  );
}
import Meta from "../components/Meta";
import { useRouter } from "next/router";
import { createVinsonSignature } from "../lib/vinsonRequestSignature";
import { getCollection } from "../lib/collectionRequest";
import Header from "../components/Header/Header";
import LayoutGrid from "../components/PageLayouts/LayoutGrid";
import { getCurrentDate } from "../lib/utils";

const currentDate = getCurrentDate();

export const getServerSideProps = async () => {
  const stateParams = ["latitude=41.8483", "longitude=-87.6517"];
  const stateChannelSignature = createVinsonSignature(
    currentDate,
    stateParams,
    "Channel/ByState"
  );

  const stateChannels = await getCollection(
    "Channel/ByState",
    stateParams,
    stateChannelSignature,
    currentDate
  );

  const stateContent = stateChannels.states;

  return {
    props: { data: stateContent },
  };
};

const Location = ({ data }) => {
  const { pathname } = useRouter();

  const pathArray = pathname.split("/");
  const path = pathArray[pathArray.length - 1];

  return (
    <div className="container">
      <Meta />
      <Header
        gridSectionType="large"
        pageTitle="Browse By Location"
        pathString={path}
        byLocationData={data}
      />

      <LayoutGrid
        pageTitle="Browse By Location"
        stationGroupItems={[{}]}
      ></LayoutGrid>
    </div>
  );
};

export default Location;

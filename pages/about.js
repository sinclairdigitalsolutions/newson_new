import Meta from "../components/Meta";
import Link from "next/link";
import {
  AboutSection,
  AboutMain,
  AboutSectionTitle,
  AboutSectionSecondaryTitle,
  AboutColumn,
  AboutCallout,
  AboutLegalTitle,
  AboutFooterColumn,
  AboutRow,
  BottomWrapper,
  TopWrapper,
} from "../components/PageLayouts/AboutPageStyles";

import Obfuscate from "react-obfuscate";

export const getServerSideProps = async () => {
  return {
    props: { data: {} },
  };
};

export default function About({ data }) {
  const PrivacyPolicyQRCode = () => (
    <img
      src="/images/settings_privacy_policy_qr.png"
      alt={"privacy policy QR code"}
      className="privacy-policy-qr"
    />
  );

  return (
    <div className="container">
      <Meta />

      <AboutMain>
        <TopWrapper>
          <AboutSection className="about__top">
            <AboutColumn className="about__column-left">
              <AboutSectionTitle>About NewsON</AboutSectionTitle>
              <AboutCallout>
                Watch local news for free from anywhere in the country with
                NewsON.
              </AboutCallout>
              <p className="faded-content">
                This free, ad-supported app provides instant access to live and
                on-demand newscasts from over 275 trusted local TV station
                partners in over 165 U.S. markets. Viewers can personalize their
                experience by setting favorite stations, and watch breaking news
                coverage of major events and storms from multiple local
                stations. Stay connected anytime, anywhere without a cable
                subscription or login.
              </p>
            </AboutColumn>
            <AboutColumn className="about__column-right">
              <AboutSectionSecondaryTitle>
                Need Help ?
              </AboutSectionSecondaryTitle>
              <p className="larger-copy">
                Send us an email to briefly describe your experience by letting
                us know what station you’re watching, the time of the newscast,
                and if you’re watching live or on demand. We’ll get back to you
                as soon as we can.
              </p>
              <p className="contact-info">
                Please reach out any time to:
                <span>
                  <Obfuscate email="info@newson.us" />
                </span>
              </p>
            </AboutColumn>
          </AboutSection>
        </TopWrapper>
        <BottomWrapper>
          <AboutSection className="about__bottom">
            <AboutColumn className="about__column-left">
              <AboutRow>
                <AboutFooterColumn>
                  <AboutLegalTitle>Privacy Policy</AboutLegalTitle>
                  <p>
                    For information regarding our privacy policy, please visit:
                    <a href="https://corporate.newson.us/privacy-policy">
                      <a>corporate.newson.us/privacy-policy</a>
                    </a>
                  </p>
                </AboutFooterColumn>
                <PrivacyPolicyQRCode />
              </AboutRow>
            </AboutColumn>

            <AboutColumn>
              <AboutRow>
                <AboutFooterColumn>
                  <AboutLegalTitle>Terms of Use</AboutLegalTitle>
                  <p>
                    To read our software terms of service please visit:
                    <a href="https://corporate.newson.us/terms-of-service">
                      <a>corporate.newson.us/terms-of-service</a>
                    </a>
                  </p>
                </AboutFooterColumn>
                <PrivacyPolicyQRCode />
              </AboutRow>
            </AboutColumn>
          </AboutSection>
        </BottomWrapper>
      </AboutMain>
    </div>
  );
}

import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { term },
  } = req;

  const params = [`term=${term}`];
  let results;

  if ( term.length > 1 ){
    const searchSignature = createVinsonSignature(
      currentDate,
      params,
      "Channel/Search"
    );
    const searchResults = await getCollection(
      "Channel/Search",
      params,
      searchSignature,
      currentDate
    );
  
    results = searchResults.channels;
  }
  else {
    results = [];
  }

  res.statusCode = 200;
  res.json(results);
};

import { loadFirebaseDB } from "../../lib/firebaseDB";

export default async (req, res) => {
  let firebaseInstance = await loadFirebaseDB();

  const config = await fetch(`${process.env.ENVIRONMENT_URL}/api/config`);
  const { appConfig, featured } = await config.json();

  const breaking_news = await firebaseInstance
    .auth()
    .signInAnonymously()
    .then(async () => {
      return await firebaseInstance.database()
      .ref("breakingNews")
      .once("value")
      .then((snapshot) => {
        return snapshot.val();
      });
    });

  const firebaseConfig = { appConfig, breaking_news, featured };

  res.statusCode = 200;
  res.json(firebaseConfig);
};

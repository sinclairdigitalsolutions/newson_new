import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { categories },
  } = req;

  const params = [`categories=[${categories}]`];
  const signature = createVinsonSignature(currentDate, params, "Video/Listing");

  const listingItems = await getCollection(
    "Video/Listing",
    params,
    signature,
    currentDate
  );

  res.statusCode = 200;
  res.json(listingItems);
};

import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { id },
  } = req;
  
  const params = [`key=mobile_config`];
  const signature = createVinsonSignature(currentDate, params, "Config/GetConfigValues");

  const configValues = await getCollection(
    "Config/GetConfigValues",
    params,
    signature,
    currentDate
  );

  res.statusCode = 200;
  res.json(configValues);
};
import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/config`
  );
  let {
    appConfig: {
      collections: {
        1: { key: nearMeStationsID },
      },
    },
  } = await firebase.json();

  const params = [
    `id=${nearMeStationsID}`,
    "latitude=41.8483",
    "longitude=-87.6517",
  ];

  const signature = createVinsonSignature(
    currentDate,
    params,
    "Content/Collection"
  );

  const localItems = await getCollection(
    "Content/Collection",
    params,
    signature,
    currentDate
  );

  const localItemsRes = localItems.content;

  res.statusCode = 200;
  res.json(localItemsRes);
};

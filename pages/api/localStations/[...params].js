import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { params },
  } = req;

  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/config`
  );
  let {
    appConfig: {
      collections: {
        1: { id: nearMeStationsID },
      },
    },
  } = await firebase.json();

  const localParams = [
    `id=${nearMeStationsID}`,
    `latitude=${params[1]}`,
    `longitude=${params[2]}`,
    `limit=${params[0]}`,
  ];

  const signature = createVinsonSignature(
    currentDate,
    localParams,
    "Content/Collection"
  );

  const localItems = await getCollection(
    "Content/Collection",
    localParams,
    signature,
    currentDate
  );

  const localItemsRes = localItems.content;

  res.statusCode = 200;
  res.json(localItemsRes);
};

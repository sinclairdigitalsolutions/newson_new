import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { params },
  } = req;

  const stattionParams = [
    `id=${params[0]}`,
    `latitude=${params[1]}`,
    `longitude=${params[2]}`,
  ];

  const signature = createVinsonSignature(
    currentDate,
    stattionParams,
    "Program/ByChannel"
  );

  const localItems = await getCollection(
    "Program/ByChannel",
    stattionParams,
    signature,
    currentDate
  );

  const localItemsRes = localItems;

  res.statusCode = 200;
  res.json(localItemsRes);
};

import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { params },
  } = req;

  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/firebaseConfig`
  );
  let {
    appConfig: { liveCollection: liveCollectionId },
  } = await firebase.json();

  const liveParams = [
    `id=${liveCollectionId}`,
    `latitude=${params[1]}`,
    `longitude=${params[2]}`,
    `limit=${params[0]}`,
  ];
  const liveSignature = createVinsonSignature(
    currentDate,
    liveParams,
    "Content/Collection"
  );
  const liveNowItems = await getCollection(
    "Content/Collection",
    liveParams,
    liveSignature,
    currentDate
  );

  const liveNowContent = liveNowItems.content;

  res.statusCode = 200;
  res.json(liveNowContent);
};

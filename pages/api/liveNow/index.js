import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { frequency },
  } = req;

  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/firebaseConfig`
  );
  let {
    appConfig: { liveCollection: liveCollectionId },
  } = await firebase.json();

  const liveParams = [
    `id=${liveCollectionId}`,
    "latitude=41.8483",
    "longitude=-87.6517",
    `limit=${frequency}`,
  ];
  const liveSignature = createVinsonSignature(
    currentDate,
    liveParams,
    "Content/Collection"
  );
  const liveNowItems = await getCollection(
    "Content/Collection",
    liveParams,
    liveSignature
  );

  const liveNowContent = liveNowItems.content;

  res.statusCode = 200;
  res.json(liveNowContent);
};

import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { channels },
  } = req;

  const params = [`channels=[${channels}]`];
  const signature = createVinsonSignature(
    currentDate,
    params,
    "Channel/Listing"
  );

  const listingItems = await getCollection(
    "Channel/Listing",
    params,
    signature,
    currentDate
  );

  res.statusCode = 200;
  res.json(listingItems);
};

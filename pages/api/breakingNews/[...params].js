import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { params },
  } = req;

  let firebase = await fetch(
    `${process.env.ENVIRONMENT_URL}/api/config`
  );
  let {
    breakingNews,
  } = await firebase.json();

  const ids = breakingNews.swimlanes.map(item => item.id);

  if (!ids) {
    res.statusCode = 204;
    res.json([]);
    return;
  }

  let requests = ids.map(async (id) => {
    let bnParams = [
      `id=${id}`,
      `latitude=${params[0]}`,
      `longitude=${params[1]}`,
    ];

    const signature = createVinsonSignature(
      currentDate,
      bnParams,
      "Content/Collection"
    );

    const breakingNewsItems = await getCollection(
      "Content/Collection",
      bnParams,
      signature,
      currentDate
    );

    return breakingNewsItems;
  });

  const results = await Promise.all(requests);
  const breakingNewsContent = results.map((item) => item.content);

  res.statusCode = 200;
  res.json(breakingNewsContent);
};

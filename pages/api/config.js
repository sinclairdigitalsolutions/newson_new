export default async (req, res) => {
  const configDevUrl = "https://redspace-newson.akamaized.net/config/staging/config.json";
  const configProdUrl = "https://redspace-newson.akamaized.net/config/config.json";

  const configFetch = await fetch(configDevUrl);
  // TODO: use env variable here to add prod url switch
  const {appConfig, breakingNews, featured, sponsorship} = await configFetch.json();
  const globalConfig = { appConfig, breakingNews, featured, sponsorship };

  res.statusCode = 200;
  res.json(globalConfig);
};

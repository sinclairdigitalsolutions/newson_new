import { getUserLocation } from "../../lib/maxmindRequest";

export default async (req, res) => {
  const coordinates = await getUserLocation(req);

  res.statusCode = 200;
  res.json(coordinates);
};

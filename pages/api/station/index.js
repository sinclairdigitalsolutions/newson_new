import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { id },
  } = req;
  const params = [`id=${id}`];

  const signature = createVinsonSignature(
    currentDate,
    params,
    "Program/ByChannel"
  );

  const stationItems = await getCollection(
    "Program/ByChannel",
    params,
    signature,
    currentDate
  );

  const stationItemsRes = stationItems.content;

  res.statusCode = 200;
  res.json(stationItemsRes);
};

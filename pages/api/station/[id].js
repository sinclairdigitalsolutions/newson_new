import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { id },
  } = req;

  const params = [`id=${id}`];

  const signature = createVinsonSignature(
    currentDate,
    params,
    "Program/ByChannel"
  );

  const localItems = await getCollection(
    "Program/ByChannel",
    params,
    signature,
    currentDate
  );

  const localItemsRes = localItems;

  res.statusCode = 200;
  res.json(localItemsRes);
};

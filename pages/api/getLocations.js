import { createVinsonSignature } from "../../lib/vinsonRequestSignature";
import { getCollection } from "../../lib/collectionRequest";
import { getCurrentDate } from "../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const params = ["latitude=41.8483", "longitude=-87.6517"];

  const signature = createVinsonSignature(
    currentDate,
    params,
    "Channel/ByState"
  );

  const locations = await getCollection(
    "Channel/ByState",
    params,
    signature,
    currentDate
  );

  res.statusCode = 200;
  res.json(locations);
};

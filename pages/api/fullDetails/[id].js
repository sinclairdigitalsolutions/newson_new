import { createVinsonSignature } from "../../../lib/vinsonRequestSignature";
import { getCollection } from "../../../lib/collectionRequest";
import { getCurrentDate } from "../../../lib/utils";

const currentDate = getCurrentDate();

export default async (req, res) => {
  const {
    query: { id },
  } = req;
  
  const params = [`programid=${id}`];
  const signature = createVinsonSignature(currentDate, params, "Program/FullDetail");

  const listingItems = await getCollection(
    "Program/FullDetail",
    params,
    signature,
    currentDate
  );

  res.statusCode = 200;
  res.json(listingItems);
};
import styled from "styled-components";
import {
  tabletBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const LiveNowFlagWrapper = styled.div`
  width: ${(props) => props.stationDetails ? pxToRem(130) : pxToRem(91)};
  background-color: ${(props) => props.theme.colors.liveNowFlagGreen};
  border-radius: ${pxToRem(400)};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${(props) => props.stationDetails ? "0.1875rem 0.875rem 0.125rem 0.875rem" : 0};
  color: ${(props) => props.theme.colors.white};
  z-index: 10;
  margin-bottom: ${(props) => props.stationDetails ? pxToRem(5) : 0};
  margin-top: ${pxToRem(10)};
  user-select: none;

  @media (min-width: ${tabletBreakpoint}){
    width: ${(props) => props.stationDetails ? pxToRem(180) : pxToRem(91)};
  }
`;

export const RedDot = styled.div`
  width: ${(props) => props.stationDetails ? pxToRem(9) : pxToRem(7)};
  height: ${(props) => props.stationDetails ? pxToRem(9) : pxToRem(7)};
  border-radius: 50%;
  background-color: ${(props) => props.theme.colors.liveNowRed};
  margin-right:  ${(props) => props.stationDetails ? pxToRem(9) : pxToRem(7)};
`;

export const LiveNow = styled.div`
  font-family: Nunito-Sans-Regular;
  font-size: ${(props) => props.stationDetails ? pxToRem(17) : pxToRem(11)};
  color: ${(props) => props.theme.colors.white};

  @media (min-width: ${tabletBreakpoint}){
    font-size: ${(props) => props.stationDetails ? pxToRem(26) : pxToRem(11)};
  }
`;


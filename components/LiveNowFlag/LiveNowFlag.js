import React from "react";
import { LiveNowFlagWrapper, LiveNow, RedDot } from "./LiveNowFlagStyles";

const LiveNowFlag = ({stationDetails}) => {
  return (
    <LiveNowFlagWrapper stationDetails={stationDetails}>
      <RedDot stationDetails={stationDetails} />
      <LiveNow stationDetails={stationDetails}>
        Live Now
      </LiveNow>
    </LiveNowFlagWrapper>
  );
};

export default LiveNowFlag;
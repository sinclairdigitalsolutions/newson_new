import React, { useEffect, useState } from "react";
import ForwardArrowIcon from "../../public/images/icons/forward-arrow.svg";
import LinkButton from "../../components/LinkButton/LinkButton";
import FavoriteIcon from "../../public/images/icons/favorites-icon.svg";
import UnFavoriteIcon from "../../public/images/icons/favorites-icon-outline.svg";
import {
  BANNER_IMAGE_ROOT_URL,
  HERO_CAROUSEL_BUTTON_LABEL,
} from "../../lib/constants";

import {
  CarouselItem,
  CarouselItemContent,
  CarouselItemSubtitle,
  CarouselItemTitle,
  FavoritesBadge,
  LinkButtonWrapper
} from "./HeroCarouselStyles";
import LiveNowFlag from "../LiveNowFlag/LiveNowFlag";

const HeroCarouselItem = ({ content, active, isFavorite, favoriteButton, breakingNewsEnabled }) => {
  const {
    description,
    configValue: { affiliation },
    distance,
    id,
    icon,
    name,
    live = false,
  } = content;

  const bannerUrl = `${BANNER_IMAGE_ROOT_URL}/channel_background_${id}.jpg`;

  const StationIcon = () => (
    <img src={icon} alt={name} className="station-icon" />
  );

  const label = live
    ? HERO_CAROUSEL_BUTTON_LABEL.LIVE
    : HERO_CAROUSEL_BUTTON_LABEL.DEFAULT;

  return (
    <CarouselItem bgImageUrl={bannerUrl} active={active} breakingNewsEnabled={breakingNewsEnabled}>
      {isFavorite ? (
        <FavoriteIcon className="favorite-icon favorite-icon__favorited" />
      ) : (
        <UnFavoriteIcon className="favorite-icon" />
      )}
      <StationIcon />
      <CarouselItemContent>
        {isFavorite && (
          <LinkButtonWrapper><FavoritesBadge>Recently Added to favourites</FavoritesBadge></LinkButtonWrapper>
        )}

        {live && <LiveNowFlag stationDetails={true} />}

        <CarouselItemTitle>{description || name}</CarouselItemTitle>
        <CarouselItemSubtitle>
          {affiliation} <span className="separator" /> {distance} Miles Away
        </CarouselItemSubtitle>
        <LinkButtonWrapper>
          <LinkButton
            type="link"
            href="/stationDetails/[id]"
            as={`/stationDetails/${id}`}
            icon={<ForwardArrowIcon className="svg" />}
            label={label}
          />
        </LinkButtonWrapper>
        {favoriteButton}
      </CarouselItemContent>
    </CarouselItem>
  );
};

export default HeroCarouselItem;

import React, { useState, useEffect, useRef } from "react";
import { CarouselContainer, CarouselContent } from "./HeroCarouselStyles";
import { useSwipeable } from "react-swipeable";
import HeroCarouselItem from "./HeroCarouselItem";
import ArrowNav from "./ArrowNav";
import DotNav from "./DotNav";
import useFavorites from "../../hooks/useFavorites";

const HeroCarousel = ({ items, autoPlayInterval, breakingNewsEnabled }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [favorites] = useFavorites();

  const swipeHandlers = useSwipeable({
    onSwipedLeft: () => nextItem(),
    onSwipedRight: () => prevItem(),
  });

  const autoPlayRef = useRef();

  /**
   * Increments the carousel forward one Item
   */
  const nextItem = () => {
    setActiveIndex(activeIndex === items.length - 1 ? 0 : activeIndex + 1);
  };

  /**
   * Decrements the carousel back one Item
   */
  const prevItem = () => {
    setActiveIndex(activeIndex === 0 ? items.length - 1 : activeIndex - 1);
  };

  /**
   * Moves the carousel to an item of specified index
   *
   * @param { Number } index the index of the item to navigate to
   */
  const goToItem = (index) => {
    setActiveIndex(index);
  };

  /**
   * Initiates autoplay if the interval propery is greater than 0
   */
  const play = () => {
    if (autoPlayInterval > 0) {
      autoPlayRef.current();
    }
  };

  useEffect(() => {
    if (autoPlayInterval > 0) {
      autoPlayRef.current = nextItem;
    }
  });

  useEffect(() => {
    const interval = setInterval(play, autoPlayInterval * 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <CarouselContainer {...swipeHandlers}>
      <CarouselContent>
        {items.map((item, i) => {
          return (
            <HeroCarouselItem
              key={`slide-${i}`}
              active={i === activeIndex ? true : false}
              content={item}
              isFavorite={favorites && favorites.includes(item.id)}
              breakingNewsEnabled={breakingNewsEnabled}
            />
          );
        })}
      </CarouselContent>

      <ArrowNav direction="left" handleClick={prevItem} />
      <ArrowNav direction="right" handleClick={nextItem} />
      <DotNav
        items={items}
        activeIndex={activeIndex}
        handleClick={goToItem}
      ></DotNav>
    </CarouselContainer>
  );
};

export default HeroCarousel;

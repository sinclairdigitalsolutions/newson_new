import styled from "styled-components";
import {
  ButtonStyle,
  tabletBreakpoint,
  laptopBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";
import { BANNER_SIZES_IN_PIXELS } from '../../lib/constants';
import { paddingTopSizeInPixels } from '../../lib/utils';

export const HeroCarouselWrapper = styled.div`
  height: ${pxToRem(450)};
  width: 100%;

  @media (min-width: ${tabletBreakpoint}) {
    height: ${pxToRem(640)};
  }
`;

export const CarouselContainer = styled.div`
  height: 100%;
  margin: 0 auto;
  overflow: hidden;
  position: relative;
  width: 100%;
`;

export const CarouselContent = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`;

export const LinkButtonWrapper = styled.div`
  user-select: none;
`;

export const CarouselItem = styled.div`
  background-image: url(${(props) => props.bgImageUrl});
  background-position: 50% 50%;
  background-size: cover;
  display: flex;
  height: 100%;
  opacity: ${(props) => (props.active ? "1" : "0")};
  position: absolute;
  transition: opacity 500ms ease-in-out;
  width: 100%;

  &:before {
    background-image: url("/images/image-header-overlay.png");
    background-position: 50% 100%;
    background-repeat: no-repeat;
    background-size: 100% auto;
    content: "";
    display: block;
    height: 100%;
    position: absolute;
    width: 100%;
    z-index: 1;

    @media (min-device-pixel-ratio: 1.5), (min-resolution: 120dpi) {
      background-image: url("/images/image-header-overlay@2x.png");
    }
  }

  .favorite-icon {
    height: ${pxToRem(25)};
    position: absolute;


    right: ${pxToRem(10)};
    top: ${ ({breakingNewsEnabled}) => pxToRem(paddingTopSizeInPixels(breakingNewsEnabled) + 10)};
    width: ${pxToRem(35)};

    &__favorited {
      fill: red;
      z-index: 499;
    }
  }

  .station-icon {
    display: none;
    max-width: ${pxToRem(125)};
    position: absolute;
    z-index: 10;
    user-select: none;

    @media (min-width: ${laptopBreakpoint}) {
      bottom: ${pxToRem(90)};
      display: block;
      right: ${pxToRem(52)};
    }

    @media (min-width: ${desktopBreakpoint}) {
      bottom: ${pxToRem(85)};
      right: ${pxToRem(162)};
    }
  }
`;

export const CarouselItemContent = styled.div`
  margin-bottom: 3.5rem;
  margin-top: ${ pxToRem(BANNER_SIZES_IN_PIXELS.BREAKING_AND_NAV()) };
  display: flex;
  justify-content: center;
  flex-direction: column;

  position: relative;
  z-index: 5;
  margin-left: 2.5625rem;
  margin-right: 2.5625rem;

  @media (min-width: ${tabletBreakpoint}) {
    margin-left: 3.75rem;
    margin-bottom: 3.75rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    margin-left: 8.9375rem;
    margin-bottom: 6.0625rem;
  }

  a {
    ${ButtonStyle};
    color: ${(props) => props.theme.colors.darkBlue};
    cursor: pointer;
    font-size: 0.6875rem;
    margin-right: ${pxToRem(10)};

    .svg {
      margin-right: 0.4375rem;

      &__red {
        fill: ${(props) => props.theme.colors.red};
        width: ${pxToRem(14)};
        height: ${pxToRem(11)};
      }
    }

    @media (min-width: ${tabletBreakpoint}) {
      font-size: 0.875rem;
      line-height: 1.1875rem;
    }

    @media (min-width: ${desktopBreakpoint}) {
      font-size: 1rem;
    }
  }
`;

export const CarouselItemTitle = styled.h2`
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(25)};
  font-weight: normal;
  line-height: 3.1875rem;
  margin-block-start: 0;
  margin: 0;
  user-select: none;

  @media (min-width: ${tabletBreakpoint}) {
    font-size: 3.8125rem;
    line-height: 5.25rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    font-family: Nunito-Sans-Bold;
    font-size: 5.375rem;
    font-weight: bold;
    line-height: 7.3125rem;
  }
`;

export const CarouselItemSubtitle = styled.h3`
  align-items: center;
  display: flex;
  font-family: Nunito-Sans-Regular;
  font-size: 0.8125rem;
  line-height: 1.125rem;
  margin-block-start: 0;
  margin-bottom: 1.25rem;
  margin-top: 0;
  user-select: none;

  .separator {
    background: ${(props) => props.theme.colors.white};
    border-radius: 50%;
    display: inline-block;
    height: 0.625rem;
    margin: 0 0.625rem;
    width: 0.625rem;
  }

  @media (min-width: ${tabletBreakpoint}) {
    font-size: 0.875rem;
    line-height: 1.1875rem;
    margin-bottom: 2rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    font-size: 1.5rem;
    line-height: 1;
    margin-bottom: 2.5rem;
  }
`;

export const Arrow = styled.div`
  display: flex;
  position: absolute;
  top: 50%;
  height: 3.125rem;
  width: 3.125rem;
  justify-content: center;
  ${(props) =>
    props.direction === "right" ? "right: 1.5625rem" : "left: 1.5625rem"};
  cursor: pointer;
  align-items: center;
  transition: transform ease-in 100ms;
  color: blue;
  z-index: 5;
  visibility: hidden;

  &:hover {
    transform: scale(1.1);
  }

  @media (min-width: ${desktopBreakpoint}) {
    visibility: visible;
  }
`;

export const Dot = styled.span`
  background: ${(props) =>
    props.active ? props.theme.colors.white : "transparent"};
  border-radius: 50%;
  border: solid 0.125rem ${(props) => props.theme.colors.white};
  cursor: pointer;
  height: 0.6875rem;
  margin-right: 0.75rem;
  width: 0.6875rem;

  @media (min-width: ${tabletBreakpoint}) {
    height: 0.625rem;
    margin-right: 0.875rem;
    width: 0.625rem;
  }
`;

export const Dots = styled.div`
  align-items: center;
  bottom: 0.6875rem;
  display: flex;
  justify-content: center;
  position: absolute;
  width: 100%;
  z-index: 5;

  @media (min-width: ${tabletBreakpoint}) {
    bottom: 1.25rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    bottom: 1.5625rem;
  }
`;

export const FavoritesBadge = styled.span`
  background-color: ${(props) => props.theme.colors.blue};
  border-radius: 1.625rem;
  color: ${(props) => props.theme.colors.white};
  font-family: Nunito-Sans-Regular;
  font-size: 0.6875rem;
  line-height: 0.9375rem;
  padding: 0.1875rem 0.875rem 0.125rem 0.875rem;
  user-select: none;

  @media (min-width: ${tabletBreakpoint}) {
    font-size: 0.8125rem;
    line-height: 1.125rem;
  }
`;

import React from "react";
import HeroCarousel from "./HeroCarousel";
import { ThemeProvider } from "styled-components";
import theme from "../../globalStyles/theme";
import { mockItems } from "./HeroCarouselMockData";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);

const setup = () =>
  render(
    <ThemeProvider theme={theme}>
      <HeroCarousel items={mockItems} />
    </ThemeProvider>
  );

beforeEach(() => {
  console.error = jest.fn();
});

describe("Hero Carousel Component", () => {
  it("should render slide titles", () => {
    const { getByText } = setup();

    expect(getByText("WSBT - South Bend"));
    expect(getByText("WNDU - South Bend"));
    expect(getByText("WISN - Milwaukee"));
    expect(getByText("WIFR - Rockford"));
  });

  it("should render arrow navigation buttons", () => {
    const { getByTestId } = setup();

    expect(getByTestId("arrow-nav-right"));
    expect(getByTestId("arrow-nav-left"));
  });

  it("should render Watch Now buttons", () => {
    const { getAllByText } = setup();

    expect(getAllByText("Watch Now")).toHaveLength(4);
  });

  it("Should render the dot navigation", () => {
    const { getByTestId } = setup();

    expect(getByTestId("dot-nav"));
  });
});

import React from "react";
import { Dot, Dots } from "./HeroCarouselStyles";

const DotItem = ({ active, goToItem }) => (
  <Dot onClick={goToItem} active={active} />
);

const DotNav = ({ items, activeIndex, handleClick }) => {
  return (
    <Dots data-testid="dot-nav">
      {items.map((item, i) => (
        <DotItem
          goToItem={() => handleClick(i)}
          key={`item${i}`}
          active={activeIndex === i}
        ></DotItem>
      ))}
    </Dots>
  );
};

export default DotNav;

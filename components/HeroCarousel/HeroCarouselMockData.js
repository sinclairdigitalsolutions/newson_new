export const mockItems = [
  {
    live: false,
    latest: {
      id: 16281593,
      channelId: 192,
      name: "WSBT 22 First in the Morning 6am",
      description: "",
      startTime: 1595325600,
      endTime: 1595329200,
      hashTags: "#wsbt",
      live: false,
      streamUrl:
        "http://newson-vsms.trafficmanager.net/WSBT_1595332152_2af837e2eccbcf5f212ea5eb0aebccda0d2dfd34fdeecac4a20d25b1ccdf8dfb.m3u8?a=b&starttimestampdef=20200721100000000&endtimestampdef=20200721110000000",
      thumbnailUrl:
        "http://static.newson.akm.cdn.vinsontv.com/thumbnails192/1595325960.jpg",
    },
    id: 192,
    name: "WSBT - South Bend",
    icon:
      "http://static.newson.akm.cdn.vinsontv.com/icons/v3/sinclair/WSBT.png",
    configValue: {
      localvodcategories: [243],
      callsign: "WSBT",
      stationgroup: "Sinclair",
      latitude: 41.67503,
      longitude: -86.251961,
      region: "Midwest",
      locations: [
        {
          state: "Indiana",
          city: "South Bend",
        },
      ],
      affiliation: "CBS",
      midrollsenabled: true,
      midrollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=180000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DSinclair%26stationgroup%3DWSBT%26isdai%3Dfalse%26correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_mobile: "",
      midrollurl_mobile_live: "",
      prerollsenabled: true,
      programstartprerollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DSinclair%26stationgroup%3DWSBT&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DSinclair%26stationgroup%3DWSBT&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_mobile: "",
      programstartprerollurl_mobile_live: "",
      midrollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=120000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DSinclair%26stationgroup%3DWSBT&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=preroll",
      midrollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=midroll",
      midrollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Sinclair/WSBT&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DSinclair%26stationgroup%3DWSBT&unviewed_position_start=1&vpos=midroll",
    },
    distance: 73,
    description: "WSBT - South Bend",
  },
  {
    live: false,
    latest: {
      id: 16300671,
      channelId: 334,
      name: "16 Morning News Now",
      description: "",
      startTime: 1595320200,
      endTime: 1595329200,
      hashTags: null,
      live: false,
      streamUrl:
        "http://newson-vsms.trafficmanager.net/WNDU_1595332152_135efb2042bf27fa9339acfd67f31ebaa6ea1d5c7a209902dc9eb10a9c6ee963.m3u8?a=b&starttimestampdef=20200721083000000&endtimestampdef=20200721110000000",
      thumbnailUrl:
        "http://static.newson.akm.cdn.vinsontv.com/thumbnails334/1595321100.jpg",
    },
    id: 334,
    name: "WNDU - South Bend",
    icon: "http://static.newson.akm.cdn.vinsontv.com/icons/v3/gray/WNDU.png",
    configValue: {
      localvodcategories: [451],
      callsign: "WNDU",
      stationgroup: "Gray",
      latitude: 41.700944,
      longitude: -86.21381,
      region: "Midwest",
      locations: [
        {
          state: "Indiana",
          city: "South Bend",
        },
      ],
      affiliation: "NBC",
      midrollsenabled: true,
      midrollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=180000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWNDU%26isdai%3Dfalse%26correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=120000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWNDU&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_mobile: "",
      midrollurl_mobile_live: "",
      midrollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=midroll",
      midrollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=midroll",
      prerollsenabled: true,
      programstartprerollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWNDU&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWNDU&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_mobile: "",
      programstartprerollurl_mobile_live: "",
      programstartprerollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WNDU&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWNDU&unviewed_position_start=1&vpos=preroll",
    },
    distance: 75,
    description: "WNDU - South Bend",
  },
  {
    live: true,
    latest: {
      id: 16242619,
      channelId: 21,
      name: "WISN 12 News This Morning",
      description:
        "Get the top Milwaukee news weather and sports. With the day's biggest stories and ones you may have missed, our Wisconsin news team has you covered.",
      startTime: 1595329190,
      endTime: 1595332804,
      hashTags: "#wisn,#Milwaukee,#wisn12news",
      live: true,
      streamUrl:
        "http://newson-vsms.trafficmanager.net/WISN_1595332157_4c2ade0f3ac7e7e67738bf82d8b3c40380ed9e926e2c8aeb6de04c6e5cb48c5a.m3u8?a=b",
      thumbnailUrl:
        "http://static.newson.akm.cdn.vinsontv.com/thumbnails21/1595329551.jpg",
    },
    id: 21,
    name: "WISN - Milwaukee",
    icon: "http://static.newson.akm.cdn.vinsontv.com/icons/v3/hearst/WISN.png",
    configValue: {
      localvodcategories: [26],
      callsign: "WISN",
      stationgroup: "Hearst",
      latitude: 43.0454,
      longitude: -87.8981,
      region: "Midwest",
      locations: [
        {
          state: "Wisconsin",
          city: "Milwaukee",
        },
      ],
      affiliation: "ABC",
      midrollsenabled: true,
      midrollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=180000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DHearst_TV%26stationgroup%3DWISN%26isdai%3Dfalse%26correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_roku_live_disabled:
        "http://pubads.g.doubleclick.net/gampad/ads?slotname=/82114269/rrafcs/bf/71845_hrst_mid_live&sz=1920x1080&tfcd=ROKU_ADS_KIDS_CONTENT&url=http://roku.com&unviewed_position_start=1&output=xml_vmap1&impl=s&env=vp&gdfp_req=1&ad_rule=1&description_url=http%3A%2F%2Fapps.roku.com&vad_type=linear&ss_req=1&ip=ROKU_ADS_EXTERNAL_IP&nofb=0&sdkv=roku&min_ad_duration=0&max_ad_duration=60000&rdid=ROKU_ADS_TRACKING_ID&is_lat=ROKU_ADS_LIMIT_TRACKING&idtype=rida&correlator=ROKU_ADS_TIMESTAMP&scor=ROKU_ADS_TIMESTAMP&pod=POD_NUM&ppos=POD_POSITION&cust_params=genre%3DROKU_ADS_CONTENT_GENRE%26content%3dROKU_ADS_CONTENT_ID%26length%3dROKU_ADS_CONTENT_LENGTH%26device%3dROKU_ADS_DEVICE_MODEL%26ua%3DROKU_ADS_USER_AGENT%26ai%3DROKU_ADS_APP_ID",
      midrollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=120000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_mobile: "",
      midrollurl_mobile_live: "",
      prerollsenabled: true,
      programstartprerollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_mobile: "",
      programstartprerollurl_mobile_live: "",
      programstartprerollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=preroll",
      midrollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=midroll",
      midrollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=midroll",
      programstartprerollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=preroll",
      midrollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Hearst_TV/WISN&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DHearst_TV%26stationgroup%3DWISN&unviewed_position_start=1&vpos=midroll",
    },
    distance: 84,
    description: "WISN - Milwaukee",
  },
  {
    live: true,
    latest: {
      id: 16300595,
      channelId: 327,
      name: "23 News This Morning",
      description:
        "Overnight and early morning news events affecting the region, the state and the nation, along with timely reports from the business sector and the latest information on Rockford area weather and traffic issues are provided by the 23 Morning News Team",
      startTime: 1595329200,
      endTime: 1595332800,
      hashTags: null,
      live: true,
      streamUrl:
        "http://newson-vsms.trafficmanager.net/WIFR_1595332158_5a778c7782c74eac2317f158621f0d8824c72710c0d729099d7f9517b48f998d.m3u8?a=b",
      thumbnailUrl:
        "http://static.newson.akm.cdn.vinsontv.com/thumbnails327/1595329560.jpg",
    },
    id: 327,
    name: "WIFR - Rockford",
    icon: "http://static.newson.akm.cdn.vinsontv.com/icons/v3/gray/WIFR.png",
    configValue: {
      localvodcategories: [444],
      callsign: "WIFR",
      stationgroup: "Gray",
      latitude: 42.296051,
      longitude: -89.173576,
      region: "Midwest",
      locations: [
        {
          state: "Illinois",
          city: "Rockford",
        },
      ],
      affiliation: "CBS",
      midrollsenabled: true,
      midrollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=180000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWIFR%26isdai%3Dfalse%26correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=120000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=midroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWIFR&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      midrollurl_mobile: "",
      midrollurl_mobile_live: "",
      midrollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=midroll",
      midrollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=midroll",
      midrollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=midroll",
      prerollsenabled: true,
      programstartprerollurl_roku:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3DVOD%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWIFR&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_roku_live:
        "https://pubads.g.doubleclick.net/gampad/live/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&sz=1920x1080&env=vp&output=xml_vast4&pmnd=0&pmxd=30000&sdmax=0&url=https://watchnewson.com/&tfcd=0&npa=0&gdfp_req=1&nofb=1&unviewed_position_start=1&vpos=preroll&description_url=https://watchnewson.com/&cust_params=v_type%3Dlive%26platform%3Droku%26pubgroup%3DGray%26stationgroup%3DWIFR&isdai=false&correlator=&rdid=https://tv.rlcdn.com/api/ctvid?pid=710527%26ctvid=ROKU_ADS_TRACKING_ID",
      programstartprerollurl_mobile: "",
      programstartprerollurl_mobile_live: "",
      programstartprerollurl_appletv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_appletv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dappletv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3DVOD%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=preroll",
      programstartprerollurl_firetv_live:
        "https://pubads.g.doubleclick.net/gampad/ads?iu=/4756/OTT_Gateway/NewsOn/Gray/WIFR&description_url=https://www.newson.us&env=vp&url=https://www.newson.us&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=1920x1080&cust_params=v_type%3Dlive%26platform%3Dfiretv%26pubgroup%3DGray%26stationgroup%3DWIFR&unviewed_position_start=1&vpos=preroll",
    },
    distance: 84,
    description: "WIFR - Rockford",
  },
];

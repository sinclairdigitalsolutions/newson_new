import React from "react";
import RightArrow from "../../public/images/icons/hero-carousel-right.svg";
import LeftArrow from "../../public/images/icons/hero-carousel-left.svg";
import { Arrow } from "./HeroCarouselStyles";

const ArrowNav = ({ direction, handleClick }) => {
  return (
    <Arrow
      onClick={handleClick}
      direction={direction}
      data-testid={direction === "right" ? "arrow-nav-right" : "arrow-nav-left"}
    >
      {direction === "right" ? <RightArrow /> : <LeftArrow />}
    </Arrow>
  );
};

export default ArrowNav;

import React from "react";
import RightArrow from "../../public/images/icons/NOweb-carousel-right.svg";
import LeftArrow from "../../public/images/icons/NOweb-carousel-left.svg";
import { Arrows } from "./StationGroupCarouselStyles";

const CarouselArrow = ({ direction, onClick }) => {
  return (
    <Arrows onClick={() => onClick()} direction={direction}>
      {direction === "right" ? <RightArrow /> : <LeftArrow />}
    </Arrows>
  );
};

export default CarouselArrow;

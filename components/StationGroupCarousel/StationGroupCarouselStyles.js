import styled from "styled-components";
import { tabletBreakpoint } from "../../globalStyles/sharedStyles";

export const CarouselContainer = styled.div`
  overflow: hidden;
  transition: transform ease-out 0.5s;
`;

export const CarouselArrowContainer = styled.div`
  width: 3.125rem;
  height: 100%;
  background: red;
  position: absolute;
  top: 0;
  z-index: 2;
  ${(props) => (props.direction === "right" ? "right: 0" : "left: 0")};
`;

export const Arrows = styled.div`
  display: none;
  position: absolute;
  height: 100%;
  width: 3.125rem;
  transform: scale(1.5);
  justify-content: center;
  ${(props) => (props.direction === "right" ? "right: 0" : "left: 0")};
  cursor: pointer;
  align-items: center;
  transition: transform ease-in 100ms;
  color: blue;
  background: rgba(0, 0, 0, 0.81);

  &:hover {
    background: rgba(0, 0, 0, 1);
  }

  @media (min-width: ${tabletBreakpoint}) {
    display: flex;
  }
`;

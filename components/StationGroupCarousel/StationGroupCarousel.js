import React, { useEffect, useRef, useState } from "react";
import Carousel from "react-multi-carousel";
import { debounce } from "../../lib/utils";
import CarouselArrow from "./StationGroupCarouselArrows";
import { CarouselContainer } from "./StationGroupCarouselStyles";
import {
  tabletBreakpoint,
  desktopBreakpoint,
} from "../../globalStyles/sharedStyles";

export const StationGroupCarousel = ({ children }) => {
  const ref = useRef(null);
  const [carouselWidth, setCarouselWidth] = useState(1920);

  useEffect(() => {
    const initialWidthValue = window?.innerWidth ?? 1920;
    setCarouselWidth(initialWidthValue);
  }, []);

  const useCurrentWidth = () => {
    useEffect(() => {
      const getWidth = () => (ref.current ? ref.current.offsetWidth : 0);

      const debouncedHandleResize = debounce(function resizeListener() {
        setCarouselWidth(getWidth());
      }, 0);
      window.addEventListener("resize", debouncedHandleResize);
      return () => {
        window.removeEventListener("resize", debouncedHandleResize);
      };
    }, []);
    return carouselWidth;
  };

  // Desktop amd Tablet Settings
  const carouselMargins = 108;
  const tileWidth = 395;
  const tilesShowingOnCarousel =
    (useCurrentWidth() - carouselMargins) / tileWidth;
  const tilesShowingOnCarouselRounded = Math.floor(tilesShowingOnCarousel);
  const partialTileToShow = Math.floor(
    (tilesShowingOnCarousel - tilesShowingOnCarouselRounded) * 100
  );

  // Mobile Settings
  const mobileTileWidth = 290;
  const mobileTilesShowing = useCurrentWidth() / mobileTileWidth;
  const mobileTilesOnCarouselRounded = Math.floor(mobileTilesShowing);
  const mobilePartialTileToShow = Math.floor(
    (mobileTilesShowing - mobileTilesOnCarouselRounded) * 100
  );

  const useTabletBreakpoint = parseFloat(tabletBreakpoint);
  const useDesktopBreakpoint = parseFloat(desktopBreakpoint);

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: useDesktopBreakpoint },
      items: tilesShowingOnCarouselRounded,
      slidesToSlide: tilesShowingOnCarouselRounded - 0.25,
      partialVisibilityGutter: partialTileToShow,
    },
    tablet: {
      breakpoint: { max: useDesktopBreakpoint, min: useTabletBreakpoint },
      items: tilesShowingOnCarousel,
      slidesToSlide: tilesShowingOnCarouselRounded - 0.25,
      partialVisibilityGutter: partialTileToShow,
    },
    mobile: {
      breakpoint: { max: useTabletBreakpoint, min: 0 },
      items: mobileTilesOnCarouselRounded + 0.15,
      slidesToSlide: 1,
      partialVisibilityGutter: mobilePartialTileToShow,
    },
  };

  return (
    <CarouselContainer ref={ref}>
      <Carousel
        customRightArrow={<CarouselArrow direction="right" />}
        customLeftArrow={<CarouselArrow direction="left" />}
        responsive={responsive}
        keyBoardControl={true}
        swipeable={true}
        customTransition="transform 1000ms ease-in-out"
        transitionDuration={1000}
        partialVisible={true}
      >
        {children}
      </Carousel>
    </CarouselContainer>
  );
};

export default StationGroupCarousel;

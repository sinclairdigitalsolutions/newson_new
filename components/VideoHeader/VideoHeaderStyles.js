import styled from "styled-components";
import {
  tabletBreakpoint,
  desktopBreakpoint,
  pxToRem,
  laptopBreakpoint,
} from "../../globalStyles/sharedStyles";
import { paddingTopSizeInPixels  } from "../../lib/utils";


export const VideoHeaderWrapper = styled.div`

  display: flex;
  flex-direction: column-reverse;
  width: 100%;

  justify-content: center;
  align-items: stretch;
  padding: ${({ breakingNewsEnabled }) => pxToRem(paddingTopSizeInPixels(breakingNewsEnabled))} 0 ${pxToRem(32)} 0;

  background: transparent
    linear-gradient(56deg, #19002f 0%, #191048a5 41%, #192f7600 100%);
  background-color: ${(props) => props.theme.colors.turqoise};
  opacity: 1;

  @media (min-width: ${laptopBreakpoint}) {
    padding-top: ${({ breakingNewsEnabled }) => pxToRem(paddingTopSizeInPixels(breakingNewsEnabled) + 32)};

    flex-direction: row;
  }
`;

export const PlayerWrapper = styled.div`
  position: relative;
`;

export const VideoMetaSection = styled.div`
  width: 100%;
  background: ${(props) => props.theme.colors.videoMetaGrey} 0% 0% no-repeat
    padding-box;
  padding: 0;
  position: relative;
  margin-right: -1px;
  user-select: none;

  @media (min-width: ${laptopBreakpoint}) {
    width: ${pxToRem(301)};
    height: ${pxToRem(382.5)};
  }
`;

export const TextWrapper = styled.div`
  padding: ${pxToRem(30)};
  display: flex;
  flex-direction: column;
  overflow-wrap: break-word;
`;

export const Station = styled.div`
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(22)};
  margin-bottom: ${pxToRem(10)};
  margin-top: ${(props) => props.isLive ? pxToRem(14): 0};
`;

export const Distance = styled.div`
  font-family: Nunito-Sans-Regular;
  color: ${(props) => props.theme.colors.subtextGrey};
  font-size: ${pxToRem(13)};
`;

export const AirTime = styled.div`
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(13)};
  color: white;
  margin-top: ${pxToRem(63)};
  display: ${(props) => props.isClip ? "none" : "block"};
`;

export const Description = styled.p`
  font-family: Nunito-Sans-Italic;
  font-size: ${pxToRem(13)};
  color: white;
  margin-top: ${pxToRem(30)};
  display: ${(props) => props.isClip ? "block" : "none"}
`

export const ShareAndFavouriteWrapper = styled.div`
  padding: ${pxToRem(30)};
  position: absolute;
  bottom: 0;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;

  @media (min-width: ${laptopBreakpoint}) {
    position: absolute;
    bottom: 0;
  }
`;

export const ShareButton = styled.div`
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(13)};
  color: black;
  background-color: white;
  width: ${pxToRem(116)};
  height: ${pxToRem(30)};
  border-radius: ${pxToRem(40)};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  outline: none;
  user-select: none;
`;

import React, { useEffect } from "react";
import PlayerController from "../Player/PlayerController/PlayerController";
import {
  VideoHeaderWrapper,
  VideoMetaSection,
  Station,
  Distance,
  AirTime,
  ShareAndFavouriteWrapper,
  PlayerWrapper,
  TextWrapper,
  ShareButton,
  Description,
} from "./VideoHeaderStyles";
import { Share } from "react-twitter-widgets";
import LiveNowFlag from "../LiveNowFlag/LiveNowFlag";
import useStationFetch from "../../hooks/useStationFetch";

const VideoHeader = ({
  clip,
  live,
  channel,
  favoritesButton,
  pageUrl,
  videoExpired,
  userCoordinates,
  vodAds,
  adHolidays,
  breakingNewsEnabled
}) => {
  const { latitude, longitude } = userCoordinates;
  const {
    name,
    id,
    configValue: { affiliation },
  } = channel;

  const weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  let startTime = live?.startTime ?? clip?.releaseTime;
  let date = new Date(startTime * 1000);
  let time =
    date.toLocaleTimeString().slice(0, 5) +
    date.toLocaleTimeString().slice(8, 11);

  const weekday = weekdays[date.getDay()];
  const month = months[date.getUTCMonth()];
  const day = date.getUTCDate();
  const isClip = Boolean(clip);
  const isLive = Boolean(live);
  const regex = /([#])/gm;
  const tags = live?.hashTags ?? clip?.hashTags;
  const hashTags = tags?.replace(regex, "");

  useEffect(() => {
    window.FB?.XFBML.parse();
  }, []);

  const { data: stationData, isLoading, isError } = useStationFetch({
    id,
    latitude,
    longitude,
  });

  const shareButton = `<div class="fb-share-button" data-href={${pageUrl}} data-layout="button" />`;
  if (isLoading) return <div></div>;

  const {
    channel: { distance },
  } = stationData;

  return (
    <VideoHeaderWrapper breakingNewsEnabled={breakingNewsEnabled}>
      {(live || clip) && (
        <VideoMetaSection>
          <TextWrapper>
            {live?.live && <LiveNowFlag stationDetails={false} />}

            <Station isLive={live?.live}>{name}</Station>

            <Distance>{`${affiliation} • ${distance} Miles Away`}</Distance>

            <AirTime
              isClip={isClip}
            >{`${weekday}, ${month} ${day} - ${time}`}</AirTime>

            <Description isClip={isClip}>{clip?.displayName}</Description>
          </TextWrapper>

          {!(videoExpired && pageUrl) && (
            <ShareAndFavouriteWrapper>
              <div
                style={{ marginBottom: "4px", marginRight: "5px" }}
                dangerouslySetInnerHTML={{ __html: shareButton }}
              />
              <Share
                url={pageUrl}
                options={{
                  text: isLive ? live?.name : clip?.name,
                  hashtags: hashTags,
                }}
              />

              {favoritesButton && favoritesButton}
            </ShareAndFavouriteWrapper>
          )}
        </VideoMetaSection>
      )}
      <PlayerWrapper>
        <PlayerController
          live={live}
          clip={clip}
          channel={channel}
          videoExpired={videoExpired}
          vodAds={vodAds}
          adHolidays={adHolidays}
        />
      </PlayerWrapper>
    </VideoHeaderWrapper>
  );
};

export default VideoHeader;

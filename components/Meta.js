import React from "react";
import Head from "next/head";
import { FACEBOOK_SDK_URL } from "../lib/constants";

const Meta = ({ enablePlayer = false, children }) => {
  return (
    <Head>
      <title>News ON</title>
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="/favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/favicon-16x16.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="192x192"
        href="/android-chrome-192x192.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="512x512"
        href="/android-chrome-512x512.png"
      />
      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="/apple-touch-icon.png"
      />

      {enablePlayer ? (
        [<script src="https://amp.akamaized.net/hosted/1.1/player.esi?apikey=sinclair.newson.trial&version=9.0.16"></script>,
        <script src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>]
      ) : null}

      <link rel="stylesheet" type="text/css" href="/nprogress.css" />

      <script crossOrigin="anonymous" src={FACEBOOK_SDK_URL} nonce="hHx3IwB5"></script>

      <div id="fb-root"></div>

      {React.Children.map(children, (child) => {
        return React.cloneElement(child);
      })}
    </Head>
  );
};

export default Meta;

import styled from "styled-components";
import {
  tabletBreakpoint,
  laptopBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const StationGroupsContainer = styled.div`
  padding: 1.0625rem 0 0 0;
  margin-left:${pxToRem(15)};

  @media (min-width: ${laptopBreakpoint}){
    margin-left:${pxToRem(30)};
  }
`;

import styled from "styled-components";
import {
  EmptyResults,
  EmptyResultsTitle,
  EmptyResultsMessage,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const EmptySearchResults = styled.div`
  ${EmptyResults};
  padding-top: 10vw;
`;

export const EmptySearchResultsTitle = styled.h3`
  ${EmptyResultsTitle};
`;

export const EmptySearchResultsMessage = styled.p`
  ${EmptyResultsMessage};
  margin: 0;
`;

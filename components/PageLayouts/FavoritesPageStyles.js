import styled from "styled-components";
import {
  tabletBreakpoint,
  pxToRem,
  EmptyResults,
  EmptyResultsTitle,
  EmptyResultsMessage,
  laptopBreakpoint,
} from "../../globalStyles/sharedStyles";

export const FavoritesEmptyIcon = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  margin-top: ${pxToRem(75)};
  
  .svg {
    fill: ${(props) => props.theme.colors.red};
    height: ${pxToRem(75)};
    width: ${pxToRem(75)};
  }

  @media (min-width: ${tabletBreakpoint}) {

    margin-top: ${pxToRem(150)};
    .svg {
      height: ${pxToRem(150)};
      width: ${pxToRem(150)};
    }
  }
`;

export const FavoritesEmptyTitle = styled.h3`
  ${EmptyResultsTitle}
`;

export const FavoritesEmptyMessage = styled.p`
  display: none;
  ${EmptyResultsMessage}
  @media (min-width: ${laptopBreakpoint}){
    display: flex;
  }
`;

export const FavoritesEmptyMessageMobile = styled.p`
  ${EmptyResultsMessage}
  @media (min-width: ${laptopBreakpoint}){
    display: none;
  }
`;

export const FavoritesEmptySection = styled.div`
  ${EmptyResults};
`;

import styled from "styled-components";
import {
  EmptyResults,
  EmptyResultsTitle,
  EmptyResultsMessage,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const ErrorWrapper = styled.div`
  ${EmptyResults};
  display: flex;
  height: 100vh;
  width: 100vw;
  background: transparent
    linear-gradient(58deg, #070a40 0%, #14467c 41%, #079c94 100%) 0% 0%
    no-repeat padding-box;
  max-width: 100%;
`;

export const ErrorIcon = styled.div`
  margin-bottom: ${pxToRem(30)};
`;

export const ErrorTitle = styled.h1`
  ${EmptyResultsTitle};
  font-size: ${pxToRem(56)};
  line-height: ${pxToRem(77)};
  margin: 0 0 ${pxToRem(5)} 0;
`;

export const ErrorMessage = styled.p`
  ${EmptyResultsMessage};
  font-size: ${pxToRem(30)};
  line-height: ${pxToRem(41)};
  margin: 0;
`;

export const ErrorButton = styled.button`
  background-color: ${(props) => props.theme.colors.white};
  border: 0;
  border-radius: ${pxToRem(80)};
  color: ${(props) => props.theme.colors.navy};
  cursor: pointer;
  font-size: ${pxToRem(26)};
  line-height: ${pxToRem(35)};
  padding: ${pxToRem(15)} ${pxToRem(64)};
  margin-top: ${pxToRem(100)};
`;

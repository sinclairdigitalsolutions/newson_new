import StationGroup from "../StationGroup";
import { StationGroupsContainer } from "./LayoutCarouselStyles";

export const LayoutCarousel = ({ stationGroups, favorites }) => {
  return (
    <StationGroupsContainer>
      {stationGroups.map((group, index) => {
        if(!Boolean(group.data.length)){
          return
        }
        return (
          <StationGroup
            key={index}
            stationGroupData={group.data}
            stationGroupStyle="carousel"
            stationGroupTitle={group.title}
            favorites={favorites}
          />
        );
      })}
    </StationGroupsContainer>
  );
};

export default LayoutCarousel;

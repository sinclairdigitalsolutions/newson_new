import styled from "styled-components";
import { tabletBreakpoint, pxToRem } from "../../globalStyles/sharedStyles";

export const MobileByLocationMenu = styled.div`
  display: flex;
  align-items: baseline;
`;

export const GridSection = styled.div`
  padding: ${pxToRem(10)};

  @media (min-width: ${tabletBreakpoint}) {
    padding: 1.6875rem 3.125rem 0 3.125rem;
  }
`;

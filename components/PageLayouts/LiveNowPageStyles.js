import styled from "styled-components";
import {
  tabletBreakpoint,
  pxToRem,
  EmptyResults,
  EmptyResultsTitle,
  EmptyResultsMessage,
} from "../../globalStyles/sharedStyles";

export const LiveNowEmptyIcon = styled.div`
  align-items: center;
  background: ${(props) => props.theme.colors.lighterGrey};
  border-radius: ${pxToRem(400)};
  display: flex;
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(31)};
  line-height: ${pxToRem(42)};
  margin-bottom: ${pxToRem(56)};
  margin-top: ${pxToRem(145)};
  padding: ${pxToRem(8)} ${pxToRem(40)};

  &:before {
    background-color: ${(props) => props.theme.colors.darkerGrey};
    border-radius: 50%;
    content: "";
    display: block;
    height: ${pxToRem(17)};
    margin-right: ${pxToRem(13)};
    width: ${pxToRem(17)};
  }
`;

export const LiveNowEmptySection = styled.div`
  ${EmptyResults};
`;

export const LiveNowEmptyTitle = styled.h3`
  ${EmptyResultsTitle};
  margin-bottom: ${pxToRem(11)};
  margin-top: 0;
`;

export const LiveNowEmptyMessage = styled.p`
  ${EmptyResultsMessage};
  margin: 0;
`;

import styled, { css } from "styled-components";
import {
  laptopBreakpoint,
  desktopBreakpoint,
  tabletBreakpoint,
  largeScreenBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const AboutMain = styled.main`
  display: flex;
  flex-direction: column;
  font-family: Nunito-Sans-Regular;

  .privacy-policy-qr {
    width: ${pxToRem(100)};
    height: ${pxToRem(100)};
    margin: 0;

    @media (min-width: ${desktopBreakpoint}) {
      margin: 0 ${pxToRem(48)};
    }
  }
`;

export const AboutSection = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;

  &.about__top {
    padding-top: 8.75rem;
    padding-bottom: ${pxToRem(37)};
    padding: 8.75rem 2rem 0;

    @media (min-width: ${desktopBreakpoint}) {
      flex-direction: row;
      justify-content: center;
    }

    @media (min-device-pixel-ratio: 1.5), (min-resolution: 120dpi) {
      background-image: url("./images/page-bg@2x.png");
    }
  }

  &.about__bottom {
    width: 100%;
    margin: 0 2rem;
    display: flex;

    @media (min-width: ${tabletBreakpoint}) {
      flex-direction: row;
      justify-content: center;
    }

    @media (min-width: ${desktopBreakpoint}) {
      margin: auto;
    }
  }
`;

export const AboutSectionInner = styled.div`
  width: 100%;

  @media (min-width: ${tabletBreakpoint}) {
    width: 75%;
  }
`;

const subtitleStyle = css`
  font-size: 1.4375rem;
  line-height: 1.875rem;
  font-weight: normal;
`;

export const AboutCallout = styled.h2`
  ${subtitleStyle};
`;

export const AboutColumn = styled.div`
  width: 100%;

  @media (min-width: ${desktopBreakpoint}) {
    width: ${pxToRem(450)};
    &.about__column-left {
      margin-right: ${pxToRem(77)};
    }
  }

  p {
    font-family: Nunito-Sans-Light;
    font-size: 0.875rem;
    line-height: 1.375rem;

    &.faded-content {
      color: ${(props) => props.theme.colors.periwinkle};
    }

    &.larger-copy {
      font-size: 1rem;
    }

    &.contact-info {
      ${subtitleStyle};
      line-height: 2.5rem;
      text-align: left;

      span {
        font-size: 2.25rem;
        line-height: 2.5rem;

        a {
          color: ${(props) => props.theme.colors.white};
          font-size: 2.25rem;
          font-weight: 500;
          margin-top: 0;
        }
      }
    }
  }

  a {
    color: ${(props) => props.theme.colors.pastelBlue};
    display: block;
    font-size: 1.5rem;
    margin-top: 1rem;
    text-decoration: none;

    @media (min-width: ${desktopBreakpoint}) {
      margin-top: 3.125rem;
    }
  }
`;

export const AboutRow = styled.div`
  display: flex;
  flex-direction: column;
  align-items: left;
  margin-bottom: ${pxToRem(30)};

  @media (min-width: ${desktopBreakpoint}) {
    flex-direction: row;
    align-items: center;
    width: ${pxToRem(450)};
  }
`;

export const TopWrapper = styled.div`
  background-image: url("./images/page-bg.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-color: ${(props) => props.theme.colors.lightBlue};
`;

export const BottomWrapper = styled.div`
    width: 100%;
    background-color: ${(props) => props.theme.colors.grey};
    flex: 1;
    display: flex;
    justify-content: center;
    padding-bottom: ${pxToRem(100)};

    @media (min-width: ${tabletBreakpoint}) {
      align-items: center;
    }
`;

export const AboutFooterColumn = styled.div``;

const titleStyle = css`
  font-size: 2.1875rem;
  font-weight: normal;
`;

export const AboutSectionTitle = styled.h1`
  ${titleStyle};
`;

export const AboutSectionSecondaryTitle = styled.h2`
  ${titleStyle};
`;

export const AboutLegalTitle = styled.h4`
  ${subtitleStyle};
`;

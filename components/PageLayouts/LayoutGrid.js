// Components & Helpers
import StationGroup from "../StationGroup";

// Styles
import { GridSection } from "./LayoutGridStyles";

export default function LayoutGrid({
  pageTitle,
  stationGroupItems,
  favorites,
}) {
  return (
    <main>
      <GridSection>
        <StationGroup
          stationGroupTitle={pageTitle}
          stationGroupData={stationGroupItems}
          stationGroupStyle="grid"
          favorites={favorites}
        />
      </GridSection>
    </main>
  );
}

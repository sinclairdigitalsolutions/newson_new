// Components & Helpers
import TextField from "@material-ui/core/TextField";
import { redirectHandler } from "../../lib/routeHelpers";
import ByLocationForm from "../ByLocationForm";
import FavoriteIcon from "../../public/images/icons/favorites-icon.svg";
import UnFavoriteIcon from "../../public/images/icons/favorites-icon-outline.svg";

// Styles
import {
  BannerSection,
  BannerTitle,
  ShareWrapper,
  ShareAndFavoriteWrapper,
} from "./HeaderStyles";
import LiveNowFlag from "../LiveNowFlag/LiveNowFlag";
import { Share } from "react-twitter-widgets";
import { useEffect } from "react";
import LinkButton from "../../components/LinkButton/LinkButton";
import { LinkButtonWrapper } from "../../components/LinkButton/LinkButtonStyles"
import ForwardArrowIcon from "../../public/images/icons/forward-arrow.svg";
import { useRouter } from 'next/router';
import useFavorites from '../../hooks/useFavorites';
import { URL_TEMPLATES } from '../../lib/constants';
import { LinearProgress } from "@material-ui/core";

export default function Header({
  bgImgUrl,
  byLocationData,
  pageTitle,
  pathString,
  size,
  stationIcon,
  subtitle,
  type,
  liveNow,
  pageUrl,
  shareButtons,
  stationId,
  latestVideoId = null,
  breakingNewsEnabled
}) {
  useEffect(() => {
    window.FB?.XFBML.parse();
  }, [shareButtons]);

  const router = useRouter();
  const shareButton = `<div class="fb-share-button" data-href={${pageUrl}} data-layout="button" />`;
  const liveHref = liveNow ? `/live/${stationId}/${latestVideoId}` : `/clips/${stationId}/${latestVideoId}`;
  let favorites, FavoritesButton, isFavorite;
  let showFavoriteButton = false;

  if(router.pathname === URL_TEMPLATES.STATION_DETAILS){
    showFavoriteButton = true;
    [favorites, FavoritesButton] = useFavorites(parseInt(stationId), pageTitle)
    isFavorite = favorites?.includes(parseInt(stationId))
  }


  const watchLiveClickHandler = () => router.push(liveHref);

  return (
    <BannerSection 
    bgImgUrl={bgImgUrl} 
    size={size} 
    type={type} 
    breakingNewsEnabled={breakingNewsEnabled}
    >
      {/* We are using type to detect station here since only station detail pages have images in their headers
        if this changes we may have to add another property to the component */}
      {type === "image" && isFavorite && (
        <FavoriteIcon className="favorite-icon favorite-icon__favorited" />
      )}

      {type === "image" && !isFavorite && (
        <UnFavoriteIcon className="favorite-icon" />
      )}

      {stationIcon && stationIcon}
      {liveNow && <LiveNowFlag stationDetails={true} />}
      <BannerTitle>{pageTitle}</BannerTitle>
      {subtitle && subtitle}

      <ShareAndFavoriteWrapper>
        <LinkButtonWrapper>
          {router.pathname === URL_TEMPLATES.STATION_DETAILS && latestVideoId && 
            <LinkButton
              href={liveHref}
              as={liveHref}
              clickHandler={watchLiveClickHandler}
              icon={<ForwardArrowIcon className="svg" />}
              label= {liveNow ? "Watch Live Now" : "Watch Most Recent"}
              isLiveNow={true}
            />
          }
        </LinkButtonWrapper>
        <LinkButtonWrapper>
          {showFavoriteButton && <FavoritesButton item={parseInt(stationId)}/>}
        </LinkButtonWrapper>
        {(shareButtons && pageUrl) && (
          <ShareWrapper>
            <div
              style={{ marginBottom: "4px", marginRight: "5px" }}
              dangerouslySetInnerHTML={{ __html: shareButton }}
            />
            <Share
              url={pageUrl}
              options={{
                text: pageTitle,
                hashtags: "NewsOn",
              }}
            />
          </ShareWrapper>
        )}
      </ShareAndFavoriteWrapper>
      {pathString === "search" && (
        <TextField
          className="mobile-search-field"
          type="search"
          label="Search"
          placeholder="Search by call letters, City or State"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
          onKeyPress={(e) => {
            if (e.key === "Enter" && e.target.value.length > 1) {
              redirectHandler(e.target.value, "/search");
            }
          }}
        />
      )}

      {pathString === "location" && <ByLocationForm states={byLocationData} />}

      {pathString === "byLocation" && (
        <ByLocationForm states={byLocationData} />
      )}
    </BannerSection>
  );
}

import styled from "styled-components";
import {
  tinyScreenBreakpoint,
  tabletBreakpoint,
  desktopBreakpoint,
  laptopBreakpoint,
  pxToRem,
  BannerSubtitleStyle,
} from "../../globalStyles/sharedStyles";
import theme from "../../globalStyles/theme";
import { paddingTopSizeInPixels } from "../../lib/utils"

const handleBackground = (type, img) => {
  switch (type) {
    case "image":
      return `background-image: url('${img}')`;
    case "generic":
      return `background: transparent linear-gradient(56deg, #19002F 0%, #191048A5 41%, #192F7600 100%);`;
    case "none":
      return `background-image: ""`;
    default:
      return `background-image: ""`;
  }
};

const handleBackgroundColor = (type, theme) => {
  switch (type) {
    case "generic":
      return `background-color: ${theme.colors.turqoise}`;
    case "none":
      return `background-color: ${theme.colors.navy}`;
    default:
      return `background-color: ${theme.colors.navy}`;
  }
};

export const BannerSection = styled.div`
  ${({ type, bgImgUrl }) => handleBackground(type, bgImgUrl)};
  ${({ type, theme }) => handleBackgroundColor(type, theme)};
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  flex-direction: column;
  padding: 0 ${pxToRem(15)} ${pxToRem(15)} ${pxToRem(15)};
  padding-top: ${({breakingNewsEnabled}) => pxToRem( paddingTopSizeInPixels(breakingNewsEnabled) + 50)};
  position: relative;
  user-select: none;

  &:before {
    background-image: url("../images/image-header-overlay.png");
    background-position: 50% 100%;
    background-repeat: no-repeat;
    background-size: 100% auto;
    content: "";
    display: ${({ type }) => (type === "image" ? "block" : "none")};
    height: 100%;
    left: 0;
    position: absolute;
    right: 0;
    width: 100%;
    z-index: 1;
    top: 0;
    bottom: 0;
    @media (min-device-pixel-ratio: 1.5), (min-resolution: 120dpi) {
      background-image: url("../images/image-header-overlay@2x.png");
    }
  }

  .station-icon {
    display: none;
    max-height: ${pxToRem(100)};
    position: absolute;
    z-index: 5;
  }

  @media (min-device-pixel-ratio: 1.5), (min-resolution: 120dpi) {
    ${(props) =>
      props.bgType === "large"
        ? `background-image: "/images/page-header-bg@2x.png"`
        : `background-image: ""`};
  }

  @media (min-width: ${tabletBreakpoint}) {
    padding: ${({ size }) =>
      size === "large"
        ? `0 ${pxToRem(75)} ${pxToRem(55)} ${pxToRem(105)}`
        : `0 0 ${pxToRem(40)} ${pxToRem(65)}`};
    padding-top: ${({breakingNewsEnabled}) => pxToRem( paddingTopSizeInPixels(breakingNewsEnabled) + 50)};
  }

  @media (min-width: ${laptopBreakpoint}){
    .station-icon {
      bottom: ${pxToRem(55)};
      display: block;
      right: ${pxToRem(70)};
    }
  }
  @media (min-width: ${desktopBreakpoint}) {
    .station-icon {
      bottom: ${pxToRem(55)};
      right: ${pxToRem(170)};
      max-height: ${pxToRem(100)};
    }
  }

  .MuiOutlinedInput-root {
    background: ${(props) => props.theme.colors.white};

    &:before {
      background-image: url("images/icons/search-icon-blue.svg");
      background-repeat: no-repeat;
      background-size: cover;
      content: "";
      display: block;
      height: ${pxToRem(18)};
      margin-left: ${pxToRem(15)};
      width: ${pxToRem(22)};
    }
  }

  .MuiOutlinedInput-input {
    padding: ${pxToRem(9)} ${pxToRem(12)} ${pxToRem(9)} ${pxToRem(10)};
    position: relative;
  }

  legend,
  fieldset,
  label {
    transform: translate(${pxToRem(40)}, ${pxToRem(26)}) scale(1);
    z-index: 1;

    @media (min-width: ${laptopBreakpoint}) {
      display: none;
    }
  }

  .mobile-search-field {
    max-width: ${pxToRem(500)};
    legend,
    fieldset,
    label {
      display: none;
      transform: translate(0, 0) scale(1);
    }

    @media (min-width: ${laptopBreakpoint}) {
      display: none;
    }
  }

  .mui-form-control {
    @media (min-width: ${laptopBreakpoint}) {
      display: none;
    }
  }

  .MuiInput-input {
    background: ${theme.colors.white};
    color: ${theme.colors.partialOpaqueNavy};
    font-size: ${pxToRem(21)};
    font-family: "Nunito-Sans-Regular";
    border-radius: 3px;

    :focus {
      background: white;
      color: ${theme.colors.partialOpaqueNavy};
    }
  }

  .mui-by-location{
    max-width: ${pxToRem(500)};
  }

  .Mui-selected {
    opacity: 0.6;
  }

  .MuiSelect-icon {
    transform: scale(1.5);
    fill: ${theme.colors.navy};
  }

  .MuiSelect-selectMenu {
    display: flex;
    align-items: center;

    &:before {
      background-image: url("images/icons/bylocation-icon-blue.svg");
      background-repeat: no-repeat;
      background-size: ${pxToRem(14)};
      content: "";
      display: block;
      height: ${pxToRem(20.8)};
      margin-left: ${pxToRem(14)};
      width: ${pxToRem(16)};
      padding-right: ${pxToRem(10)};
    }
  }

  .favorite-icon {
    align-self: flex-end;
    height: ${pxToRem(25)};
    width: ${pxToRem(35)};

    &__favorited {
      fill: red;
    }
  }

  h3 {
    ${BannerSubtitleStyle};
  }
`;

export const BannerTitle = styled.div`
  font-family: Nunito-Sans-Regular;
  font-size: 2.125rem;
  font-weight: normal;
  margin-top: auto;
  margin-bottom: ${pxToRem(30)};
  position: relative;
  z-index: 5;
  user-select: none;

  .strong {
    display: inline-block;
    font-family: Nunito-Sans-Bold;
  }

  .mobile {
    display: block;

    @media (min-width: ${tabletBreakpoint}) {
      display: none;
    }
  }

  .desktop-only {
    display: none;

    @media (min-width: ${tabletBreakpoint}) {
      display: inline;
    }
  }

  @media (min-width: ${tabletBreakpoint}) {
    font-family: Nunito-Sans-ExtraLight;
    font-size: 2.875rem;
    margin-bottom: 0;
  }
`;

export const ShareWrapper = styled.div`
  display: flex;
  
  @media (min-width: ${tabletBreakpoint}){
    margin-left: ${pxToRem(15)};
  }
`;

export const ShareAndFavoriteWrapper = styled.div`
  z-index: 10;
  display: flex;
  flex-direction: column;

  @media (min-width: ${tinyScreenBreakpoint}){
    flex-direction: row;
    align-items: flex-end;
    justify-content: left;
  }
`;

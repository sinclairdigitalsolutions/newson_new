import React from "react";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  AdditionalInfo,
  InfoPanel,
  CardTitle,
  CardSubtitle,
  CardDistance,
  StationIcon,
  IconContainer
} from "./CardStyles";
import { LiveBadge, FavoriteBadge, tabletBreakpoint } from "../../globalStyles/sharedStyles";
import { CarouselCard } from "../StationGroupStyles";
import FavoriteIcon from "../../public/images/icons/favorites-icon.svg";
import FavoriteIconOutline from "../../public/images/icons/favorites-icon-outline.svg";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  STATION_GROUP_TITLES,
  URL_TEMPLATES,
  BANNER_IMAGE_ROOT_URL,
} from "../../lib/constants";

const Card = ({
  item,
  stationGroupStyle,
  favoriteBadgeVisibility,
  cardType,
}) => {
  const isMobile = useMediaQuery('(max-width: ' + tabletBreakpoint + ')');
  const router = useRouter();
  let bgImage = "/images/card-default-bg.png";
  let streamUrl = "/";

  const {
    configValue = {},
    distance,
    latest,
    live = false,
    name,
    icon,
    portraitImage,
    id,
    hideIcon = false,
  } = item;

  const bannerUrl = `${BANNER_IMAGE_ROOT_URL}/channel_background_${id}.jpg`;

  // Returns the proper url template for routing with refreshing
  // Returning "null" causes a page reload
  const urlTemplate = (() => {
    if(
      cardType === STATION_GROUP_TITLES.LIVE_NOW || 
      cardType === STATION_GROUP_TITLES.LIVE_NOW || 
      cardType === STATION_GROUP_TITLES.LIVE_STATIONS
    ){
      return null
    }

    if(cardType?.includes(STATION_GROUP_TITLES.CLIPS)){
      return null
    }

    if(cardType === STATION_GROUP_TITLES.BREAKING_NEWS){
      return URL_TEMPLATES.BREAKING_NEWS
    }

    if(
      cardType === STATION_GROUP_TITLES.LOCAL_STATIONS ||
      router.pathname === URL_TEMPLATES.SEARCH ||
      router.pathname === URL_TEMPLATES.FAVORITES ||
      router.pathname === URL_TEMPLATES.BY_LOCATION
    ){
      return URL_TEMPLATES.STATION_DETAILS
    }
    return null
  })()

  const showStationIcon = (() => {
    if(
      cardType === STATION_GROUP_TITLES.LOCAL_STATIONS ||
      router.pathname === URL_TEMPLATES.FAVORITES ||
      router.pathname === URL_TEMPLATES.SEARCH ||
      router.pathname === URL_TEMPLATES.BY_LOCATION
    ){
      return true
    }
    return false
  })()

  const returnLiveThumbImage = (cardType) =>
    cardType === STATION_GROUP_TITLES.LIVE && live === true
      ? bannerUrl
      : portraitImage;

  const channelId = Array.isArray(router.query.id)
    ? router.query.id[0]
    : router.query.id;

  if (
    (latest && cardType === STATION_GROUP_TITLES.LIVE) ||
    cardType === STATION_GROUP_TITLES.LIVE_NOW ||
    cardType === STATION_GROUP_TITLES.LIVE_STATIONS ||
    cardType === STATION_GROUP_TITLES.BREAKING_NEWS
  ) {
    bgImage = latest.thumbnailUrl;

    if (latest.streamUrl) {
      streamUrl = `/live/${latest.channelId}/${latest.id}`;

      if (!latest.live) {
        const assetId = id === latest.channelId ? latest.id : id;
        streamUrl = `/clips/${latest.channelId}/${assetId}`;
      }
    }
  }
  
  if (typeof cardType === "string" && cardType.includes(STATION_GROUP_TITLES.CLIPS)) {
    // For routes with 2 Id's we need to grab the first channel
    const channelId = Array.isArray(router.query.id)
      ? router.query.id[0]
      : router.query.id;

    streamUrl = `/clips/${channelId}/${item.id}`;
  }

  return (
    <Link
      // href={streamUrl !== "/" ? urlTemplate : "/stationDetails/[id]"}
      // as={streamUrl !== "/" ? streamUrl : `/stationDetails/${id}`}
      href={urlTemplate || streamUrl}
      as={streamUrl !== "/" ? streamUrl : `/stationDetails/${id}`}
    >
      <a>
        <CarouselCard
          stationGroupStyle={stationGroupStyle}
          bgImage={returnLiveThumbImage(cardType) || bgImage}
          bgFallBackImg={bannerUrl}
        >
          {/* TODO Confirm favorite badge and live badge business logic/priorities */}
          
          <StationIcon 
            src={icon} 
            showStationIcon={showStationIcon}
            stationGroupStyle={stationGroupStyle}
            cardType={cardType}
          />
          <InfoPanel
            className="info-panel"
            stationGroupStyle={stationGroupStyle}
            latest={latest || portraitImage}
          >
            <CardTitle stationGroupStyle={stationGroupStyle}>{name}</CardTitle>
            <AdditionalInfo stationGroupStyle={stationGroupStyle}>
              <CardSubtitle>{configValue?.affiliation}</CardSubtitle>
              {cardType !== STATION_GROUP_TITLES.CLIPS &&
                cardType !== STATION_GROUP_TITLES.LIVE &&
                distance && (
                  <CardDistance stationGroupStyle={stationGroupStyle}>
                    {distance} miles
                  </CardDistance>
                )}
            </AdditionalInfo>
          </InfoPanel>
          <IconContainer stationGroupStyle={stationGroupStyle}>
            {(live || cardType === STATION_GROUP_TITLES.BREAKING_NEWS) && (
              <LiveBadge
                isFavorite={favoriteBadgeVisibility}
                stationGroupStyle={stationGroupStyle}
                className="live-badge"
              >
                Live
              </LiveBadge>
            )}
            <FavoriteBadge
              isLive={live || cardType === STATION_GROUP_TITLES.BREAKING_NEWS}
              className="favorite-badge"
              stationGroupStyle={stationGroupStyle}
            >
              {favoriteBadgeVisibility ? 
                <FavoriteIcon className="svg" /> : 
                (isMobile && stationGroupStyle !== "carousel") &&
                  <FavoriteIconOutline className="svg"/>
              }
            </FavoriteBadge>
          </IconContainer>
        </CarouselCard>
      </a>
    </Link>
  );
};

export default Card;

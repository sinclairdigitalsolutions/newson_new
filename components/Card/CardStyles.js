import styled from "styled-components";
import { tinyScreenBreakpoint, tabletBreakpoint, pxToRem } from "../../globalStyles/sharedStyles";
import { STATION_GROUP_TITLES } from '../../lib/constants';
import { CarouselCard } from "../StationGroupStyles";

export const InfoPanel = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  max-width: ${props => props.stationGroupStyle === "carousel" ? "" : pxToRem(130)};
  overflow: hidden;
  height: ${(props) =>
    props.stationGroupStyle === "carousel" ? null : "auto" };
  margin-top: ${(props) =>
    props.stationGroupStyle === "carousel" ? "auto" : "0" };
  ${(props) =>
    props.stationGroupStyle === "carousel"
      ? {backgroundColor: 
          props.latest
            ? props.theme.colors.semiOpaqueBlack
            : props.theme.colors.whiteOpaque
        }
      : {
          background:
            "none",
        }};
  margin-left: ${props => props.stationGroupStyle === "carousel" ? "" : pxToRem(20)};
  padding: ${pxToRem(6)};
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(17)};
  align-items: start;
  user-select: none;

  @media (min-width: ${tinyScreenBreakpoint}){
    max-width: ${props => props.stationGroupStyle === "carousel" ? "" : pxToRem(250)};
  }
  
  @media (min-width: ${tabletBreakpoint}){
    z-index: auto;
    max-width: 100%;
    align-items: start;
    height: ${pxToRem(44)};
    padding-left: ${pxToRem(18)};
    margin-left: 0;
    margin-top: auto;
    background: none;
    background-color: ${(props) =>
      props.latest
        ? props.theme.colors.semiOpaqueBlack
        : props.theme.colors.whiteOpaque};
  }

`;

export const CardTitle = styled.h3`
  margin: 0;
  ${(props) =>
    props.stationGroupStyle === "carousel"
      ? null
      : { maxWidth: pxToRem(200) }};

  color: ${(props) => props.theme.colors.white};
  font-family: Nunito-Sans-SemiBold;
  font-size: ${pxToRem(13)};
  line-height: ${pxToRem(18)};
  overflow: hidden;
  -webkit-line-clamp: 2;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  
  @media( min-width: ${tabletBreakpoint}){
    max-width: none;
    margin: 0;
  }
`;

export const CardSubtitle = styled.h4`
  color: ${(props) => props.theme.colors.lightGrey};
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(12)};
  line-height: ${pxToRem(16)};
  margin: 0;
`;

export const CardDistance = styled.p`
  color: ${(props) => props.theme.colors.lightGrey};
  margin: 0;
  align-self: center;
  font-family: Nunito-Sans-SemiBold;
  font-size: ${pxToRem(13)};
  line-height: ${pxToRem(16)};
  opacity: 0.64;

  right: ${pxToRem(19)};
  bottom: ${pxToRem(11)};

  ${(props) =>
    props.stationGroupStyle === "carousel" ? { position: "absolute" } : null};
  ${(props) =>
    props.stationGroupStyle === "carousel"
      ? null
      : { marginLeft: `${pxToRem(16)}` }};

  @media (min-width: ${tabletBreakpoint}) {
    position: absolute;
    line-height: ${pxToRem(18)};
  }
`;

export const StationIcon = styled.img`
  max-height: ${pxToRem(50)};
  max-width: ${pxToRem(43)};
  margin-left: ${pxToRem(10)};
  display: ${props => props.showStationIcon ? "block" : "none"};


  ${props => {
    return props.stationGroupStyle === "carousel"
    ? `
      margin: auto; 
      position: absolute; 
      top: 0; 
      left: 0; 
      bottom 0; 
      right: 0; 
      padding-bottom: 50px;
    `
    : ""
  }}

  @media (min-width: ${tabletBreakpoint}) {
    margin: auto; 
    position: absolute; 
    top: 0; 
    left: 0; 
    bottom 0; 
    right: 0; 
    padding-bottom: 50px;
    max-height: ${pxToRem(160)};
    max-width: ${pxToRem(150)};
  }
`;

export const AdditionalInfo = styled.div`
  display: flex;
  justify-content: flex-start;

  @media (min-width: ${tabletBreakpoint}) {
    margin: 0;
    justify-content: space-between;

  }
`;

export const IconContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: 100%;
  margin-right: ${props => props.stationGroupStyle === "carousel" ? "" : pxToRem(10)};

  @media (min-width: ${tinyScreenBreakpoint}){
    flex-direction: row;
  }

  @media (min-width: ${tabletBreakpoint}){
    margin-right: 0;
  }
`
import styled from "styled-components";
import {
  ButtonStyle,
  tabletBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const BreakingNewsWrapper = styled.div`
  background-color: ${(props) => props.theme.colors.red};
  color: ${(props) => props.theme.colors.white};
  padding: 0 ${pxToRem(10)} 0 0;
  position: fixed;
  width: 100%;
  z-index: 9999;
  height: ${pxToRem(60)};
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (min-width: ${tabletBreakpoint}) {
    padding: 0;
  }
`;

export const BreakingNewsContent = styled.div`
  display: flex;
  font-family: Nunito-Sans-Regular;
  font-size: 0.8125rem;
  align-items: center;
  margin: 0;
  width: 100%;
  padding: ${pxToRem(10)} ${pxToRem(15)};
  overflow: hidden;
  user-select: none;

  @media (min-width: ${tabletBreakpoint}) {
    font-size: 0.9375rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    margin: 0 ${pxToRem(109)};
  }

  p {
    margin: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    @media (min-width: ${tabletBreakpoint}) {
      max-width: 100%;
      overflow: visible;
      white-space: normal;
      text-overflow: unset;
    }
  }
`;

export const BreakingNewsTitle = styled.div`
  margin-right: 0.625rem;
  font-family: Nunito-Sans-Bold;
  user-select: none;

  @media (min-width: ${tabletBreakpoint}) {
    margin-right: 1.875rem;
  }

  span {
    display: none;

    @media (min-width: ${tabletBreakpoint}) {
      display: inline;
    }
  }
`;

export const BreakingNewsButton = styled.a`
  ${ButtonStyle};
  color: ${(props) => props.theme.colors.darkRed};
  cursor: pointer;
  margin-right: ${pxToRem(25)};
  padding-bottom: 0.1875rem;
  padding-top: 0.1875rem;
  text-align: center;
  height: 1.875rem;

  @media (min-width: ${tabletBreakpoint}) {
    padding: 0.1875rem 0.9375rem;
    font-size: 0.6875rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    padding: 0.1875rem 0.9375rem!important;
  }
`;

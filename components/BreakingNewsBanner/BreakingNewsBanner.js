import React from "react";
import Link from "next/link";
import {
  BreakingNewsWrapper,
  BreakingNewsContent,
  BreakingNewsTitle,
  BreakingNewsButton,
} from "./BreakingNewsBannerStyles";
import { truncateString } from "../../lib/utils";

const BreakingNewsBanner = ({ storyTitle, storyLink }) => {
  return (
    <BreakingNewsWrapper>
      <BreakingNewsContent>
        <Link href={storyLink}>
          <BreakingNewsButton>Watch Coverage</BreakingNewsButton>
        </Link>
        <BreakingNewsTitle>
          Breaking <span>News</span>
        </BreakingNewsTitle>
        <p>{truncateString(storyTitle, 115)}</p>
      </BreakingNewsContent>
    </BreakingNewsWrapper>
  );
};

export default BreakingNewsBanner;

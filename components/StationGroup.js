import React from "react";
import {
  Content,
  Container,
  TitleDesktop,
  TitleMobile,
} from "./StationGroupStyles";
import StationGroupCarousel from "./StationGroupCarousel/StationGroupCarousel";
import Card from "./Card/Card";
import { useRouter } from "next/router";
import { URL_TEMPLATES } from "../lib/constants";

export const StationGroup = ({
  stationGroupData,
  stationGroupStyle,
  stationGroupTitle,
  favorites,
}) => {
  const router = useRouter();
  return (
    <Container stationGroupStyle={stationGroupStyle}>
      {stationGroupStyle === "carousel" || router.pathname === URL_TEMPLATES.BY_LOCATION ? (
        <TitleDesktop
          stationGroupStyle={stationGroupStyle}
          className={stationGroupTitle?.split(" ").join("")}
        >
          {stationGroupTitle}
        </TitleDesktop>
      ) : null}

      {stationGroupTitle && 
      <TitleMobile stationGroupStyle={stationGroupStyle}>
        {stationGroupTitle}
      </TitleMobile>
      }

      {stationGroupStyle === "carousel" ? (
        <StationGroupCarousel>
          {stationGroupData.map((item, index) => {
            return (
              <Card
                cardType={stationGroupTitle}
                key={index}
                stationGroupStyle={stationGroupStyle}
                item={item}
                favoriteBadgeVisibility={
                  favorites && favorites.includes(item.id)
                }
              ></Card>
            );
          })}
        </StationGroupCarousel>
      ) : (
        <Content stationGroupStyle={stationGroupStyle}>
          {stationGroupData.map((item, index) => {
            return (
              <Card
                key={index}
                stationGroupStyle={stationGroupStyle}
                cardType={stationGroupTitle}
                item={item}
                favoriteBadgeVisibility={
                  favorites && favorites.includes(item.id)
                }
              ></Card>
            );
          })}
        </Content>
      )}
    </Container>
  );
};

export default StationGroup;

export function OpenGraphMeta({url, title, description, thumbnail}) {
  return [
      <meta name="twitter:card" content="summary" />,
      <meta name="twitter:site" content="@newson" />,
      <meta property="og:url" key={url} content={url} />,
      <meta property="og:type" content="website" />,
      <meta property="og:title" key={title} content={title} />,
      <meta property="og:description" key={description} content={description} />,
      <meta property="og:image" key={thumbnail} content={thumbnail} />,
  ];
}

export default OpenGraphMeta;
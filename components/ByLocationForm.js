import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import ByLocationIcon from "../public/images/icons/bylocation-icon.svg";
import FormControl from "@material-ui/core/FormControl";
import { redirectHandler } from "../lib/routeHelpers";
import { MobileByLocationMenu } from "../components/PageLayouts/LayoutGridStyles";
import useStatesFetch from "../hooks/useStatesFetch";

export const ByLocationForm = () => {
  const { states, isLoading, isError } = useStatesFetch();
  const [state, setState] = useState("By Location");
  const [locationLabel, setLocationLabel] = useState("By Location");
  const { pathname, query } = useRouter();

  /**
   * Called every time the 'By Location' dropdown is changed and updates the
   * value of the dropdown and fires an app redirect
   *
   * @param { Object } event the event object recived from the onChange event
   */
  const handleStateChange = (event) => {
    setState(event.target.value);
    setLocationLabel("");
    redirectHandler(event.target.value, "/byLocation");
  };

  useEffect(() => {
    if (pathname !== "/byLocation") {
      setLocationLabel("By Location");
    } else {
      setLocationLabel(query.term ? "" : "Stations near me...");
    }
  }, [pathname]);

  if (isLoading) return <div>Loading...</div>;

  return (
    <FormControl className="mui-form-control mui-by-location">
      <InputLabel>{locationLabel}</InputLabel>

      <Select
        MenuProps={{
          PaperProps: {
            square: true,
          },
        }}
        style={{zIndex: "10"}}
        value={pathname === "/byLocation" && query.term ? query.term : ""}
        onChange={handleStateChange}
      >
        <MenuItem className="location-first-item" value="" key={"by-location"}>
          <MobileByLocationMenu>
            <ByLocationIcon className="location-menu-icon" />
            Choose a Location...
          </MobileByLocationMenu>
        </MenuItem>
        {states &&
          states.map((state, i) => {
            return (
              <MenuItem
                value={state.name}
                key={i}
                style={{ width: 250, paddingLeft: 30 }}
              >
                {state.name}
              </MenuItem>
            );
          })}
      </Select>
    </FormControl>
  );
};

export default ByLocationForm;

import styled from "styled-components";
import {
  tabletBreakpoint,
  laptopBreakpoint,
  desktopBreakpoint,
  largeScreenBreakpoint,
  ginormousScreenBreakpoint,
  pxToRem,
} from "../globalStyles/sharedStyles";
import { STATION_GROUP_TITLES } from "../lib/constants";

const breakingNewsClass = STATION_GROUP_TITLES.BREAKING_NEWS.split(" ").join(
  ""
);

export const Container = styled.div`
    margin-bottom: ${pxToRem(30)};
`;

export const TitleDesktop = styled.div`
  display: none;
  user-select: none;

  @media (min-width: ${tabletBreakpoint}) {
    display: block;
    height: ${pxToRem(30)};
    font-family: Nunito-Sans-Regular;
    font-size: ${pxToRem(22)};
    margin-bottom: ${pxToRem(5)};
  }
`;

export const TitleMobile = styled.div`

  display: block;
  height: ${pxToRem(30)};
  font-family: Nunito-Sans-Regular;
  font-size: ${pxToRem(14)};
  margin-left: ${(props) =>
    props.stationGroupStyle === "carousel" ? pxToRem(19) : pxToRem(20)};
  margin-bottom: ${(props) =>
    props.stationGroupStyle === "carousel" ? null : pxToRem(0)};
  padding-top: ${pxToRem(13)};

  span > div {
    display: inline;
  }

  .mobile {
    display: none;
  }
  @media (min-width: ${tabletBreakpoint}){
    display: none;
  }
`;

export const Content = styled.div`
  height: auto;
  display: ${(props) => (props.carouselStyle === "carousel" ? "flex" : "grid")};
  grid-template-columns: 1fr;
  width: ${(props) => (props.carouselStyle === "carousel" ? "auto" : "100%")};
  flex-flow: ${(props) =>
    props.stationGroupStyle === "carousel" ? "none" : "wrap"};
  justify-content: center;
  overflow: ${(props) =>
    props.carouselStyle === "row" ? "scroll" : "visible"};

  @media (min-width: ${tabletBreakpoint}){
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: ${laptopBreakpoint}) {
    display: ${(props) =>
      props.carouselStyle === "carousel" ? "flex" : "grid"};
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media (min-width: ${desktopBreakpoint}) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }

  @media (min-width: ${largeScreenBreakpoint}) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  }

  @media (min-width: ${ginormousScreenBreakpoint}) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  }

`;

export const CarouselCard = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: ${(props) =>
    props.stationGroupStyle === "carousel" ? pxToRem(150) : pxToRem(65)};
  background: ${(props) =>
    props.stationGroupStyle === "carousel" ?  `url(${props.bgImage}), url(${props.bgFallBackImg})` : "transparent linear-gradient(180deg, #414040 0%, #2e2e2e 100%)" };
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  margin-right: ${(props) =>
    props.stationGroupStyle === "carousel" ? null : "0"};
  margin-bottom: ${(props) =>
    props.stationGroupStyle === "carousel" ? "0" : pxToRem(5)};

  min-width: ${(props) =>
    props.stationGroupStyle === "carousel" ? pxToRem(290) : 0};
  max-width: ${(props) =>
    props.stationGroupStyle === "carousel" ? pxToRem(290) : "unset"};

  @media (min-width: ${tabletBreakpoint}){

    background: url(${props => props.bgImage}), url(${props => props.bgFallBackImg});
    background-size: cover;
    background-position: center;
    height: ${(props) =>
      props.stationGroupStyle === "carousel" ? pxToRem(248) : pxToRem(248)};
    margin-right: ${(props) =>
      props.stationGroupStyle === "carousel" ? pxToRem(15) : pxToRem(13)};
    margin-bottom: ${(props) =>
      props.stationGroupStyle === "carousel" ? "0" : pxToRem(20)};
    
    min-width: 0;
    max-width: none;
  }

  @media (min-width: ${desktopBreakpoint}) {
    &:before {
      border-bottom: 0;
      border: solid ${pxToRem(2)} transparent;
      content: "";
      display: block;
      height: 100%;
      pointer-events: none;
      position: absolute;
      width: 99%;
      z-index: 200;
    }

    .info-panel {
      border-bottom: solid ${pxToRem(2)} transparent;
    }
    :hover {
      border-color: ${(props) => props.theme.colors.white};
      border-bottom: 0;

      &:before {
        border-color: ${(props) => props.theme.colors.white};
      }

      .info-panel {
        border-bottom-color: ${(props) => props.theme.colors.white};
      }
    }
  }
`;

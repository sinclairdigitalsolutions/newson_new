import styled from "styled-components";
import {
  tabletBreakpoint,
  desktopBreakpoint,
  pxToRem,
  laptopBreakpoint,
} from "../../../globalStyles/sharedStyles";

export const PlayerControllerWrapper = styled.div`
  width: 100%;

  @media (min-width: ${laptopBreakpoint}) {
    width: ${pxToRem(680)};
  }
`;

export const PlayerWrapper = styled.div`
  position: relative;
  padding-bottom: 56.25%; /* 16:9 aspect ratio */
  visibility: ${(props) => props.inAd ? "hidden" : "visible"}!important;

  #amp {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .amp-share {
    display: none;
  }
`;

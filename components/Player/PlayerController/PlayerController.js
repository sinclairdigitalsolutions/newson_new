import React, { useEffect, useRef, useState } from "react";
import Player from "../Player/Player";
import {
  PlayerControllerWrapper,
  PlayerWrapper,
} from "./PlayerControllerStyles";
import ErrorBoundary from "../../ErrorBoundry/ErrorBoundary";
import PlayerErrorSlate from "../PlayerErrorSlate/PlayerErrorSlate";
import { ERROR_TYPES, ANALYTICS_VALUES } from "../../../lib/constants";
import NProgress from "nprogress";
import useAdManager from "../../../hooks/useAdManager";
import {
  analyticsVideoErrorEvent,
  analyticsVideoInitiatePlay,
  analyticsVideoPlay,
  analyticsVideoPause,
  analyticsVideoStop,
  analyticsVideoResume,
  analyticsVideoProgress,
} from "../../../lib/analytics";
import { setupAnalyticsDimensions } from "../../../lib/utils";

const PlayerController = ({
  clip,
  live,
  videoExpired,
  channel,
  vodAds,
  adHolidays,
}) => {
  const playerRef = useRef(null);
  const vodAdState = useRef(vodAds);
  const [streamUrl, setStreamUrl] = useState(null);
  const [asset, setAsset] = useState(null);
  const [errorType, setErrorType] = useState(null);

  const videoCompletion = useRef({
    firstQuartile: false,
    midpoint: false,
    thirdQuartile: false,
    finished: false,
  });

  const nextAdMarker = useRef(null);
  const inAdRef = useRef(null);
  const adReadyRef = useRef(null);
  const {
    configValue: {
      programstartprerollurl_mobile,
      midrollurl_mobile,
      programstartprerollurl_mobile_live,
      midrollurl_mobile_live,
    },
  } = channel;
  const preRollUrl = live ? programstartprerollurl_mobile_live : programstartprerollurl_mobile;
  const midRollUrl = live ? midrollurl_mobile_live : midrollurl_mobile;
  // TODO: remove test urls when ads are solid
  // const testMidrollUrl =
  //   "https://pubads.g.doubleclick.net/gampad/ads?" +
  //   "sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&" +
  //   "impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&" +
  //   "cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";
  // const testPrerollUrl =
  //   "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480" +
  //   "&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250" +
  //   "&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1" +
  //   "&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&cmsid=496" +
  //   "&vid=short_onecue&correlator=";
  const [adHoliday, setAdHoliday] = useState(adHolidays?.preroll);
  const vodAdsExist = vodAds && vodAds[0];
  const inAdHolidayRef = useRef(false);
  const stationName = channel.description;
  const videoTitle = Boolean(live) ? live.name : clip?.name;
  // Collect analytics data
  const { EVENTS } = ANALYTICS_VALUES;
  const analyticsDimensions = setupAnalyticsDimensions(
    stationName,
    channel,
    live,
    clip
  );

  const [
    initializeIMA,
    loadAds,
    AdContainer,
    inAd,
    adReady,
    inAdHoliday,
  ] = useAdManager(inAdHolidayRef, analyticsDimensions);

  useEffect(() => {
    if (vodAdsExist) {
      nextAdMarker.current = {
        start: vodAdState.current[0].start,
        end: vodAdState.current[0].end,
        index: 0,
        played: false,
      };
    }

    return () => {
      // component unmount
      cleanupPlayer();
    };
  }, []);

  useEffect(() => {
    if (clip) {
      setStreamUrl(clip.streamUrl);
      setAsset(clip);
    }

    if (live) {
      setStreamUrl(live.streamUrl);
      setAsset(live);
    }

    if (videoExpired) {
      NProgress.done();
      setErrorType(ERROR_TYPES.VIDEO_EXPIRED);
    }
  }, [clip, live, videoExpired]);

  useEffect(() => {
    inAdRef.current = inAd;
    adReadyRef.current = adReady;
  }, [inAd, adReady]);

  useEffect(() => {
    createAndInitializePlayer();
  }, [streamUrl]);

  useEffect(() => {
    setPlayStateForAdPlayback();
  }, [inAd]);

  useEffect(() => {
    inAdHolidayRef.current = inAdHoliday;
  }, [inAdHoliday]);

  /**
   * Void Function used to set the VOD play state when we enter or exit an ad
   */
  const setPlayStateForAdPlayback = () => {
    if (playerRef?.current) {
      if (!inAdRef.current) {
        playerRef.current.play();
      }

      if (inAdRef.current) {
        playerRef.current.pause();
      }
    }
  };

  /**
   * Void function used to kick off the player initialization
   * and trigger the call to set up events
   */
  const createAndInitializePlayer = () => {
    console.debug("[createAndInitializePlayer]");
    const AMP = window.akamai?.amp?.AMP;

    const poster = clip?.thumbnailUrl ?? live?.thumbnailUrl;

    if (AMP && streamUrl) {
      const config = {
        media: {
          src: streamUrl,
          autoplay: false,
          poster: poster ?? "/images/card-default-bg@2x.png",
          title: "",
          track: [
            {
              kind: "subtitles",
              type: "text/cea-608",
              srclang: "en",
            },
          ],
        },
      };

      AMP.create("amp", config, (e) => initializePlayerEvents(e));
      return;
    }

    if (!AMP) {
      setErrorType(ERROR_TYPES.AMP_ERROR);
    }
  };

  /**
   * Void Function used to set the next ad marker in the array
   */
  const setNextVodAdMarker = (timeInSeconds) => {
    vodAdState.current.forEach((vodAd, idx) => {
      if (nextAdMarker.current.start >= vodAd.start) {
        return;
      }

      if (timeInSeconds >= nextAdMarker.current.start) {
        console.debug("[Setting next ad marker]", vodAd);
        if (vodAd.start > nextAdMarker.current.start) {
          nextAdMarker.current = {
            start: vodAd.start,
            end: vodAd.end,
            index: idx,
            played: false,
          };
        }

        return;
      }
    });
  };

  /**
   * Function used to check the current VOD ad window
   * to check if we need to start an ad
   * takes a player event object as a param
   * @param  {object} event
   */
  const checkVodAdWindow = (event) => {
    const timeInSeconds = event.data;
    const inAdWindow =
      timeInSeconds >= nextAdMarker.current.start &&
      timeInSeconds < nextAdMarker.current.end;
    const { played } = vodAdState.current[nextAdMarker.current.index];
    const shouldTerminateAd = inAdWindow && !inAdRef.current && played;

    if (inAdWindow && !inAdRef.current && !played) {
      if (!adReadyRef.current) {
        console.debug("[Requesting new ad]");
        inAdRef.current = true;
        initializeIMA(midRollUrl);
      }

      if (adReadyRef.current && !inAdHoliday) {
        console.debug("[Loading Ads]");
        inAdRef.current = true;
        nextAdMarker.current.played = true;

        const newAdState = vodAdState.current.map((vodAd, index) => {
          if (index === nextAdMarker.current.index) {
            return nextAdMarker.current;
          }

          return vodAd;
        });

        vodAdState.current = newAdState;
        loadAds();
      }
    }

    if (shouldTerminateAd) {
      console.debug("[Terminating Ad]");
      inAdRef.current = false;
      // TODO: not sure about this - could cause some weirdness.
      playerRef.current.seek(nextAdMarker.current.end);
      setNextVodAdMarker(timeInSeconds);
    }
  };

  const checkIfSeekedPassedAdMarker = (event) => {
    const currentPlayHead = event.data;
    const inAdWindow =
      currentPlayHead >= nextAdMarker.current.start &&
      currentPlayHead < nextAdMarker.current.end;

    if (
      nextAdMarker.current.end < playerRef.current.currentTime &&
      !inAdWindow
    ) {
      setNextVodAdMarker(currentPlayHead);
    }
  };

  /**
   * Function used to add event listeners to the player
   * takes the player event object as a param
   * @param  {object} event
   */
  const initializePlayerEvents = (event) => {
    playerRef.current = event.player;
    const player = playerRef.current;
    NProgress.done();

    player.addEventListener(akamai.amp.Events.PLAY_REQUEST, (event) => {
      if (vodAdsExist && !inAdRef.current && adReadyRef.current) {
        loadAds(true);
      }
      analyticsVideoInitiatePlay(videoTitle, analyticsDimensions);
    });

    player.addEventListener(akamai.amp.Events.PLAY_STATE_CHANGE, (event) => {
      if (event?.data.value === "playing" && inAdRef.current) {
        event.player.pause();
      }
    });

    player.addEventListener(akamai.amp.Events.ERROR, (event) => {
      console.error("AMP Error", amp.error, event);
      analyticsVideoErrorEvent(event?.detail?.message, analyticsDimensions);

      NProgress.done();
      setErrorType(ERROR_TYPES.AMP_ERROR);
    });

    player.addEventListener(akamai.amp.Events.TIME_UPDATE, (event) => {
      if (vodAdsExist) {
        checkVodAdWindow(event);
      }

      let videoPercentageComplete = player.currentTime / player.duration;

      if (
        videoPercentageComplete >= 0.25 &&
        !videoCompletion.current.firstQuartile
      ) {
        videoCompletion.current = {
          ...videoCompletion.current,
          firstQuartile: true,
        };
        analyticsVideoProgress(EVENTS.FIRST_QUARTILE, analyticsDimensions);
      } else if (
        videoPercentageComplete >= 0.5 &&
        !videoCompletion.current.midpoint
      ) {
        videoCompletion.current = {
          ...videoCompletion.current,
          midpoint: true,
        };
        analyticsVideoProgress(EVENTS.MIDPOINT, analyticsDimensions);
      } else if (
        videoPercentageComplete >= 0.75 &&
        !videoCompletion.current.thirdQuartile
      ) {
        videoCompletion.current = {
          ...videoCompletion.current,
          thirdQuartile: true,
        };
        analyticsVideoProgress(EVENTS.THIRD_QUARTILE, analyticsDimensions);
      }
    });

    player.addEventListener(akamai.amp.Events.PLAY, () => {
      analyticsVideoPlay(videoTitle, analyticsDimensions);
    });

    player.addEventListener(akamai.amp.Events.PAUSE, () => {
      analyticsVideoPause(videoTitle, analyticsDimensions);
    });

    player.addEventListener(akamai.amp.Events.RESUME, () => {
      analyticsVideoResume(videoTitle, analyticsDimensions);
    });

    player.addEventListener(akamai.amp.Events.ENDED, () => {
      videoCompletion.current = { ...videoCompletion, finished: true };
      analyticsVideoProgress(EVENTS.COMPLETE, analyticsDimensions);
    });

    player.addEventListener(akamai.amp.Events.SEEKED, (event) => {
      if (vodAdsExist) {
        checkIfSeekedPassedAdMarker(event);
      }
    });

    player.hls.addEventListener("hlsFragChanged", (event, data) => {
      const regex = /EXT-X-DATERANGE|/i;
      const frag = event.data.frag;
      const tags = [...frag.tagList];
      const cue = tags
        .filter((tag) => regex.test(tag))
        .map((tag) => (tag instanceof Array ? tag[0] : tag));
      const [cueType] = cue;

      if (cue == null) return;

      switch (cueType) {
        case "EXT-X-DATERANGE":
          console.debug("[#EXT-X-DATERANGE]", tags);
          // TODO: check if the current ad has been played
          if (!inAdRef.current && live) {
            if (!adReadyRef.current) {
              console.debug("[Requesting new ad]");
              inAdRef.current = true;
              // Set the ad holiday to midroll now that preroll is done
              if (adHoliday === adHolidays?.preroll) {
                setAdHoliday(adHolidays?.midroll);
              }

              initializeIMA(midRollUrl);
            }

            if (adReadyRef.current && !inAdHoliday) {
              console.debug("[Loading Ads]");
              inAdRef.current = true;
              loadAds();
            }
          }
          break;
      }
    });

    if (vodAdsExist) {
      initializeIMA(preRollUrl);
    }
  };

  /**
   * Void function to stop the player and clear
   * the current station item from the context.
   * Used when the player unmounts for cleanup.
   */
  const cleanupPlayer = () => {
    if (playerRef.current) {
      playerRef.current.end();
    }
  };

  return (
    <ErrorBoundary>
      <PlayerControllerWrapper>
        {errorType && <PlayerErrorSlate errorType={errorType} />}

        {!errorType && (
          <PlayerWrapper inAd={inAd} id="video-container">
            <Player />
          </PlayerWrapper>
        )}
        {AdContainer}
      </PlayerControllerWrapper>
    </ErrorBoundary>
  );
};

export default PlayerController;

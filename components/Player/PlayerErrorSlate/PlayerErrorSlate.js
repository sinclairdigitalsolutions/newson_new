import React from "react";

import Error from "../../Error/Error";
import { ERROR_COPY } from "../../../lib/constants";

const PlayerErrorSlate = ({ errorType }) => {
  const title = ERROR_COPY[errorType].TITLE;
  const description = ERROR_COPY[errorType].DESCRIPTION;

  return <Error title={title} description={description} errorType={errorType} />;
};

export default PlayerErrorSlate;

import styled from "styled-components";
import {
  tabletBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../../globalStyles/sharedStyles";

export const PlayerErrorSlateWrapper = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 100;
  text-align: center;
  padding: 0 ${pxToRem(100)};
  box-sizing: border-box;

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #000000 0% 0% no-repeat padding-box;
    opacity: 0.24;
  }

  @media (min-width: ${tabletBreakpoint}) {
    width: ${pxToRem(680)};
    height: ${pxToRem(382.5)};
  }
`;

export const SlateTitle = styled.h1`
  font-family: Nunito-Sans-Bold;
  font-size: ${pxToRem(30)};
  margin: 0;
`;

export const SlateDescription = styled.p`
  font-family: Nunito-Sans-Light;
  font-size: ${pxToRem(12)};
`;

export const ErrorIcon = styled.div`
  margin-bottom: ${pxToRem(10)};
`;

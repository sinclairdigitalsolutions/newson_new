import React from "react";
import { ErrorWrapper, Title, Description, ErrorIcon } from "./ErrorStyles";
import ErrorIconSvg from "../../public/images/icons/NO-error-exclamation.svg";
import VideoExpiredSvg from "../../public/images/icons/NO-error-videoexpired.svg";
import { ERROR_TYPES } from "../../lib/constants";

const Error = ({title, description, errorType}) => {
  return (
    <ErrorWrapper>
      <ErrorIcon>
        {ERROR_TYPES.AMP_ERROR === errorType ? <ErrorIconSvg /> : <VideoExpiredSvg />}
      </ErrorIcon>
      <Title>{title}</Title>
      <Description>
        {description}
      </Description>
    </ErrorWrapper>
  );
};

export default Error;

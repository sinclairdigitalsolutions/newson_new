import styled from "styled-components";
import {
  tabletBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

export const ErrorWrapper = styled.div`
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 100;
  text-align: center;
  padding: 0 ${pxToRem(100)};
  box-sizing: border-box;

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: ${(props) => props.theme.colors.black};
    opacity: 0.24;
  }
`;

export const Title = styled.h1`
  font-family: Nunito-Sans-Bold;
  font-size: ${pxToRem(30)};
  margin: 0;
`;

export const Description = styled.p`
  font-family: Nunito-Sans-Light;
  font-size: ${pxToRem(12)};
`;

export const ErrorIcon = styled.div`
  margin-bottom: ${pxToRem(10)};
`;

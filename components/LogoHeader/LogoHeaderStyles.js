import styled from "styled-components";
import {
  pxToRem,
  laptopBreakpoint,
  tabletBreakpoint,
} from "../../globalStyles/sharedStyles";
import { BANNER_SIZES_IN_PIXELS } from "../../lib/constants"; 


export const LogoHeaderWrapper = styled.div`

display: flex;
position: fixed;
top: ${(props) => props.breakingNewsEnabled ? pxToRem(BANNER_SIZES_IN_PIXELS.BREAKING_NEWS) : 0 };
padding: 0;

height: ${pxToRem(BANNER_SIZES_IN_PIXELS.LOGO_HEADER)};
align-items: center;
justify-content: space-between;
z-index: 500;
opacity: 0.78;
width: 100%;

background: ${(props) => (props.pageScrolled ? 
    "transparent linear-gradient(180deg, #101114 0%, #323740 100%) 0% 0% no-repeat padding-box" 
    : "none")};

  &:before {
    content: "";
    background: transparent
      linear-gradient(
        180deg,
        #041f3b 0%,
        #052445ef 44%,
        #0f4d8b7b 69%,
        #2748a300 100%
      )
      0% 0% no-repeat padding-box;
    display: block;
    height: ${pxToRem(140)};
    opacity: 0.35;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: -1;
  }

@media (min-width: ${laptopBreakpoint}) {
  display: none;
}
`;

export const HomeLogo = styled.div`
  list-style: none;
  display: block;
  margin-right: ${pxToRem(75)};
  padding-left: ${pxToRem(40)};

  img {
    width: ${pxToRem(100)};
  }

  @media (min-width: ${tabletBreakpoint}){
    img{
      width: auto;
    }
  }
`;

export const NavigationAboutIcon = styled.div`
  justify-content: flex-end;
  list-style: none;
  padding-right: ${pxToRem(30)};
`;
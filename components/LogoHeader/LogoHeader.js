import React, { useEffect, useState, } from "react";
import { useRouter } from "next/router";
import Link from 'next/link';
import DefaultAboutIcon from "../../public/images/icons/NO-nav-about-inactive.svg";
import ActiveAboutIcon from "../../public/images/icons/NO-nav-about-active.svg";

import { LogoHeaderWrapper, HomeLogo, NavigationAboutIcon } from "./LogoHeaderStyles";

const LogoHeader = ({ breakingNewsEnabled }) => {
  const [pageScrolled, setPageScrolled] = useState(false);
  const router = useRouter();
  const selectedRoute = router.pathname;

  const isPageScrolled = () => {
    if (window.scrollY !== 0) {
      setPageScrolled(true);
      return;
    }
    setPageScrolled(false);
  };

  useEffect(() => {
    window.addEventListener("scroll", isPageScrolled);
    return () => window.removeEventListener("scroll", isPageScrolled);
  }, []);

  return (
    <LogoHeaderWrapper 
      pageScrolled={pageScrolled}
      breakingNewsEnabled={breakingNewsEnabled}
    >
      <HomeLogo>
        <Link href="/featured">
          <a key="Home">
            <img src="/images/NO-logo.png" />
          </a>
        </Link>
      </HomeLogo>
      <NavigationAboutIcon>
        <Link href="/about">
          <a key="About" className={selectedRoute === "/about" ? "active" : ""}>
            {selectedRoute === "/about" ? (
              <ActiveAboutIcon />
            ) : (
              <DefaultAboutIcon />
            )}
          </a>
        </Link>
      </NavigationAboutIcon>
    </LogoHeaderWrapper>
  );
};

export default LogoHeader;

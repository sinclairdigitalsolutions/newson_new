import styled from "styled-components";
import {
  ButtonStyle,
  pxToRem,
  tabletBreakpoint,
  desktopBreakpoint,
} from "../../globalStyles/sharedStyles";

const whiteHeartStyles = `
  padding: 0 ${pxToRem(10)};
  background-color: transparent;

  @media (min-width: ${tabletBreakpoint}) {
    padding: 0 ${pxToRem(10)};
    font-size: ${pxToRem(13)};
  }

  @media (min-width: ${desktopBreakpoint}) {
    padding: 0 ${pxToRem(10)};
  }
`;

export const Anchor = styled.a`
  ${ButtonStyle};
  ${(props) => (props.heartButton ? whiteHeartStyles : "")}
  color: ${(props) =>
    props.theme.colors.darkBlue};
  cursor: pointer;
  font-size: ${pxToRem(11)};
  position: relative;
  z-index: 10;
  max-width: ${pxToRem(150)};
  margin-bottom: ${pxToRem(10)};
  line-height: ${pxToRem(19)};


  .svg {
    margin-right: ${pxToRem(6)};

    &__red {
      fill: ${(props) => props.theme.colors.red};
      width: ${pxToRem(14)};
      height: ${pxToRem(11)};
    }

    &__white {
      fill: ${(props) => (props.isFavourite ? "white" : "none")};
      width: ${pxToRem(30)};
      height: ${pxToRem(30)};
    }
  }

  &.liveNowButton {
    margin-right: ${pxToRem(10)};
  }

  @media (min-width: ${tabletBreakpoint}) {
    font-size: ${pxToRem(14)};
    max-width: none;
  }

  @media (min-width: ${desktopBreakpoint}) {
    font-size: ${pxToRem(16)};
  }
`;

export const LinkButtonWrapper = styled.div``;
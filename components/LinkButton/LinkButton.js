import React from "react";
import { Anchor } from "./LinkButtonStyles";
import Link from "next/link";

const LinkButton = ({ icon, label, as, href, type, clickHandler, heartButton, isFavorite, isLiveNow }) => {
  if (type === "link")
    return (
      <Link href={href} as={as}>
        <a>
          {icon && icon}
          {label}
        </a>
      </Link>
    );

  return (
    <Anchor isFavorite={isFavorite} onClick={clickHandler} heartButton={heartButton} className={isLiveNow ? "liveNowButton" : ""}>
      {icon && icon}
      {label}
    </Anchor>
  );
};

export default LinkButton;

import Link from "next/link";
import React, { useRef, useState, useEffect } from "react";
import { withRouter } from "next/router";
import Router from "next/router";

// Components & Helpers
import { GlobalNavigationData } from "./GlobalNavigationData";
import ByLocationForm from "../ByLocationForm";
import TextField from "@material-ui/core/TextField";
import ByLocationIcon from "../../public/images/icons/bylocation-icon.svg";
import SearchIcon from "../../public/images/icons/search-icon.svg";
import MobileByLocationIcon from "../../public/images/icons/NO-mobile-nav-bylocation.svg";
import MobileSearchIcon from "../../public/images/icons/NO-mobile-nav-search.svg";
import { redirectHandler } from "../../lib/routeHelpers";
import { InputAdornment } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import DefaultAboutIcon from "../../public/images/icons/NO-nav-about-inactive.svg";
import ActiveAboutIcon from "../../public/images/icons/NO-nav-about-active.svg";

// Styles
import theme from "../../globalStyles/theme";
import {
  MobileSearchLink,
  NavigationAboutIcon,
  NavigationContainer,
  NavigationHomeLogo,
  NavigationPageTabLinks,
  NavigationPageTabListItems,
  NavigationPageTabs,
  NavigationUtilities,
  SearchListItem,
  MobileLocationLink,
  ByLocationDesktop,
  NavItemWrapper,
} from "./GlobalNavigationStyles";

const GlobalNavigation = ({
  router,
  locations: { states },
  breakingNewsEnabled,
}) => {
  const searchRef = useRef(null);
  const [defaultSearchTerm, setDefaultSearchTerm] = useState("");
  const [pageScrolled, setPageScrolled] = useState(false);
  const searchTerm = router?.query?.term;
  const selectedRoute = router.pathname;

  const isPageScrolled = () => {
    if (window.scrollY !== 0) {
      setPageScrolled(true);
      return;
    }

    setPageScrolled(false);
  };

  useEffect(() => {
    window.addEventListener("scroll", isPageScrolled);

    return () => window.removeEventListener("scroll", isPageScrolled);
  }, []);

  useEffect(() => {
    setDefaultSearchTerm(searchTerm);
  }, [searchTerm, router]);

  useEffect(() => {
    //Reset the autofill value for material ui uncontrolled component
    if (selectedRoute !== "/search") {
      searchRef.current.value = "";
      searchRef.current?.labels[0].classList.remove("MuiFormLabel-filled");
    }
  }, [router]);

  return (
    <NavigationContainer
      breakingNewsEnabled={breakingNewsEnabled}
      pageScrolled={pageScrolled}
    >
      <NavigationHomeLogo>
        <Link href="/featured">
          <a key="Home">
            <img src="/images/NO-logo.png" />
          </a>
        </Link>
      </NavigationHomeLogo>
      <NavigationPageTabs>
        {GlobalNavigationData.map((navItem) => (
          <Link href={navItem.navRoute}>
            <NavigationPageTabListItems
              key={navItem.navName}
              className={selectedRoute === navItem.navRoute ? "active" : ""}
            >
              <NavigationPageTabLinks
                selectedRouteLinkStyle={
                  selectedRoute === navItem.navRoute
                    ? theme.colors.navy
                    : theme.colors.white
                }
              >
                <NavItemWrapper>
                  {navItem.mobileNavIcon}
                  {navItem.navIcon}
                </NavItemWrapper>

                <span>{navItem.navName}</span>
              </NavigationPageTabLinks>
            </NavigationPageTabListItems>
          </Link>
        ))}
      </NavigationPageTabs>
      <NavigationUtilities>
        <SearchListItem className={selectedRoute === "/search" ? "active" : ""}>
          <Link href="/search">
            <MobileSearchLink>
              <MobileSearchIcon
                className="icon mobile"
                onClick={() => {
                  Router.push({
                    pathname: "/search",
                  });
                }}
              />
              Search
            </MobileSearchLink>
          </Link>

          <div className="desktop-link">
            <FormControl
              className={
                defaultSearchTerm?.length && selectedRoute === "/search"
                  ? "filled"
                  : ""
              }
            >
              <TextField
                id="standard-search"
                type="search"
                label="Search"
                autoComplete="false"
                inputRef={searchRef}
                placeholder="Search by call letters, City or State"
                variant="outlined"
                InputLabelProps={{ shrink: true }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon
                        className="icon desktop"
                        onClick={() => {
                          searchRef.current.focus();
                        }}
                      />
                    </InputAdornment>
                  ),
                }}
                onKeyPress={(e) => {
                  if (e.key === "Enter" && e.target.value.length > 1) {
                    redirectHandler(e.target.value, "/search");
                  }
                }}
                onChange={(e) => {
                  if (e.target.value === "") {
                    setDefaultSearchTerm(null);
                  }
                }}
              />
            </FormControl>
          </div>
        </SearchListItem>
        <NavigationPageTabListItems>
          <MobileLocationLink
            onClick={() => {
              Router.push({
                pathname: "/byLocation",
              });
            }}
          >
            <MobileByLocationIcon className="icon mobile icon__bylocation" />
            By Location
          </MobileLocationLink>
          <ByLocationDesktop>
            <ByLocationIcon className="icon desktop" />
            <ByLocationForm states={states} />
          </ByLocationDesktop>
        </NavigationPageTabListItems>
      </NavigationUtilities>
      <NavigationAboutIcon>
        <Link href="/about">
          <a key="About" className={selectedRoute === "/about" ? "active" : ""}>
            {selectedRoute === "/about" ? (
              <ActiveAboutIcon />
            ) : (
              <DefaultAboutIcon />
            )}
          </a>
        </Link>
      </NavigationAboutIcon>
    </NavigationContainer>
  );
};

export default withRouter(GlobalNavigation);

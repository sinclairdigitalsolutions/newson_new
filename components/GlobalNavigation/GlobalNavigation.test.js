import React from "react";
import GlobalNavigation from "./GlobalNavigation";
import { render } from "@testing-library/react";
import { withRouter } from "next/router";

const setup = () => render(withRouter(<GlobalNavigation />));

beforeEach(() => {
  console.error = jest.fn();
});

describe("Navigation Component", () => {
  it("should render the navigation links", () => {
    const { getByText } = setup();

    expect(getByText("Featured"));
    expect(getByText("Live Now"));
    expect(getByText("By Location"));
    expect(getByText("Search"));
    expect(getByText("Favorites"));
  });
});

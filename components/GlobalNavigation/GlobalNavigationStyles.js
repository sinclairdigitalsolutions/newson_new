import styled, { css } from "styled-components";
import {
  laptopBreakpoint,
  desktopBreakpoint,
  pxToRem,
} from "../../globalStyles/sharedStyles";

const navPsuedoElStyles = css`
  border-radius: ${pxToRem(25)};
  bottom: 0;
  content: "";
  display: block;
  height: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  transition: opacity 250ms ease-in-out;
  width: 100%;
  z-index: -1;
`;

export const NavigationContainer = styled.nav`
  display: flex;
  position: fixed;
  bottom: 0;
  height: ${pxToRem(50)};
  margin: 0;
  padding: 0;
  align-items: center;
  justify-content: space-between;
  z-index: 500;
  opacity: 0.78;
  width: 100%;

  background: transparent linear-gradient(180deg, #101114 0%, #323740 100%) 0%
    0% no-repeat padding-box;

  @media (min-width: ${laptopBreakpoint}) {
    height: ${pxToRem(65)};
    top: ${(props) => (props.breakingNewsEnabled === true ? `${pxToRem(60)}` : "0")};
    background: ${(props) => (props.pageScrolled ? "" : "none")};

    &:before {
      content: "";
      background: transparent
        linear-gradient(
          180deg,
          #041f3b 0%,
          #052445ef 44%,
          #0f4d8b7b 69%,
          #2748a300 100%
        )
        0% 0% no-repeat padding-box;
      display: block;
      height: ${pxToRem(140)};
      opacity: 0.35;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
  }
`;

export const NavigationHomeLogo = styled.div`
  list-style: none;
  display: none;
  user-select: none;

  @media (min-width: ${laptopBreakpoint}) {
    display: block;
    margin-right: ${pxToRem(75)};
    padding-left: ${pxToRem(40)};
  }

  @media (min-width: ${desktopBreakpoint}) {
    margin-right: ${pxToRem(128)};
    padding-left: ${pxToRem(60)};
  }
`;

export const NavigationAboutIcon = styled.div`
  display: none;
  justify-content: flex-end;
  list-style: none;
  padding-right: ${pxToRem(30)};

  a {
    position: relative;

    &:before {
      border-radius: 50%;
      border: solid ${pxToRem(2)} ${(props) => props.theme.colors.white};
      content: "";
      display: block;
      height: ${pxToRem(28)};
      left: ${pxToRem(-3)};
      opacity: 0;
      position: absolute;
      top: ${pxToRem(-3)};
      transition: opacity 250ms ease-in-out;
      width: ${pxToRem(28)};
    }

    &:hover {
      &::before {
        opacity: 1;
      }
    }
  }

  @media (min-width: ${laptopBreakpoint}) {
    display: flex;
  }
`;

export const NavigationPageTabs = styled.ul`
  display: flex;
  align-items: center;
  padding: 0;
  width: 60%;
  justify-content: space-between;
  margin: 0;
  height: 100%;

  @media (min-width: ${laptopBreakpoint}) {
    justify-content: unset;
    width: auto;
  }
`;

export const NavigationUtilities = styled.ul`
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 0;
  width: 40%;
  height: 100%;

  @media (min-width: ${laptopBreakpoint}) {
    justify-content: center;
    width: auto;
    margin-right: ${pxToRem(16)};
    margin-left: ${pxToRem(16)};
  }

  @media (min-width: ${laptopBreakpoint}) {
    margin-left: auto;
  }

  @media (min-width: ${desktopBreakpoint}) {
    margin-right: ${pxToRem(96)};
  }
`;

export const NavigationPageTabListItems = styled.li`
  align-items: center;
  background: transparent;
  display: flex;
  justify-content: space-evenly;
  list-style: none;
  line-height: 1;
  transition: background 250ms ease-in-out;
  flex-direction: column;
  height: 100%;
  flex: 1;
  position: relative;
  user-select: none;
  cursor: pointer;

  &:last-of-type {
    margin-right: 0;
  }

  &.active,
  &:active {
    &:after {
      opacity: 1;
    }
  }

  &:hover {
    &:before {
      opacity: 1;
    }

    .icon path {
      fill: ${(props) => props.theme.colors.white};
    }

    a {
      overflow: hidden;
      position: relative;

      &:after {
        background-color: ${(props) => props.theme.colors.lightPastelBlue};
        border-radius: 50%;
        bottom: ${pxToRem(-6)};
        content: "";
        display: block;
        height: ${pxToRem(12)};
        position: absolute;
        width: ${pxToRem(12)};
      }

      @media (min-width: ${laptopBreakpoint}) {
        &:after {
          display: none;
        }
      }
    }
  }

  a {
    color: ${(props) => props.theme.colors.white};
  }

  @media (min-width: ${laptopBreakpoint}) {
    border-radius: ${pxToRem(20)};
    flex-direction: row;
    height: auto;
    justify-content: center;
    margin-right: 0;
    padding: ${pxToRem(5)} ${pxToRem(14)};
    white-space: nowrap;

    &:before {
      ${navPsuedoElStyles};
      border: solid ${pxToRem(2)} ${(props) => props.theme.colors.white};
      left: ${pxToRem(-2)};
      top: ${pxToRem(-2)};
    }

    &:after {
      ${navPsuedoElStyles};
      background-color: white;
      left: 0;
      top: 0;
      transform: scale(0.98, 0.9);
    }

    &.active,
    &:focus,
    &:active {
      color: ${(props) => props.theme.colors.navy};
      a {
        color: ${(props) => props.theme.colors.navy};
      }
    }

    &.active,
    &:hover,
    &:active {
      .icon path {
        fill: currentColor;
      }
    }
  }

  @media (min-width: ${laptopBreakpoint}) {
    margin-right: ${pxToRem(20)};
  }

  @media (min-width: ${desktopBreakpoint}) {
    margin-right: ${pxToRem(24)};
    padding: ${pxToRem(5)} ${pxToRem(15)};
  }

  .icon {
    fill: currentColor;

    @media (min-width: ${laptopBreakpoint}) {
      margin-right: ${pxToRem(5)};
    }

    &.mobile {
      @media (min-width: ${laptopBreakpoint}) {
        display: none;
      }
    }

    &.desktop {
      display: none;

      @media (min-width: ${laptopBreakpoint}) {
        display: block;
        width: ${pxToRem(10)};
      }
    }

    &__live-now {
      @media (min-width: ${laptopBreakpoint}) {
        stroke: currentColor;
      }
    }
  }

  .MuiInputBase-root {
    color: currentColor;
  }

  label + .MuiInput-formControl {
    margin-top: 0;
  }

  .MuiSelect-icon,
  .MuiInput-underline:before,
  .MuiInput-underline:after {
    display: none;
  }

  .mui-form-control {
    @media (min-width: ${laptopBreakpoint}) {
      flex: 1;
      width: ${pxToRem(100)};
    }
  }

  .mui-select-label,
  .MuiSelect-select,
  .MuiInputLabel-shrink {
    font-family: Nunito-Sans-Light;
    color: currentColor;
    font-size: ${pxToRem(9)};

    &.MuiSelect-select {
      margin-top: ${pxToRem(4)};
      padding: 0;

      @media (min-width: ${laptopBreakpoint}) {
        margin-top: 0;
      }
    }

    @media (min-width: ${laptopBreakpoint}) {
      font-size: ${pxToRem(15)};
    }
  }

  .MuiInputLabel-formControl {
    color: ${(props) => props.theme.colors.white};
    font-family: Nunito-Sans-Light;
    font-size: ${pxToRem(15)};
    transform: translate(0, 1px) scale(1);
    z-index: 5;
  }

  .MuiFormLabel-root.Mui-focused {
    color: ${(props) => props.theme.colors.white};
  }

  .MuiInputLabel-shrink {
    transform: translate(0, 1px) scale(1);
    color: ${(props) => props.theme.colors.white};
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  .MuiSelect-nativeInput {
    border: 0;
  }
`;

export const SearchListItem = styled.li`
  align-items: center;
  color: ${(props) => props.theme.colors.white};
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
  justify-content: space-evenly;
  list-style-type: none;
  user-select: none;

  @media (min-width: ${laptopBreakpoint}) {
    justify-content: center;
    flex-direction: row;
    margin-right: ${pxToRem(10)};
  }

  &.active,
  &:hover {
    color: ${(props) => props.theme.colors.white};

    a {
      color: ${(props) => props.theme.colors.white};
      overflow: hidden;
      position: relative;

      &:after {
        background-color: ${(props) => props.theme.colors.lightPastelBlue};
        border-radius: 50%;
        bottom: ${pxToRem(-6)};
        content: "";
        display: block;
        height: ${pxToRem(12)};
        position: absolute;
        width: ${pxToRem(12)};
      }

      @media (min-width: ${laptopBreakpoint}) {
        &:after {
          display: none;
        }
      }
    }
  }

  .MuiFormControl-root {
    &.filled {
      .desktop.icon {
        transform: translateX(${pxToRem(30)});
        fill: ${(props) => props.theme.colors.navy};
      }

      .MuiOutlinedInput-input {
        color: ${(props) => props.theme.colors.navy};
        font-size: ${pxToRem(15)};
        padding: ${pxToRem(2)} ${pxToRem(5)} ${pxToRem(2)} ${pxToRem(30)};
        background: ${(props) => props.theme.colors.white};
      }
    }
  }

  legend {
    display: none;
  }

  .icon {
    &.mobile {
      @media (min-width: ${laptopBreakpoint}) {
        display: none;
      }
    }

    &.desktop {
      display: none;

      @media (min-width: ${laptopBreakpoint}) {
        display: block;
        fill: white;
        position: relative;
        top: ${pxToRem(-1)};
        z-index: 10;
      }
    }
  }

  .desktop-link {
    display: none;
    position: relative;

    @media (min-width: ${laptopBreakpoint}) {
      align-items: center;
      display: flex;
      justify-content: center;

      &::focus {
        &::before {
          opacity: 0;
        }
      }

    }

    .MuiOutlinedInput-root {
      &::before {
        border-radius: ${pxToRem(25)};
        border: solid ${pxToRem(2)} ${(props) => props.theme.colors.white};
        bottom: 0;
        content: "";
        display: block;
        height: 100%;
        left: ${pxToRem(-2)};
        opacity: 0;
        position: absolute;
        right: 0;
        top: ${pxToRem(-2)};
        transition: opacity 250ms ease-in-out;
        width: 100%;
      }
    }

    &:hover {
      .MuiOutlinedInput-root:not(.Mui-focused) {
        &::before {
          opacity: 1;
        }
      }
    }

  .MuiOutlinedInput-input {
    padding: 0;
  }

  .MuiInputLabel-outlined.MuiInputLabel-shrink,
  .MuiInputLabel-formControl {
    color: currentColor;
    font-family: Nunito-Sans-Light;
    transform: translate(${pxToRem(30)}, ${pxToRem(6)});

    &.MuiFormLabel-filled {
      display: none;
    }
  }

  .MuiInputLabel-outlined {
    color: ${(props) => props.theme.colors.white};
    font-family: Nunito-Sans-Light;
    font-size: ${pxToRem(9)};

    @media (min-width: ${laptopBreakpoint}) {
      font-size: ${pxToRem(15)};
    }
  }

  .MuiOutlinedInput-root {
    border-radius: ${pxToRem(20)};
  }

  .MuiInputBase-input::placeholder {
    visibility: hidden;
    color: transparent;
  }

  .Mui-focused {
    @media (min-width: ${laptopBreakpoint}) {
      &.MuiInputLabel-outlined {
        display: none;
      }

      .MuiInputBase-input::placeholder {
        visibility: visible;
        color: currentColor;
      }

      .desktop.icon {
        transform: translateX(${pxToRem(30)});
        fill: ${(props) => props.theme.colors.navy};
      }
    }
  }

  .MuiFormLabel-root.Mui-focused {
    color: ${(props) => props.theme.colors.white};

    @media (min-width: ${laptopBreakpoint}) {
      color: transparent;
    }
  }
  .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline {
    border-color: transparent;
    outline: none;
  }

  .MuiOutlinedInput-input {
    border-radius: ${pxToRem(20)};
    color: ${(props) => props.theme.colors.white};
    font-family: Nunito-Sans-Light;
    font-size: ${pxToRem(15)};
    position: absolute;
    transform: translate(-71%, ${pxToRem(-90)});
    transition: background 250ms ease-in-out, width 250ms ease-in-out;
    width: 0;
    padding: ${pxToRem(5)};

    &:focus {
      background: ${(props) => props.theme.colors.white};
      color: ${(props) => props.theme.colors.navy};
      padding: ${pxToRem(5)} ${pxToRem(5)} ${pxToRem(5)} ${pxToRem(30)};
      font-size: ${pxToRem(25)};
      width: 90vw;
    }

    &.mobile-hide {
      @media (min-width: ${laptopBreakpoint}) {
        display: block;
      }
    }

    @media (min-width: ${laptopBreakpoint}) {
      width: ${pxToRem(36)};
      position: static;
      transform: translate(0, 0);

      &:focus {
        width: ${pxToRem(160)};
        transform: translate(0, 0);
        font-size: ${pxToRem(15)};
      }
    }

    @media (min-width: ${laptopBreakpoint}) {
      width: ${pxToRem(68)};
    }

    @media (min-width: ${desktopBreakpoint}) {
      width: ${pxToRem(100)};
    }
  }
`;

export const NavigationPageTabLinks = styled.a`
  align-items: center;
  color: ${(props) => props.selectedRouteLinkStyle};
  cursor: pointer;
  display: flex;
  font-family: Nunito-Sans-Light;
  font-size: ${pxToRem(9)};
  letter-spacing: 0;
  text-align: left;
  text-decoration: none;
  transition: color 250ms ease-in-out;
  flex-direction: column;
  flex: 1;
  justify-content: space-evenly;

  @media (min-width: ${laptopBreakpoint}) {
    font-size: ${pxToRem(15)};
    flex-direction: row;
  }
`;

export const MobileSearchLink = styled.a`
  align-items: center;
  color: ${(props) => props.selectedRouteLinkStyle};
  cursor: pointer;
  display: flex;
  font-family: Nunito-Sans-Light;
  font-size: ${pxToRem(9)};
  letter-spacing: 0;
  text-align: left;
  text-decoration: none;
  transition: color 250ms ease-in-out;
  flex-direction: column;
  flex: 1;
  justify-content: space-evenly;

  @media (min-width: ${laptopBreakpoint}) {
    display: none;
  }
`;

export const MobileByLocation = styled.div`
  @media (min-width: ${laptopBreakpoint}) {
    display: none;
  }
`;

export const MobileLocationLink = styled.a`
  align-items: center;
  color: ${(props) => props.selectedRouteLinkStyle};
  cursor: pointer;
  display: flex;
  font-family: Nunito-Sans-Light;
  font-size: ${pxToRem(9)};
  letter-spacing: 0;
  text-align: left;
  text-decoration: none;
  transition: color 250ms ease-in-out;
  flex-direction: column;
  flex: 1;
  justify-content: space-evenly;

  @media (min-width: ${laptopBreakpoint}) {
    display: none;
  }
`;

export const ByLocationDesktop = styled.div`
  display: none;
  align-items: baseline;
  user-select: none;

  @media (min-width: ${laptopBreakpoint}) {
    display: flex;
  }
`;

export const NavItemWrapper = styled.div`
  width: 100%; 
  height: 100%;
`;
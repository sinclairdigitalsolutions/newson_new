import FeaturedIcon from "../../public/images/icons/featured-icon.svg";
import MobileFeaturedIcon from "../../public/images/icons/NO-mobile-nav-featured.svg";
import MobileLiveIcon from "../../public/images/icons/NO-mobile-nav-live.svg";
import MobileFavoritesIcon from "../../public/images/icons/NO-mobile-nav-favorite.svg";
import LiveNowIcon from "../../public/images/icons/live-now-icon.svg";
import FavoritesIcon from "../../public/images/icons/favorites-icon.svg";

export const GlobalNavigationData = [
  {
    navName: "Featured",
    navRoute: "/featured",
    navIcon: <FeaturedIcon className="icon icon__featured desktop" />,
    mobileNavIcon: (
      <MobileFeaturedIcon className="icon icon__featured mobile" />
    ),
    navIconUnfocused: "/images/NO-navicon-featured.png",
    navIconFocused: "/images/NO-navicon-featured-active.png",
  },
  {
    navName: "Live Now",
    navRoute: "/liveNow",
    mobileNavIcon: <MobileLiveIcon className="icon icon__live-now mobile" />,
    navIcon: <LiveNowIcon className="icon icon__live-now desktop" />,
    navIconUnfocused: "/images/NO-navicon-live.png",
    navIconFocused: "/images/NO-navicon-live-active.png",
  },
  {
    navName: "Favorites",
    navRoute: "/favorites",
    mobileNavIcon: (
      <MobileFavoritesIcon className="icon icon__favorites mobile" />
    ),
    navIcon: <FavoritesIcon className="icon icon__favorites desktop" />,
    navIconUnfocused: "/images/NO-navicon-favorite.png",
    navIconFocused: "/images/NO-navicon-favorite-active.png",
  },
];

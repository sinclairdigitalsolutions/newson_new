import useSWR from "swr";

const useLocalItemsFetch = (params) => {
  const { frequency = "12", latitude, longitude } = params;
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR(
    `/api/localStations/${frequency}/${latitude}/${longitude}`,
    fetcher
  );

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useLocalItemsFetch;

import useSWR from "swr";

const useBreakingNewsFetch = (params) => {
  const { latitude, longitude } = params;
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR(`/api/breakingNews/${latitude}/${longitude}`, {
    fetcher,
  });

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useBreakingNewsFetch;

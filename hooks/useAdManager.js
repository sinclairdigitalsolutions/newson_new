import React, { useState, useRef, useEffect } from "react";
import { analyticsAdInitiatePlay, analyticsAdProgress, analyticsAdResume, analyticsAdPause } from "../lib/analytics";
import { ANALYTICS_VALUES } from '../lib/constants'

const useAdManager = (adHoliday, analyticsDimensions) => {
  const [inAd, setInAd] = useState(null);
  const [adReady, setAdReady] = useState(false);
  const [inAdHoliday, setInAdHoliday] = useState(false);
  const adsLoadedRef = useRef(false);
  const adContainerRef = useRef(null);
  const videoElementRef = useRef(null);
  const adDisplayContainerRef = useRef(null);
  const adsLoaderRef = useRef(null);
  const adsManagerRef = useRef(null);
  const { EVENTS } = ANALYTICS_VALUES;

  const { google } = window;

  // Cleanup Ads on unmount
  useEffect(() => () => cleanupAds(), []);

  /**
   * Function that will initialize IMA and make an ad request with the tag passed in
   * This will pre-load an ad to be played
   * @param  {string} adTagUrl - url of the ad to request
   */
  const initializeIMA = (adTagUrl) => {
    if (!adDisplayContainerRef.current || !adsLoadedRef.current) {
      videoElementRef.current = document.getElementsByClassName(
        "amp-media-element"
      )[0];
      adContainerRef.current = document.getElementById("ad-container");
      console.debug("initializing IMA", videoElementRef, adContainerRef);

      adDisplayContainerRef.current = new google.ima.AdDisplayContainer(
        adContainerRef.current,
        videoElementRef.current
      );
      adsLoaderRef.current = new google.ima.AdsLoader(
        adDisplayContainerRef.current
      );

      adsLoaderRef.current.addEventListener(
        google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
        onAdsManagerLoaded,
        false
      );

      adsLoaderRef.current.addEventListener(
        google.ima.AdErrorEvent.Type.AD_ERROR,
        onAdError,
        false
      );

      adsLoaderRef.current.addEventListener(
        google.ima.AdEvent.Type.COMPLETE,
        onAdEvent
      );

      // Let the AdsLoader know when the video has ended
      videoElementRef.current.addEventListener("ended", () => {
        adsLoaderRef.current?.contentComplete();
      });
    }

    const adsRequest = new google.ima.AdsRequest();
    adsRequest.adTagUrl = adTagUrl;

    // Specify the linear and nonlinear slot sizes. This helps the SDK to
    // select the correct creative if multiple are returned.
    adsRequest.linearAdSlotWidth = videoElementRef.current.clientWidth;
    adsRequest.linearAdSlotHeight = videoElementRef.current.clientHeight;
    adsRequest.nonLinearAdSlotWidth = videoElementRef.current.clientWidth;
    adsRequest.nonLinearAdSlotHeight = videoElementRef.current.clientHeight / 3;

    // Pass the request to the adsLoader to request ads
    console.debug("[Requesting Ads] => ", { adsRequest });
    adsLoaderRef.current.requestAds(adsRequest);
  };

  /**
   * Function that will attempt to start the pre-loaded ad
   * optional param if the ad being loaded is a pre-roll. 
   * @param  {boolean} preroll - if the ad being loaded is a pre-roll
   */
  const loadAds = (preroll = false) => {
    console.debug("[loadAds] => Function call");
    // Prevent this function from running on if there are already ads loaded
    if (adsLoadedRef.current) {
      return;
    }

    adsLoadedRef.current = true;

    // Initialize the container. Must be done via a user action on mobile devices.
    if (preroll) {
      // This line was causing an issue but leaving it for now as it's IMA code. Will investigate further
      // videoElementRef.current.load();
    }

    adDisplayContainerRef.current.initialize();

    var width = videoElementRef.current.clientWidth;
    var height = videoElementRef.current.clientHeight;
    try {
      adsManagerRef.current.init(width, height, google.ima.ViewMode.NORMAL);
      adsManagerRef.current.start();
    } catch (adError) {
      // Play the video without ads, if an error occurs
      console.error("AdsManager could not be started", adError);
      setInAd(false);
      videoElementRef.current.play();
    }
  };

  const onAdsManagerLoaded = (adsManagerLoadedEvent) => {
    console.debug("[adsManagerLoaded] => ", { adsManagerLoadedEvent });
    setAdReady(true);

    // Instantiate the AdsManager from the adsLoader response and pass it the video element
    adsManagerRef.current = adsManagerLoadedEvent.getAdsManager(
      videoElementRef.current
    );

    // Listen to any additional events, if necessary.
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.LOADED,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.STARTED,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.FIRST_QUARTILE,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.MIDPOINT,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.THIRD_QUARTILE,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.COMPLETE,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.PAUSED,
      onAdEvent
    );
    adsManagerRef.current.addEventListener(
      google.ima.AdEvent.Type.RESUME,
      onAdEvent
    );
  };

  const onAdEvent = (adEvent) => {
    // Retrieve the ad from the event. Some events (e.g. ALL_ADS_COMPLETED)
    // don't have ad object associated.
    // not using this currently but may be needed.
    const ad = adEvent.getAd();
    switch (adEvent.type) {
      case google.ima.AdEvent.Type.LOADED:
        console.debug("[adLoaded]", adEvent)
        // This is the first event sent for an ad - it is possible to
        // determine whether the ad is a video ad or an overlay.
        break;
      case google.ima.AdEvent.Type.STARTED:
        // Track add play
        analyticsAdInitiatePlay(analyticsDimensions);
        // This event indicates the ad has started - the video player
        // can adjust the UI, for example display a pause button and
        // remaining time.
        setInAd(true);
        break;
      case google.ima.AdEvent.Type.FIRST_QUARTILE:
        // Track ad reaching 25%
        analyticsAdProgress(EVENTS.FIRST_QUARTILE, analyticsDimensions);
        break;
      case google.ima.AdEvent.Type.MIDPOINT:
        // Track add reaching 50%
        analyticsAdProgress(EVENTS.MIDPOINT, analyticsDimensions);
        break;
      case google.ima.AdEvent.Type.THIRD_QUARTILE:
        // Track add reaching 75%
        analyticsAdProgress(EVENTS.THIRD_QUARTILE, analyticsDimensions);
        break;
      case google.ima.AdEvent.Type.COMPLETE:
        // Tracks ad completing
        analyticsAdProgress(EVENTS.COMPLETE, analyticsDimensions);

        // This event indicates the ad has finished - the video player
        // can perform appropriate UI actions, such as removing the timer for
        // remaining time detection.

        if (adHoliday) {
          setInAdHoliday(true);
          setTimeout(() => {
            setInAdHoliday(false);
          }, adHoliday);
        }
      
        cleanupAds();
        break;
      case google.ima.AdEvent.Type.PAUSED:
        // Track ad pausing
        analyticsAdPause(analyticsDimensions);
        break;
      case google.ima.AdEvent.Type.RESUMED:
        // Track ad resuming
        analyticsAdResume(analyticsDimensions);
        break;
      default:
        console.debug("[AdEvent] => Default Case", adEvent);
    }
  };

  const onAdError = (adErrorEvent) => {
    // Handle the error logging.
    console.debug("[AD Error]", adErrorEvent.getError());

    if (adsManagerRef.current) {
      videoElementRef.current.play();
      cleanupAds();
    }
  };

  const cleanupAds = () => {
    console.debug("[cleanupAds] => Function call");
    setAdReady(false);
    adsManagerRef.current = null;
    adsLoadedRef.current = false;
    adsLoaderRef.current = null;
    setInAd(false);
  };

  const AdContainer = (
    <div
      id="ad-container"
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
      }}
    ></div>
  );

  return [initializeIMA, loadAds, AdContainer, inAd, adReady, inAdHoliday];
};

export default useAdManager;

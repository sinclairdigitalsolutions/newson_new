import useSWR from "swr";

const useStationFetch = (params) => {
  const { id, latitude, longitude } = params;
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR(
    `/api/stationDetails/${id}/${latitude}/${longitude}`,
    fetcher
  );

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useStationFetch;

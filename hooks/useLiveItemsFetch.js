import useSWR from "swr";
import { DEFAULT_FREQUENCY } from "../lib/constants";

const useLiveFetch = (params) => {
  const { frequency = DEFAULT_FREQUENCY.LIVE_ITEMS_PAGE, latitude, longitude } = params;
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR(
    `/api/liveNow/${frequency}/${latitude}/${longitude}`,
    fetcher
  );

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useLiveFetch;

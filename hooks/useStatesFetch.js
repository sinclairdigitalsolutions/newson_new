import useSWR from "swr";

const useStatesFetch = () => {
  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data, error } = useSWR(`/api/getStates`, {
    refreshInterval: 0,
    fetcher,
  });

  return {
    states: data?.states,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useStatesFetch;

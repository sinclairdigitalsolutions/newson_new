import useSWR from "swr";

const useFavoriteFetch = (channels) => {
  const shouldFetch = channels?.length > 0;
  const fetcher = (...args) => fetch(...args).then((res) => res.json());

  const { data, error } = useSWR(
    shouldFetch ? `/api/favorites/${channels}` : null,
    fetcher
  );

  return {
    channels: data?.channels,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useFavoriteFetch;

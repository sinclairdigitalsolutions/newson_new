import useSWR from "swr";
import { useEffect, useState } from "react";

const useCoordinatesFetch = () => {
  const [shouldFetch, setShouldFetch] = useState(false);
  //const [userLocation, setUserLocation] = useState(null);

  useEffect(() => {
    if (!localStorage.getItem("userLocation")) {
      setShouldFetch(true);
    }
  }, []);

  const fetcher = (...args) =>
    fetch(...args).then((res) => {
      return res.json();
    });

  const { data, error } = useSWR(shouldFetch ? `/api/getCoordinates` : null, {
    refreshInterval: 0,
    fetcher,
  });

  return {
    userCoordinates: data ?? { latitude: "", longitude: "" },
    isLoading: !error && !data,
    isError: error,
  };
};

export default useCoordinatesFetch;

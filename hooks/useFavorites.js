import React, { useState, useEffect } from "react";
import useStationFetch from "../hooks/useStationFetch";
import LinkButton from "../components/LinkButton/LinkButton";
import FavoriteIcon from "../public/images/icons/favorites-icon.svg";
import FavoriteIconActive from "../public/images/icons/NO-web-favorite-active.svg";
import FavoriteIconInactive from "../public/images/icons/NO-web-favorite-inactive.svg";
import { analyticsAddFavoritesEvent, analyticsRemoveFavoritesEvent } from "../lib/analytics";
import { getCallSignFromStationName } from "../lib/utils";

const useFavorites = (stationID, stationName, item) => {
  const [favorites, setFavorites] = useState(null);
  const [isFavorite, setIsFavorite] = useState(false);
  const stationCallSign = getCallSignFromStationName(stationName);

  useEffect(() => {
    setFavorites(JSON.parse(localStorage.getItem("favorites")) || []);
  }, []);

  useEffect(() => {
    if (favorites && favorites.length > 0) {
      localStorage.setItem("favorites", JSON.stringify(favorites));
      setIsFavorite(favorites.includes(stationID));
    } else if (favorites && favorites.length === 0) {
      localStorage.setItem("favorites", JSON.stringify([]));
    }
  }, [favorites]);

  /**
   * Adds or removes the station id from the favorites array
   * stored in browser local storage (used for closest thing to
   * persistance without a uid for users in the db)
   */
  const toggleFavorite = (item) => {
    let array = favorites;
    let addArray = true;
    let fav = item || stationID;

    // If the array is empty, just push the new favorite to the array
    if (array.length === 0) {
      array.push(fav);
      setIsFavorite(true);
      addArray = false;
    } else {
      // Check to see if the fav is already a favorite
      array.forEach((item, index) => {
        if (item === fav) {
          // The fav already exists so we are removing it from our favorites
          array.splice(index, 1)
          addArray = false;
          setIsFavorite(false);

          analyticsRemoveFavoritesEvent(stationCallSign, stationName)
        }
      });

      if (addArray) {
        // The fav does not exists in our favorites so we can add it to the array
        array.push(fav);
        addArray = false;
        setIsFavorite(true);

        analyticsAddFavoritesEvent(stationCallSign, stationName);
      }
    }
    setFavorites([...array]);
  };

  const FavoriteButton = ({ item, heartButton }) => {
    const icon = isFavorite ? (
      <FavoriteIconActive className="svg svg__white" />
    ) : (
      <FavoriteIconInactive className="svg svg__white" />
    );

    return (
      <>
        {!heartButton ? (
          <LinkButton
            icon={<FavoriteIcon className="svg svg__red" />}
            label={`${
              isFavorite || favorites?.includes(item) ? "Remove from" : "Add to"
            } Favorites`}
            clickHandler={() => toggleFavorite(item)}
          />
        ) : null}
        {heartButton ? (
          <LinkButton
            icon={icon}
            clickHandler={() => toggleFavorite(item)}
            heartButton={heartButton}
            isFavorite={isFavorite || favorites?.includes(item)}
          />
        ) : null}
      </>
    );
  };

  return [favorites, FavoriteButton];
};

export default useFavorites;

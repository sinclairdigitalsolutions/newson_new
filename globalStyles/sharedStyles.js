import styled, { css } from "styled-components";

export const tinyScreenBreakpoint = `${430}px`;
export const tabletBreakpoint = `${768}px`;
export const laptopBreakpoint = `${1024}px`;
export const desktopBreakpoint = `${1280}px`;
export const largeScreenBreakpoint = `${2048}px`;
export const ginormousScreenBreakpoint = `${3840}px`;

export const ButtonStyle = css`
  align-items: center;
  background: ${(props) => props.theme.colors.white};
  border-radius: 3.125rem;
  display: inline-flex;
  font-family: Nunito-Sans-Regular;
  padding: 0.1875rem 0.9375rem;
  text-decoration: none;
  font-size: 0.6875rem;

  @media (min-width: ${tabletBreakpoint}) {
    padding: 0.375rem 1.5625rem;
    font-size: 0.8125rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    padding: 0.5625rem 2.125rem;
  }
`;

export const LiveBadge = styled.p`
  align-items: center;
  background: ${(props) => props.theme.colors.red};
  border-radius: 5.25rem;
  color: ${(props) => props.theme.colors.white};
  display: flex;
  font-family: Nunito-Sans-BoldItalic;
  font-size: 0.875rem;
  height: 1.375rem;
  justify-content: center;
  margin: 0 ${pxToRem(5)} 0 0;
  width: 3.625rem;
  z-index: 100;
  top: ${pxToRem(15)}; 
  right: ${props => props.isFavorite ? pxToRem(40) : pxToRem(11)}; 

  ${props => {
    return props.stationGroupStyle === "carousel"
    ? "position: absolute;"
    : ""
  }}

  &:after {
    border-bottom: 4px solid transparent;
    border-left: 5px solid ${(props) => props.theme.colors.white};
    border-top: 4px solid transparent;
    content: "";
    display: block;
    height: 0;
    margin-left: 0.3125rem;
    width: 0;
  }

  @media (min-width: ${tabletBreakpoint}) {
    position: absolute;
    top: ${pxToRem(5)};
  }
`;

export function pxToRem(px) {
  const mulitplier = 0.0625;

  return `${px * mulitplier}rem`;
}

export const FavoriteBadge = styled.p`
  align-items: center;
  display: flex;
  fill: ${(props) => props.theme.colors.red};
  height: 1.028125rem;
  justify-content: center;
  margin: 0;
  right: ${pxToRem(11)};
  top: ${pxToRem(18)};
  width: 1.25rem;
  z-index: 100;

  .svg {
    width: ${pxToRem(20)};
  }

  ${props => {
    return props.stationGroupStyle === "carousel"
    ? "position: absolute;"
    : ""
  }}

  @media (min-width: ${tabletBreakpoint}) {
    position: absolute;
    top: ${pxToRem(8)};
  }
`;

export const BannerSubtitleStyle = css`
  align-items: center;
  display: flex;
  font-family: Nunito-Sans-Regular;
  font-size: 0.8125rem;
  line-height: 1.125rem;
  margin-block-start: 0;
  margin-bottom: 1.25rem;
  margin-top: 0;
  position: relative;
  z-index: 5;

  .separator {
    background: ${(props) => props.theme.colors.white};
    border-radius: 50%;
    display: inline-block;
    height: 0.625rem;
    margin: 0 0.625rem;
    width: 0.625rem;
  }

  @media (min-width: ${tabletBreakpoint}) {
    font-size: 0.875rem;
    line-height: 1.1875rem;
    margin-bottom: 2rem;
  }

  @media (min-width: ${desktopBreakpoint}) {
    font-size: ${pxToRem(18)};
    line-height: 1;
    margin-bottom: ${pxToRem(22)};
  }
`;

export const EmptyResults = css`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
  max-width: ${pxToRem(830)};
  padding: ${pxToRem(0)} ${pxToRem(60)};
  text-align: center;
`;

export const EmptyResultsTitle = css`
  color: ${(props) => props.theme.colors.white};
  font-family: "Nunito-Sans-Bold";
  font-size: ${pxToRem(20)};
  
  @media (min-width: ${tabletBreakpoint}) {
    font-size: ${pxToRem(34)};
  }
`;

export const EmptyResultsMessage = css`
  color: ${(props) => props.theme.colors.errorGrey};
  font-family: "Nunito-Sans-Light";
  text-align: center;
  font-size: ${pxToRem(12)};

  @media (min-width: ${tabletBreakpoint}) {
    font-size: ${pxToRem(15)};
  }
`;

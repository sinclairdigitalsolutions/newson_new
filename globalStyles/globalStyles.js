import { createGlobalStyle } from "styled-components";
import { tabletBreakpoint, laptopBreakpoint, pxToRem } from "./sharedStyles";
import { BANNER_SIZES_IN_PIXELS, BANNER_SIZE_IN_PIXELS } from '../lib/constants';

const GlobalStyles = createGlobalStyle`
  body {
    background: ${(props) => props.theme.colors.darkGrey};
    color: ${(props) => props.theme.colors.white};
    margin: 0;
    padding: 0;
    font-size: 16px;
  }

  #__next{
    
    margin-bottom: ${ pxToRem(BANNER_SIZES_IN_PIXELS.GLOBAL_NAV + 10) };
    @media (min-width: ${laptopBreakpoint}){
      margin-bottom: auto;
    }
  }

  a {
    text-decoration: none;
  }

  @font-face {
    font-family: 'Nunito-Sans-Light';
    src: url('/fonts/NunitoSans-Light.ttf');
  }

  @font-face {
  font-family: 'Nunito-Sans-Regular';
  src: url('/fonts/NunitoSans-Regular.ttf');
  }

  @font-face {
    font-family: 'Nunito-Sans-Bold';
    src: url('/fonts/NunitoSans-Bold.ttf');
  }

  @font-face {
    font-family: 'Nunito-Sans-SemiBold';
    src: url('/fonts/NunitoSans-SemiBold.ttf');
  }

  @font-face {
    font-family: 'Nunito-Sans-Italic';
    src: url('/fonts/NunitoSans-Italic.ttf');
  }

  @font-face {
    font-family: 'Nunito-Sans-BoldItalic';
    src: url('/fonts/NunitoSans-BoldItalic.ttf');
  }

  @font-face {
    font-family: 'Nunito-Sans-ExtraLight';
    src: url('/fonts/NunitoSans-ExtraLight.ttf');
  }

  .favorite-icon__favorited {
    z-index: 1000;
  }

  .react-multi-carousel-track {
    li {
      min-width: ${pxToRem(303)}!important;
    }
    @media (min-width: ${tabletBreakpoint}) {
      li {
        min-width: none;
      }
    }
  }

  .MuiList-root {
    color: ${(props) => props.theme.colors.navy};
    max-height: ${pxToRem(400)};
  }

  .MuiMenuItem-root {
    font-family: Nunito-Sans-Regular !important;
    padding-right: ${pxToRem(10)};

    .location-menu-icon {
      margin-right: ${pxToRem(5)};
    }

    &.location-first-item {
      position: relative;
      background-color: ${(props) => props.theme.colors.white};
      &::after {
        display: block;
        content: "";
        position: absolute;
        width: 0; 
        height: 0;
        right: ${pxToRem(12)};
        top: 50%;
        transform: translateY(-50%);
        border-left: 4px solid transparent;
        border-right: 4px solid transparent;
        border-top: 5px solid currentColor;
      }

      &.Mui-selected,
      &.Mui-selected:hover {
        background-color: ${(props) => props.theme.colors.white};
      }
    }
  }

  .MuiPopover-paper {
    scrollbar-width: thin;

    @media (min-width: ${tabletBreakpoint}) {
      margin-left: ${pxToRem(-50)};
    }
  }
`;

export default GlobalStyles;

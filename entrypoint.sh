#!/bin/sh

cd /usr/src/app

if [ -z "$SECRET_KEY_NAME" ]; then
  echo >&2 'SECRET_KEY_NAME environment variable is missing, going into development mode...'
else
  echo >&2 'SECRET_KEY_NAME environment variables are set, retrieving credentials...'
  export $(aws secretsmanager get-secret-value --secret-id "${SECRET_KEY_NAME}"  --region "${REGION}" | jq -r .SecretString | jq -r 'to_entries|map("\(.key)=\(.value|tostring)")|.[]')
fi

yarn run start
#!/bin/sh
#Only dashisms allowed (https://wiki.archlinux.org/index.php/Dash).

DEPLOY="$(cat /tmp/deploy.out)"

#Function to check if the AWS command was successful.
check_aws_status () {
  #The $? variable always contains the status code of the previously run command.
  #We can either pass in $? or this function will use it as the default value if nothing is passed in.
  local prev=${1:-$?}

  if [ $prev -eq 0 ]; then
    echo "AWS CLI command has succeeded."
  else
    echo "AWS CLI command has failed."
    #Kill the build script so that we go no further.
    exit 1
  fi
}

if [ "$DEPLOY" = "true" ]; then
  echo "Pushing the Docker image..."
  docker push "$(cat /tmp/build_tag.out)"
  docker push "$(cat /tmp/version_tag.out)"
  docker push "$(cat /tmp/build_tag_latest.out)"
else
  echo "No Docker image was built, nothing to deploy..."
fi
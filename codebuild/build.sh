#!/bin/sh
#Only dashisms allowed (https://wiki.archlinux.org/index.php/Dash).

BUILD="$(cat /tmp/build.out)"

#Function to check if the Docker build completed successfully.
check_docker_status () {
  #The $? variable always contains the status code of the previously run command.
  #We can either pass in $? or this function will use it as the default value if nothing is passed in.
  local prev=${1:-$?}

  if [ $prev -eq 0 ]; then
    echo "Docker build has succeeded."
    #Set the deployment tag to "true" since the build has succeeded.
    printf "%s" "true" > /tmp/deploy.out
  else
    echo "Docker build has failed."
    #Set the deployment tag to "false" since the build has failed.
    printf "%s" "false" > /tmp/deploy.out
    #Kill the build script so that we go no further.
    exit 1
  fi
}

if [ "$BUILD" = "true" ]; then
  echo "Checking the Docker version..."
  docker version
  
  echo "Building the Docker image: $IMAGE_REPO_NAME"
  docker build --build-arg ENV=$ENVIRONMENT --build-arg BUILD_ID="$(cat /tmp/build_id.out)" -f Dockerfile -t $IMAGE_REPO_NAME .
  check_docker_status $?
  docker tag "$IMAGE_REPO_NAME:latest" "$(cat /tmp/build_tag.out)"
  docker tag "$IMAGE_REPO_NAME:latest" "$(cat /tmp/version_tag.out)"
  docker tag "$IMAGE_REPO_NAME:latest" "$(cat /tmp/build_tag_latest.out)"
else
  echo "Docker image already exists for this GIT hash, not rebuilding image..."
  printf "%s" "false" > /tmp/deploy.out
fi
#!/bin/sh
#Only dashisms allowed (https://wiki.archlinux.org/index.php/Dash).

VERSION=$(grep -m1 version package.json | awk -F: '{ print $2 }' | sed 's/[", ]//g')
NAME=$(grep -m1 name package.json | awk -F: '{ print $2 }' | sed 's/[", ]//g')

#Function to check if the AWS command was successful.
check_aws_status () {
  #The $? variable always contains the status code of the previously run command.
  #We can either pass in $? or this function will use it as the default value if nothing is passed in.
  local prev=${1:-$?}

  if [ $prev -eq 0 ]; then
    echo "AWS CLI command has succeeded."
    #Set the build tag to "true" since the build has succeeded.
    printf "%s" "true" > /tmp/build.out
  else
    echo "AWS CLI command has failed."
    #Set the build tag to "false" since the build has failed.
    printf "%s" "false" > /tmp/build.out
    #Kill the build script so that we go no further.
    exit 1
  fi
}

echo "Application name is \"$NAME\"..."
printf "%s" "$NAME" > /tmp/name.out

echo "Application version is \"$VERSION\"..."
printf "%s" "$VERSION" > /tmp/version.out

#Get the first seven digits of the unique ID for this CodeBuild instance...
printf "%s" "$CODEBUILD_BUILD_ID" | sed "s/.*:\([[:xdigit:]]\{7\}\).*/\1/" > /tmp/build_id.out
printf "%s" "codebuild-$(cat /tmp/build_id.out)" > /tmp/build_id.out

echo "Initiating CodePipeline is $CODEBUILD_INITIATOR..."
CURRENT_PIPELINE="$(printf "%s" "$CODEBUILD_INITIATOR" | rev | cut -d/ -f1 | rev)"
echo "Current pipeline name is $CURRENT_PIPELINE..."

TZ="America/New_York" date +"%Y%m%d%H%M%S" > /tmp/datetime_et.out
echo "Current time in the Eastern Time Zone is $(cat /tmp/datetime_et.out)..."

echo "Preparing the various Docker tags..."
#Build the full URLs for the docker image repository, both for latest and the unique tag for this image.
printf "%s" "$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$IMAGE_REPO_NAME" > /tmp/docker_url.out
printf "%s:%s" "$(cat /tmp/docker_url.out)" "$(cat /tmp/build_id.out)" > /tmp/build_tag.out
printf "%s:%s" "$(cat /tmp/docker_url.out)" "latest" > /tmp/build_tag_latest.out

#Change the version tag if we are in the stage environment.
if [ "$ENVIRONMENT" = "stage" ]; then
  printf "%s:%s" "$(cat /tmp/docker_url.out)" "version-$VERSION" > /tmp/version_tag.out
else
  printf "%s:%s" "$(cat /tmp/docker_url.out)" "version-$VERSION-$(cat /tmp/datetime_et.out)" > /tmp/version_tag.out
fi

echo "Logging into non-prod Amazon ECR..."
$(aws ecr get-login --region $AWS_REGION --registry-ids $AWS_ACCOUNT_ID --no-include-email)
check_aws_status $?

echo "Checking if non-production Docker image already exists..."

#Try to pull an image with the same GIT tag.
#NOTE: We could also list out the images and see if the tag exits, but doing a pull makes sure there aren't issues with the image.
docker pull "$(cat /tmp/version_tag.out)"

if [ $? -ne 0 ]; then
  echo "No Docker image with tag \"$(cat /tmp/version_tag.out)\" was found..."
  printf "%s" "true" > /tmp/build.out
else
  echo "Docker image already exists for this GIT hash, not rebuilding image..."
  printf "%s" "false" > /tmp/build.out
fi

echo "Creating build.json file..."
#NOTE: This must use the same tag that is being tested for by the build, otherwise we run the risk of a different tag not being there when we expect it.
printf '{"tag":"%s"}' "$(cat /tmp/version_tag.out)" > /tmp/build.json
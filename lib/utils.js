import { BANNER_SIZES_IN_PIXELS } from './constants'

export const debounce = (fn, ms) => {
  let timer;
  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      timer = null;
      fn.apply(this, arguments);
    }, ms);
  };
};

/**
 * A helper to remove all duplicate items from an array
 *
 * @param { Array } the array to be filtered
 *
 * @returns { Array } a new filtered array
 */
export const filterDuplicates = (array) => {
  let data = array.map((item) => {
    return [item.id, item];
  });
  let mapArray = new Map(data);
  return [...mapArray.values()];
};

/**
 * A helper to remove all duplicate stations from any number of other arrays
 *
 * @param { Array } initalArray an array of station objects for duplicates to be filtered from
 * @param { Array } toBeChecked a array of arrays of station objects to be checked again initalArray
 *
 * @returns { Array } an array with all duplicate values filtered out
 */
export const removeStationDuplicates = (initalArray, toBeChecked) => {
  let stationIdsToBeRemoved = toBeChecked
    .map(innerArray => 
      [...innerArray.map( station => station.id )]
    )
    .flat()

  return initalArray.filter( station => !( stationIdsToBeRemoved.includes(station.id) ) );
}

/**
 * Generate a random Interger between two values
 * @param { Number } min the minimum range
 * @param { Number } max  the maximum range
 *
 * @returns { Number } the random interger
 */
export const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min) + min);
};

/**
 * Returns Current UTC date
 * @returns { Number } the random interger
 */
export const getCurrentDate = () => Math.round(new Date().getTime() / 1000);

/**
 * Truncate a string and add elipsis based on num of chars
 * @param { String } str the string to truncate
 * @param { Number } num  the number of characters to return
 *
 * @returns { String } the truncated string
 */
export const truncateString = (str, num) => {
  if (str.length <= num) {
    return str
  }
  return str.slice(0, num) + '...'
}

/**
 * Helper to extract the callsign of a station from the station name
 * 
 * @param { String } stationName the full name of the station
 * 
 * @return { String } the station call sign extracted from the stationName
 */

export const getCallSignFromStationName = stationName => {
    return stationName?.replace(/\s/g, '').split('-')[0]
}

/**
 * Helper to stop duplicate page views on pages that require
 *   extra information for their page title
 */
export const sendPageView = url => {
  const pageViewExceptions = ['stationDetails','search','byLocation']

  return !pageViewExceptions.includes(url.split('/')[1])
}

/**
 * Helper function to initialize all of the dimension values for video and ad events.
 *  The video data will either be passed as live or clip depending on the status of 
 *  the content being watched
 * 
 * 
 * @param { String } stationName the name of the station coresponding to the video
 * @param { Object } live the live video data
 * @param { Object } clip the clip video data
 */
export const setupAnalyticsDimensions = (stationName, channel, live, clip) => {
  const dimensions = { 
    dimension5: stationName, 
    dimension6: getCallSignFromStationName(channel.description) 
  };

  if(Boolean(clip)){
    return {
      dimension2: 'clip',
      dimension3: clip.name,
      dimension4: clip.startTime,
      ...dimensions
    }
  }
  
  if(Boolean(live)){
    return {
      dimension2: 'live',
      dimension3: live.name,
      dimension4: live.startTime,
      ...dimensions
    }
  }
}

/**
 * A helper that returns the correct padding for a page to be pushed down below
 *  the banners at the top
 * 
 * @param { Boolean } breakingNewsEnabled whether or not there is a breaking news banner at the top of the page
 */
export const paddingTopSizeInPixels = (breakingNewsEnabled) => {
  return breakingNewsEnabled ? BANNER_SIZES_IN_PIXELS.BREAKING_AND_NAV() : BANNER_SIZES_IN_PIXELS.GLOBAL_NAV
}

/**
 * A helper to sort a collection of channels by their specified DMAs.
 *  Also adds favorited channels to the beginning of the station group
 * 
 * @param {Array} channels an array of all the channels
 * @return {Object} a new object containing the channels sorted by DMA
 * 
 * sortedByDma STRUCTURE:
 *  sortedByDma = {
 *      dmaName1: [channelInDma1...],
 *      ...
 * }
 * 
 */
export const sortByDma = (channels, favorites) => {
  const sortedByDma = {}
  const channelFavorites = {}

  channels.forEach((channel, index) => {
    let currentDma = channel.configValue.locations[0].city;
    favorites.includes(parseInt(channel.id)) 
    ? channelFavorites[currentDma]
      ? channelFavorites[currentDma].push(channel)
      : channelFavorites[currentDma] = [channel]
    : sortedByDma[currentDma] 
      ? sortedByDma[currentDma].push(channel)
      : sortedByDma[currentDma] = [channel]
  })

  for(const dma in sortedByDma){
    sortedByDma[dma] = channelFavorites[dma]
    ? [...sortChannelsByDistance(channelFavorites[dma]), ...sortChannelsByDistance(sortedByDma[dma])]
    : sortChannelsByDistance(sortedByDma[dma])
  }
  return sortedByDma
}

/**
 * A helper that sorts an array of channels by the distance from the user
 * 
 * @param {Array} channels an array containing the channels to be sorted
 */
const sortChannelsByDistance = (channels) => {
  return channels.sort((channel1, channel2) => channel1.distance - channel2.distance )
}
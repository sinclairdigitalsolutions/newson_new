/**
 * Gets stations from vincent api
 *
 * @param { String } path the path to the api response
 * @param { String } params the key/value pairs required for the api apth
 * @param { String } signature a hash from the params required for the path
 * @param { String } currentDate a UTC string in seconds of the current date from the epoch (Jan. 1, 1970)
 *
 * @returns { Object } json object of the api response
 */
export const getCollection = async (path, params, signature, currentDate) => {
  const authString = Buffer.from(
    `${process.env.VINSENT_API_KEY}:${process.env.VINSENT_API_SECRET}`
  ).toString("base64");
  const paramsString = params.sort().join("&");

  const stationFetch = await fetch(
    `https://${process.env.VINSON_API_ENDPOINT}/v2external/${path}?apikey=${process.env.VINSENT_API_KEY}&${paramsString}&sig=${signature}&sigtime=${currentDate}&streamtype=website`,
    {
      method: "POST",
      headers: {
        "content-type": "json",
        Authorization: `Basic ${authString}`,
      },
    }
  )
    .then(function (response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response;
    })
    .then((response) => {
      return response.json();
    })
    .catch((response) => {
      return (response = {
        statusCode: 400,
        statusText: response,
      });
    });

  return stationFetch;
};

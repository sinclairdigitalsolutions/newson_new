import Router from "next/router";

/**
 * Redirects to the search page with the state query to filter by state
 *
 * @param { String } state the State to filter the return results by
 * @param { String } path the page to redirect to
 */
export const redirectHandler = (term = "", path) => {
  Router.push({
    pathname: path,
    query: {
      term,
    },
  });
};

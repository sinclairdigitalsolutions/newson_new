import firebase from "firebase";

/**
 * Instantiate firebase based on the environment variables in
 * .env.local
 *
 * @returns { Object } firebase the firebase instance
 */
export const loadFirebaseDB = () => {
  const firebaseConfig = {
    apiKey: process.env.FIREBBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASURMENT_ID,
  };
  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
    //Analytics currently commented out because it it crashing the app on first load
    //leaving it in here in case we want it though
    //firebase.analytics();
  }

  return firebase;
};

const crypto = require("crypto");

/**
 * Generates the signature required to fetch data from the Vinson api
 *
 * @param { String } currentDate the UTC current day in seconds
 * @param { Array } params the params required to complete the signature
 * @param { String } apiPath the path the the api data
 *
 * @returns { String } a hash consisting of api parameters
 */
export const createVinsonSignature = (currentDate, params, apiPath = "") => {
  let hash;
  const apiConfig = {
    apiKey: process.env.VINSENT_API_KEY,
    apiSecret: process.env.VINSENT_API_SECRET,
  };
  const configParams = [
    `apikey=${apiConfig.apiKey}`,
    `sigtime=${currentDate}`,
    `streamtype=website`,
  ];
  const requestParams = configParams.concat(params);
  const sortedParams = requestParams.sort().join("&");
  const signature = `${sortedParams}&${apiPath}${apiConfig.apiSecret}`;

  hash = crypto.createHash("sha1").update(signature).digest("hex");

  return hash;
};

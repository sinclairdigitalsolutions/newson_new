const { getClientIp } = require("@supercharge/request-ip");
const WebServiceClient = require("@maxmind/geoip2-node").WebServiceClient;
const client = new WebServiceClient(
  process.env.MAXMIND_U,
  process.env.MAXMIND_P
);

export const getUserLocation = async (req) => {
  let coordinates = {};
  const ip = getClientIp(req);

  await client
    .city(ip)
    .then((response) => {
      coordinates.latitude = response.location.latitude;
      coordinates.longitude = response.location.longitude;
    })
    .catch((error) => {
      coordinates.latitude = "44.124633";
      coordinates.longitude = "-68.741760";
    });
  return coordinates;
};

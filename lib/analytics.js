import { ANALYTICS_VALUES } from '../lib/constants'
const { 
    APP_PLATFORM, 
    CATEGORIES,
    EVENTS,
    PAGE_VIEW_TITLES,
} = ANALYTICS_VALUES;

export const GA_TRACKING_ID = (() => process.env.GA_TRACKING_ID)()

/** Description of dimensions
 *      @param { String } dimension1: App-Platform (only for FireTv and AppleTv to create a filter)
 *      @param { String } dimension2: Content Type (live/clip or previousNewscast)
 *      @param { String } dimension3: Video Title (for example `NEWS CENTER Maine MORNING REPORT`)
 *      @param { String } dimension4: Video Date (start/release time/date in epoch seconds, for example `1602577200`)
 *      @param { String } dimension5: Station Name (for example `WLBZ - Bangor`)
 *      @param { String } dimension6: Call Sign (for example `WLBZ`)
 */

/***** Helpers *****/
/**
 * A helper function that sends an analytics tracking call using the given parameters
 *
 * @param { String } url the page excluding the domain
 * @param { Object } dimensions any custom values to be sent with the pageview (see top for description)
 *
 * https://developers.google.com/analytics/devguides/collection/gtagjs/pages
 */
const sendPageView = (url, pageTitle, dimensions) => {
  if (window?.gtag) {
    window.gtag("config", GA_TRACKING_ID, {
      page_path: url,
      page_title: pageTitle,
      ...dimensions,
    });
  }
};
/**
 * A helper function that sends an analytics tracking call using the given parameters
 *
 * @param { String } eventName a general title for the event
 * @param { String } category the category that the event falls under
 * @param { String } label a short description of the event (can be used for dynamic values as well [ex. analyticsSearchEvent])
 * @param { Object } dimensions any custom values to be sent with the event (see top for description)
 *
 * https://developers.google.com/analytics/devguides/collection/gtagjs/events
 */
const sendEvent = ({ eventName, category, label, dimensions }) => {
  if (window?.gtag) {
    window.gtag("event", eventName, {
      event_category: category,
      event_label: label,
      ...dimensions,
    });
  }
};
/**
 * A function that takes a full url and 'translates' it into its analytics page name
 *   and sends a pageView request with the translated url
 *
 * @param { String } pageTitle the url of the page excluding the domain
 */
const getPageFromUrl = (url) => {
  let pageTitle = url;
  if (url === "/byLocation") {
    pageTitle = "/LocationScreen";
  } else {
    for (const pageTitleKey of Object.keys(PAGE_VIEW_TITLES)) {
      if (url.includes(pageTitleKey)) {
        pageTitle = PAGE_VIEW_TITLES[pageTitleKey];
        break;
      }
    }
  }
  return pageTitle;
};
/**
 * A function that takes the page (usually translated from getPageFromUrl)
 *    and extracts the page title
 *
 * @param { String } page the name of the page
 */
const getPageTitleFromPage = (page) => {
  return page
    .split("/")[1]
    .replace(/([A-Z])/g, " $1")
    .trim();
};

/***** PageViews *****/
/**
 * Sends a pageview event when a user visits a page
 *
 * @param { String } url the url of the page excluding the domain
 * @param { Object } dimensions any custom values to be sent with the event (see top for description)
 */
export const analyticsPageView = (url, dimensions) => {
  const page = getPageFromUrl(url);
  // Extracts the page title from the page name
  const pageTitle = getPageTitleFromPage(page);

  sendPageView(page, pageTitle, dimensions);
};

/**
 * A page view specific to the station page
 *
 * @param { String } pageTitle the url of the page excluding the domain
 */
export const analyticsStationPageView = (url, stationCallSign, dimensions) => {
  const page = getPageFromUrl(url);
  // Extracts the page title from the page name
  const pageTitle = `${getPageTitleFromPage(page)} - ${stationCallSign}`;

  sendPageView(page, pageTitle, dimensions);
};
/**
 * A page view specific to the search page
 *
 * @param { String } pageTitle the url of the page excluding the domain
 */
export const analyticsSearchPageView = (url, query) => {
  const page = getPageFromUrl(url);
  // Extracts the page title from the page name
  const pageTitle = query
    ? `${getPageTitleFromPage(page)} - ${query}`
    : getPageTitleFromPage(page);

  sendPageView(page, pageTitle);
};
/**
 * A page view specific to the byLocation page
 *
 * @param { String } pageTitle the url of the page excluding the domain
 */
export const analyticsLocationPageView = (url, state) => {
  const page = getPageFromUrl(url);

  // Extracts the page title from the page name
  const pageTitle = state
    ? `${getPageTitleFromPage(page)} - ${state}`
    : getPageTitleFromPage(page);

  sendPageView(page, pageTitle);
};

/***** Search *****/
/**
 * Tracks when a user searches by station
 *
 * @param { String } term The search term used by the user
 */
export const analyticsSearchEvent = (term) => {
  sendEvent({
    eventName: EVENTS.SEARCH,
    category: CATEGORIES.SEARCH,
    label: term,
  });
};

/***** Favorites Events *****/
/**
 * Tracks when a user adds a station to their favorites
 *
 * @param { String } stationCallSign the 4-digit callsign specific to one station
 * @param { String } stationName the full name of the station
 */
export const analyticsAddFavoritesEvent = (stationCallSign, stationName) => {
  sendEvent({
    action: EVENTS.ADD_FAVORITE,
    category: CATEGORIES.STATION,
    label: stationCallSign,
    dimensions: { dimension5: stationName },
  });
};
/**
 * Tracks when a user adds a station to their favorites
 *
 * @param { String } stationCallSign the 4-digit callsign specific to one station
 * @param { String } stationName the full name of the station
 */
export const analyticsRemoveFavoritesEvent = (stationCallSign, stationName) => {
  sendEvent({
    action: EVENTS.REMOVE_FAVORITE,
    category: CATEGORIES.STATION,
    label: stationCallSign,
    dimensions: { dimension5: stationName },
  });
};

/***** Video Events *****/
/** An internal function for all video events
 *
 * @param { String } eventName the action of the event
 * @param { String } videoTitle the name of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
const analyticsVideoEvent = (eventName, dimensions) => {
  sendEvent({
    eventName,
    category: CATEGORIES.VIDEO,
    label: dimensions.videoTitle,
    ...dimensions,
  });
};
/** Sends an event when the video is played
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoPlay = (dimensions) => {
  analyticsVideoEvent(EVENTS.PLAY, dimensions);
};
/** Sends an event when the video is stopped
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoStop = (dimensions) => {
  analyticsVideoEvent(EVENTS.STOP, dimensions);
};
/** Sends an event when the video is initated
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoInitiatePlay = (dimensions) => {
  analyticsVideoEvent(EVENTS.INITIATE_PLAY, dimensions);
};
/** Sends an event when the video progresses past certain thresholds
 *    25%, 50%, 75%, 100%
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Number } progress the amount of progress watched in the video
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoProgress = (progress, dimensions) => {
  analyticsVideoEvent(progress, dimensions);
};
/** Sends an event when the video is paused
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoPause = (dimensions) => {
  analyticsVideoEvent(EVENTS.PAUSE, dimensions);
};
/** Sends an event when the video is resumed
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsVideoResume = (dimensions) => {
  analyticsVideoEvent(EVENTS.RESUME, dimensions);
};
/***** Ad Events *****/
/** An internal function for all ad events
 *
 * @param { String } eventName the action of the event
 * @param { String } videoTitle the name of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
const analyticsAdEvent = (eventName, dimensions) => {
  sendEvent({
    eventName,
    category: CATEGORIES.AD,
    label: dimensions.videoTitle,
    ...dimensions,
  });
};
/** Sends an event when an ad is played
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsAdInitiatePlay = (dimensions) => {
  analyticsAdEvent(EVENTS.INITIATE_PLAY, dimensions);
};
/** Sends an event when the video progresses past certain thresholds
 *    25%, 50%, 75%, 100%
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Number } progress the amount of progress watched in the video
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsAdProgress = (progress, dimensions) => {
  analyticsAdEvent(progress, dimensions);
};
/** Sends an event when the video is paused
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsAdPause = (dimensions) => {
  analyticsAdEvent(EVENTS.PAUSE, dimensions);
};
/** Sends an event when the video is resumed
 *
 * @param { String } videoTitle the title of the video being played
 * @param { Object } dimensions any custom values to be sent with the event
 */
export const analyticsAdResume = (dimensions) => {
  analyticsAdEvent(EVENTS.RESUME, dimensions);
};

/***** Errors *****/
/**
 * Tracks when an error occurs site wide
 *
 * @param { String } errorMessage the message describing the error
 *
 */
export const analyticsErrorEvent = (errorMessage) => {
  sendEvent({
    eventName: EVENTS.ERROR,
    category: CATEGORIES.ERROR,
    label: errorMessage,
  });
};
/**
 * Tracks when an error occurs on video playback
 *
 * @param { String } errorMessage the message describing the error
 * @param { Object } dimensions any custom values to be sent with the event
 *
 */
export const analyticsVideoErrorEvent = (errorMessage, dimensions) => {
  sendEvent({
    eventName: EVENTS.ERROR_VIDEO,
    category: CATEGORIES.ERROR,
    label: errorMessage,
    ...dimensions,
  });
};

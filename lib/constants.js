export const DB_FAVORITES_KEY = "F";
export const DB_LOCAL_STATIONS_KEY = "C";
export const DB_LIVE_STATIONS_KEY = "L";
export const DB_BREAKING_NEWS_STATIONS_KEY = "B";
export const BANNER_IMAGE_ROOT_URL =
  "https://redspace-newson.akamaized.net/banner-images";
export const STATION_GROUP_TITLES = {
  LIVE: "Newscasts",
  CLIPS: "Clips",
  LIVE_NOW: "Live Now",
  LIVE_STATIONS: "Live Stations",
  BREAKING_NEWS: "Breaking News",
  LOCAL_STATIONS: "Stations Near Me"
};
export const URL_TEMPLATES = {
  LIVE: '/live/[...id]',
  CLIPS: '/clips/[...id]',
  BREAKING_NEWS: '/breakingNews',
  STATION_DETAILS: '/stationDetails/[id]',
  SEARCH: '/search',
  FAVORITES: '/favorites',
  BY_LOCATION: '/byLocation'
}
export const FACEBOOK_SDK_URL =
  "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";

export const ERROR_TYPES = {
  AMP_ERROR: "AMP_ERROR",
  VIDEO_EXPIRED: "VIDEO_EXPIRED",
};
export const ERROR_COPY = {
  [ERROR_TYPES.AMP_ERROR]: {
    TITLE: "Something has gone wrong!",
    DESCRIPTION:
      "We encountered an error while fetching your video. Try refreshing the page to try again.",
  },
  [ERROR_TYPES.VIDEO_EXPIRED]: {
    TITLE: "This video has expired!",
    DESCRIPTION: "Visit news.on for more free breaking news.",
  },
};
export const HERO_CAROUSEL_BUTTON_LABEL = {
  DEFAULT: "Watch Now",
  LIVE: "Play Now"
}
export const BANNER_SIZES_IN_PIXELS = {
  BREAKING_NEWS: 60,
  GLOBAL_NAV: 65,
  LOGO_HEADER: 65,
  BREAKING_AND_NAV: function() { return this.BREAKING_NEWS + this.GLOBAL_NAV } ,
}
export const DEFAULT_FREQUENCY = {
  LIVE_ITEMS_PAGE: "12",
}
export const ANALYTICS_VALUES = {
  APP_PLATFORM: 'Web',
  CATEGORIES : {
    VIDEO: 'Video',
    AD: 'Ad',
    STATION: 'Station',
    ERROR: 'Error',
    SEARCH: 'Search',
    LAUNCH: 'Launch'
  },
  EVENTS: {
    LAUNCH: 'Launch',
    SEARCH: 'Search',
    PLAY: 'Play',
    PAUSE: 'Pause',
    RESUME: 'Resume',
    STOP: 'Stop',
    INITIATE_PLAY: 'InitiatePlay',
    FIRST_QUARTILE: '25%',
    MIDPOINT: '50%',
    THIRD_QUARTILE: '75%',
    COMPLETE: '100%',
    REMOVE_FAVORITE: 'Remove Favorite',
    ADD_FAVORITE: 'Add Favorite',
    ERROR: 'Error',
    ERROR_VIDEO: 'Video'
  },
  PAGE_VIEW_TITLES: { 
    '/featured': '/HomeScreen',
    '/stationDetails': '/StationScreen',
    '/liveNow': '/LiveScreen',
    '/search' : '/SearchResultsScreen',
    '/favorites' : '/FavoritesScreen',
    '/byLocation' : '/LocationStateScreen',
    '/about' : '/SettingsScreen',
    '/breakingNews' : '/BreakingNewsScreen',
    '/clips' : '/VideoScreen',
    '/live' : '/VideoScreen',
  }
}
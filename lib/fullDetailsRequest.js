/**
 * Gets VOD ad full details from vincent api
 *
 * @param { String } params the key/value pairs required for the api apth
 * @param { String } signature a hash from the params required for the path
 * @param { String } currentDate a UTC string in seconds of the current date from the epoch (Jan. 1, 1970)
 *
 * @returns { Object } json object of the api response
 */
export const getFullDetails = async (params, signature, currentDate) => {
  const authString = Buffer.from(
    `${process.env.VINSENT_API_KEY}:${process.env.VINSENT_API_SECRET}`
  ).toString("base64");
  
  const host = "https://vinsonapi-newson.triple-it.nl";
  const path = "/V2External/Program/FullDetail";
  const configParams = [
    `apikey=externalapinewson`,
    `sig=${signature}`
    `sigtime=${currentDate}`,
    `streamtype=website`,
  ];
  const requestParams = configParams.concat(params);
  const sortedParams = requestParams.sort().join("&");
  const fetchUrl = `${host}${path}?${sortedParams}`;
  
  const fullDetailsFetch = await fetch(fetchUrl, {
    method: "POST",
    headers: {
      "content-type": "json",
      Authorization: `Basic ${authString}`,
    },
  })
  .then(function (response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  })
  .then((response) => {
    return response;
  })
  .catch((response) => {
    return (response = {
      statusCode: 400,
      statusText: response,
    });
  });

  return fullDetailsFetch;
};
